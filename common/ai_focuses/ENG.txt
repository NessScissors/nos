
ai_focus_defense_ENG = {
	research = {
		defensive = 10.0
		radar_tech = 3.0
		construction_tech = 2.0
	}
}

ai_focus_aggressive_ENG = {
	research = {
		offensive = 10.0
		synth_resources = 1.0
		motorized_equipment = 2.0
		armor = 2.0
	}
}

ai_focus_war_production_ENG = {
	research = {
		industry = 40.0
		construction_tech = 20.0
		electronics = 20.0
		computing_tech = 40.0	
	}
}

ai_focus_military_equipment_ENG = {
	research = {
		infantry_weapons = 160.0
		infantry_tech = 80.0
		night_vision = 20.0
		support_tech = 10.0
		artillery = 80.0
		motorized_equipment = 40.0
		armor = 10.0
		cat_light_armor = 40.0
		cat_medium_armor = 40.0
		cat_heavy_armor = 80.0
		encryption_tech = 20.0
		decryption_tech = 40.0
	}
}

ai_focus_military_advancements_ENG = {
	research = {
		land_doctrine = 320.0
		radar_tech = 40.0
		encryption_tech = 40.0
		decryption_tech = 80.0
		rocketry = 20.0
		jet_technology = 160.0
		motorized_equipment = 20.0
		nuclear = 10.0
	}
}

ai_focus_peaceful_ENG = {
	research = {
		industry = 80.0
		construction_tech = 40.0
		electronics = 20.0
		computing_tech = 40.0
		synth_resources = 0.0
	}
}

ai_focus_naval_ENG = {
	research = {
		naval_doctrine = 320.0
		naval_equipment = 20.0
        marine_tech = 15.0
		dd_tech = 160.0
		cl_tech = 40.0
		ca_tech = 80.0
		bc_tech = 80.0
		bb_tech = 160.0
		shbb_tech = 20.0
		cv_tech = 40.0
		ss_tech = 20.0
		tp_tech = 40.0
		marine_tech = 20.0
	}
}

ai_focus_naval_air_ENG = {
	research = {
		naval_air = 40.0
		naval_bomber = 20.0
	}
}

ai_focus_aviation_ENG = {
	research = {
		air_doctrine = 320.0
		cas_bomber = 20.0		
		light_fighter = 160.0
		naval_bomber = 40.0
		cat_heavy_fighter = 80.0
		tactical_bomber = 80.0
		cat_strategic_bomber = 160.0
		para_tech = 40.0
	}
}
