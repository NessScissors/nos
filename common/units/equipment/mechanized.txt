equipments = {

	mechanized_equipment = {
		year = 1936

		is_archetype = yes
		picture = archetype_motorized_equipment		
		is_buildable = no
		type = {
			#infantry #Removing inf type 
			mechanized
		}
		group_by = archetype
		
		interface_category = interface_category_land
		
		#Misc Abilities
		maximum_speed = 16
		reliability = 0.8
		hardness = 0.7
		
		#Defensive Abilities
		defense = 26
		breakthrough = 4
		armor_value = 15
		soft_attack = 10

		#Space taken in convoy
		lend_lease_cost = 5
		
		build_cost_ic = 4
		resources = {
			steel = 1

			rubber = 1
		}
		fuel_consumption = 1.2
	}

	# ~1941
	mechanized_equipment_1 = {
		year = 1940

		archetype = mechanized_equipment
		priority = 40
		visual_level = 0
	}

	# ~1943
	mechanized_equipment_2 = {
		year = 1942

		archetype = mechanized_equipment
		parent = mechanized_equipment_1
		priority = 40
		visual_level = 1

		maximum_speed = 17

		#Defensive Abilities
		defense = 30
		breakthrough = 5
		armor_value = 20
		soft_attack = 11
		hardness = 0.75
		
	}

	# ~1945
	mechanized_equipment_3 = {
		year = 1944

		archetype = mechanized_equipment
		parent = mechanized_equipment_2
		priority = 40
		visual_level = 2
		
		maximum_speed = 18

		#Defensive Abilities
		defense = 34
		breakthrough = 6
		armor_value = 25
		soft_attack = 12
		hardness = 0.8
		
	}
	# ~1950
	mechanized_equipment_4 = {
		year = 1950

		archetype = mechanized_equipment
		parent = mechanized_equipment_3
		priority = 40
		visual_level = 2
		
		maximum_speed = 19

		#Defensive Abilities
		defense = 40
		breakthrough = 7
		armor_value = 30
		soft_attack = 15
		hardness = 0.8
		
	}
	# ~1960
	mechanized_equipment_5 = {
		year = 1960

		archetype = mechanized_equipment
		parent = mechanized_equipment_4
		priority = 40
		visual_level = 2
		
		maximum_speed = 20

		#Defensive Abilities
		defense = 45
		breakthrough = 8
		armor_value = 35
		soft_attack = 20	
		hardness = 0.8
		
	}
}
