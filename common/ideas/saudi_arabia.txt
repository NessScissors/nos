ideas = {
country = {

SAU_dem = {

allowed = {
always = no
}

allowed_civil_war = {
always = yes
}

removal_cost = -1

modifier = {
conscription = 0.1
war_support_factor = -0.05
democratic_drift = 0.1
conservative_drift = 0.1
social_democracy_drift = 0.1
democratic_acceptance = 50
social_democracy_acceptance = 50
conservative_acceptance = 50

}
}

SAU_sharia = {

    allowed = {
      always = no
    }

    allowed_civil_war = {
      always = yes
    }

    removal_cost = -1

    modifier = {
      conscription = 0.2
      war_support_factor = 0.2
      fascism_drift = 0.1
      fascism_acceptance = 50
      democratic_acceptance = -50
      conservative_acceptance = -50
      social_democracy_acceptance = -50
      army_org_Factor = 0.2
      planning_speed = 0.2
    }
  }

SAU_jihad = {

allowed = {
always = no
}

allowed_civil_war = {
always = yes
}

removal_cost = -1

modifier = {
army_org_Factor = 0.2
planning_speed = 0.2
stability_factor = 0.05
war_support_factor = 0.1

}
}

SAU_beacon = {

allowed = {
always = no
}

allowed_civil_war = {
always = yes
}

removal_cost = -1

modifier = {
army_org_Factor = 0.4
planning_speed = 0.3
stability_factor = 0.1
war_support_factor = 0.05
production_speed_arms_factory_factor = 0.1
production_speed_dockyard_factor = 0.2
production_speed_naval_base_factor = 0.3
production_speed_coastal_bunker_factor = 0.4
production_speed_bunker_factor = 0.4
production_speed_air_base_factor = 0.4
production_speed_anti_air_building_factor = 0.1
production_speed_radar_station_factor = 0.3
production_speed_fuel_silo_factor = 0.5
production_speed_synthetic_refinery_factor = 0.1

}
}

SAU_oil = {

allowed = {
always = no
}

allowed_civil_war = {
always = yes
}

removal_cost = -1

modifier = {
army_org_Factor = 0.1
production_speed_fuel_silo_factor = 0.1
production_speed_synthetic_refinery_factor = 0.1
max_fuel_factor = 0.1
fuel_gain_factor = 0.1
}

}

SAU_embargo = {

allowed = {
always = no
}

allowed_civil_war = {
always = yes
}

removal_cost = -1

modifier = {
army_org_Factor = -0.1
production_speed_fuel_silo_factor = -0.05
production_speed_synthetic_refinery_factor = -0.05
max_fuel_factor = -0.1
fuel_gain_factor = -0.1
}

}

SAU_terrorism = {

allowed = {
always = no
}

allowed_civil_war = {
always = yes
}

removal_cost = -1

modifier = {
army_org_Factor = -0.1
war_support_factor = -0.05
production_speed_arms_factory_factor = -0.1
production_speed_dockyard_factor = -0.1
production_speed_naval_base_factor = -0.1
production_speed_coastal_bunker_factor = -0.1
production_speed_bunker_factor = -0.1
production_speed_air_base_factor = -0.1
production_speed_anti_air_building_factor = -0.1
production_speed_radar_station_factor = -0.1
production_speed_fuel_silo_factor = -0.1
production_speed_synthetic_refinery_factor = -0.1

}

}

SAU_comm = {

allowed = {
always = no
}

allowed_civil_war = {
always = yes
}

removal_cost = -1

modifier = {
army_org_Factor = 0.1
planning_speed = 0.1
stability_factor = 0.1
communism_drift = 0.05

}
}

SAU_monarch = {

allowed = {
always = no
}

allowed_civil_war = {
always = yes
}

removal_cost = -1

modifier = {
army_org_Factor = 0.1
planning_speed = 0.1
stability_factor = 0.1
monarchism_drift = 0.02

}
}

SAU_production = {

allowed = {
always = no
}

allowed_civil_war = {
always = yes
}

removal_cost = -1

modifier = {
consumer_goods_factor = 0.05
war_support_factor = -0.05
production_speed_arms_factory_factor = 0.1
production_speed_dockyard_factor = 0.1
production_speed_naval_base_factor = 0.1
production_speed_coastal_bunker_factor = 0.1
production_speed_bunker_factor = 0.1
production_speed_air_base_factor = 0.1
production_speed_anti_air_building_factor = 0.1
production_speed_radar_station_factor = 0.1
production_speed_fuel_silo_factor = 0.1
production_speed_synthetic_refinery_factor = 0.1

}
}

}

political_advisor = {

  SAU_aymanjafri  = {

    picture = generic_political_advisor_arab_2

    allowed = {
      original_tag = SAU
    }

    traits = { compassionate_gentleman }
  }

  SAU_wajeedaradi = {

    picture = generic_political_advisor_arab_3

    allowed = {
      original_tag = SAU
    }

    traits = { quartermaster_general }
  }

  SAU_muhannadabdullah = {

    picture = generic_political_advisor_arab_1

    allowed = {
      original_tag = SAU
    }

    traits = { war_industrialist }
  }
}

army_chief = {

  SAU_talalsyed = {

    picture = generic_army_arab_2

    allowed = {
      original_tag = SAU
    }

    traits = { army_chief_morale_2 }

    ai_will_do = {
      factor = 1
    }
  }


  SAU_maymunahsalah = {

    picture = generic_army_arab_2

    allowed = {
      original_tag = SAU
    }

    traits = { army_chief_defensive_2 }

    ai_will_do = {
      factor = 1
    }
  }

}

air_chief = {


  SAU_adifaahghazal = {

    picture = generic_air_arab_2

    allowed = {
      original_tag = SAU
    }

    traits = { air_chief_reform_2 }

    ai_will_do = {
      factor = 1
    }
  }


  SAU_rahmanhashmi = {

    picture = generic_air_arab_3

    allowed = {
      original_tag = SAU
    }

    traits = { air_chief_all_weather_2 }

    ai_will_do = {
      factor = 1
    }
  }
}

navy_chief = {


  SAU_hamidben = {

    picture = generic_navy_arab_1

    allowed = {
      original_tag = SAU
    }

    traits = { navy_chief_maneuver_2 }

    ai_will_do = {
      factor = 1
    }
  }


  SAU_khaldunawan = {

    picture = generic_navy_arab_3

    allowed = {
      original_tag = SAU
    }

    traits = { navy_chief_commerce_raiding_2 }

    ai_will_do = {
      factor = 1
    }
  }
}

high_command = {

  SAU_wazirsayed = {
    ledger = army

    picture = generic_army_arab_3

    allowed = {
      original_tag = SAU
    }

    traits = { army_regrouping_2 }

    ai_will_do = {
      factor = 1
    }
  }

  SAU_qindeelashraf = {
    ledger = army

    picture = generic_army_arab_1

    allowed = {
      original_tag = SAU
    }

    traits = { army_concealment_2 }

    ai_will_do = {
      factor = 1
    }
  }

  SAU_shaquellammar = {
    ledger = air

    picture = generic_air_arab_3

    allowed = {
      original_tag = SAU
    }

    traits = { air_strategic_bombing_2 }

    ai_will_do = {
      factor = 1
    }
  }


  MON_haidav = {
    ledger = navy

    picture = generic_navy_arab_1

    allowed = {
      original_tag = SAU
    }

    traits = { navy_naval_air_defense_2 }

    ai_will_do = {
      factor = 1
    }
  }
}



theorist = {
  SAU_usmansamaan = {
    ledger = army

    picture = generic_army_arab_5

    allowed = {
      original_tag = SAU
    }

    research_bonus = {
      land_doctrine = 0.10
    }

    traits = { military_theorist }
  }

  SAU_rabahsalehi = {
    ledger = navy

    picture = generic_navy_arab_3

    allowed = {
      original_tag = SAU
    }

    research_bonus = {
      naval_doctrine = 0.10
    }

    traits = { naval_theorist }
  }

  SAU_altairmohammadi = {
    ledger = air

    picture = generic_air_arab_2

    allowed = {
      original_tag = SAU
    }

    research_bonus = {
      air_doctrine = 0.10
    }

    traits = { air_warfare_theorist }
  }

}
}
