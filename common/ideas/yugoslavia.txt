ideas = {

	country = {
	
		YUG_idea_serbian_general_staff = {			
			picture = YUG_serbian_general_staff			
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}			
			removal_cost = -1					
			modifier = { #it was only used for Yugo, because only Serbs were officiers, so it was harder to find them
				max_planning = 0.1
			}
		}
		SER_idea_tractor = {
			picture = generic_agrarian_society		
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}			
			removal_cost = -1					
			modifier = {
				consumer_goods_factor = -0.05
			}
		}
		SER_persecution_of_turks = {
			picture = prc_low_popular_support3
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}	
			removal_cost = -1					
			modifier = {
				stability_factor = -0.1
				industrial_capacity_factory = -0.05
			}
		}
		SER_idea_russian_advisors = {
			picture = SER_russian_advisors
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}			
			removal_cost = -1					
			modifier = {
				army_morale_factor = 0.05
				winter_attrition_factor = -0.25
			}
		}
		SER_idea_chetniks = {
			picture = chi_war_of_resistance3
			allowed = {
				always = no
			}	
			available = {
			}
			allowed_civil_war = {
			}			
			removal_cost = -1					
			modifier = {
				boost_resistance_factor = 0.25
				out_of_supply_factor = -0.1
				attrition = -0.1
			}
		}
		SER_idea_narodna_skupstina = {			
			picture = generic_democratic_drift_bonus			
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}			
			removal_cost = -1					
			modifier = {
				social_democracy_drift = 0.01
				conservative_drift = 0.01
			}
		}
		SER_idea_officer_party = {		
			picture = generic_fascism_drift_bonus			
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}			
			removal_cost = -1					
			modifier = {
				fascism_drift = 0.01
			}
		}
		SER_idea_royal_manifesto = {		
			picture = generic_neutrality_drift_bonus		
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}			
			removal_cost = -1					
			modifier = {
				monarchism_drift = 0.03
				foreign_subversive_activites = -0.25
			}
		}	
		SER_idea_alexander_i = {			
			picture = alexander_i
			allowed = {
				tag = YUG
			}			
			allowed_civil_war = {
			}
			available = { NOT = { has_socialist_government = yes } }
			removal_cost = -1					
			modifier = {
				political_power_gain = 0.05
				social_democracy_drift = 0.01
				democratic_drift = 0.01
				conservative_drift = 0.01
			}
		}		
		SER_idea_alexander_i_1 = {			
			picture = alexander_i		
			allowed = {
				tag = YUG
			}			
			allowed_civil_war = {
			}
			available = { NOT = { has_socialist_government = yes } }		
			removal_cost = -1					
			modifier = {
				political_power_gain = 0.05
				social_democracy_drift = 0.01
				democratic_drift = 0.01
				conservative_drift = 0.01
				guarantee_cost = -0.75
			}
		}
		SER_idea_orthodox_state = {	
			picture = YUG_orthodox_church_support		
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}			
			removal_cost = -1					
			modifier = {
				surrender_limit = 0.15
				drift_defence_factor = 0.5
			}
		}
		SER_idea_preparation_for_libartion = {			
			picture = generic_reserve_divisions			
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}			
			removal_cost = -1
			
			available = {
				OR = {
					NOT = { has_global_flag = kis_firstbalkanwar_victory }
					NOT = { has_global_flag = kis_war }
				}
				SER = { is_puppet = no }
			}
			
			modifier = {
				conscription = 0.075
				training_time_army_factor = -0.25
			}
		}
		SER_idea_balkan_liberation = {
            picture = generic_morale_bonus
			allowed = {
                always = no
            }			
            removal_cost = -1 
			
			available = {
				OR = {
					NOT = { has_global_flag = kis_firstbalkanwar_victory }
					NOT = { has_global_flag = kis_war }
				}
			}
			
			modifier = {
				army_speed_factor = 1
				army_org_Factor = 1
				army_core_attack_factor = 1
				army_core_defence_factor = 1
			}

			targeted_modifier = {
				tag = TUR
				attack_bonus_against = 2
				defense_bonus_against = 2
			}
        }
		
		SER_idea_national_gendarmerie = {
			picture = generic_secret_police		
			allowed = {
				always = no
			}			
			allowed_civil_war = {
			}			
			removal_cost = -1
			
			available = {
				SER = { is_puppet = no }
			}
			
			modifier = {
				experience_gain_army = 0.05
				resistance_damage_to_garrison = -0.1
			}
		}

		YUG_anti_german_military = {
			
			picture = generic_fascism_banned

			allowed = {
				always = no
			}

			allowed_civil_war = {
				NOT = {
					has_government = fascism
				}
			}

			removal_cost = -1
			
			modifier = {
				drift_defence_factor = 0.05
				war_support_factor = 0.05
			}
		}
		
		YUG_idea_serbian_general_staff = {
			
			picture = YUG_serbian_general_staff

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				military_leader_cost_factor = 0.50
				max_planning = 0.1
			}
		}

		YUG_idea_yugoslavian_general_staff = {
			
			picture = YUG_yugoslavian_general_staff

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				max_planning = 0.1
			}
		}
		
		YUG_partisan_power = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				has_government = fascism
			}

			removal_cost = -1

			picture = ger_the_great_red_menace
			
			cancel = {
				OR = {
					has_war = no
					AND = {
						tag = PRC
						has_full_control_of_state = 622
					}
					AND = {
						tag = PRC
						any_state = {
							is_core_of = ROOT
							is_owned_by = ROOT
							is_controlled_by = ROOT
							is_coastal = yes
						}
					}
				}
			}
	
			modifier = {
				surrender_limit = 0.5
				army_speed_factor = 0.8
				supply_consumption_factor = -0.6
				army_org_Factor = 0.9
				army_core_attack_factor = 2
				army_core_defence_factor = 3
				ai_focus_aggressive_factor = 3
				ai_badass_factor = 2
			}
		}

		YUG_idea_federal_defense_council = {
			
			picture = YUG_federal_defense_council

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				max_planning = 0.1
				defence = 0.1
			}
		}
		
		YUG_idea_expanded_mining_industry = {
			
			picture = generic_exploit_mines

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				local_resources_factor = 0.10
			}
		}
		
		YUG_socialist_commisars = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				OR = {
					has_socialist_government = yes
				}
			}

			removal_cost = -1
			
			modifier = {
				training_time_army_factor = -0.1
				compliance_gain = -0.2
			}

			
			picture = chi_army_corruption3
		
		}
		
		YUG_idea_local_self_management = {
			
			picture = generic_local_self_management

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				production_speed_buildings_factor = 0.05
			}
		}
		
		YUG_idea_central_management = {
			
			picture = generic_central_management

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				industrial_capacity_factory = 0.05
			}
		}
		
		YUG_support_greek_communists_idea = {
		
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				OR = {
					has_socialist_government = yes
					has_government = fascism
				}
			}
			
			removal_cost = -1
			
			modifier = {
				communism_drift = 0.01
			}

			
			picture = generic_communism_drift_bonus
		
		}
		
		YUG_statism = {
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				OR = {
					has_socialist_government = yes
					has_government = fascism
				}
			}

			removal_cost = -1
			
			modifier = {
				production_speed_buildings_factor = 0.10
			}

			
			picture = generic_production_bonus

		}
		
		YUG_federal_economy = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				OR = {
					has_socialist_government = yes
				}
			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.05
				stability_factor = 0.1
			}

			
			picture = generic_agrarian_reform
		
		}
		
		YUG_support_from_soviet = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				OR = {
					has_socialist_government = yes
				}
			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.05
				stability_factor = 0.05
			}

			
			picture = generic_communism_drift_bonus
		
		}
		
		YUG_support_prince_paul = {
		
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				OR = {
					OR = {
						has_government = neutrality
						has_government = monarchism
					}
				}
			}
						
			cancel = {
				NOT = {
					OR = {
						has_government = neutrality
						has_government = monarchism
					}
				} 
			}
	
			
			removal_cost = -1
			
			modifier = {
				monarchism_drift = 0.2
			}

			
			picture = idea_HUN_hungarian_monarchy
		
		}
		
		YUG_socialist_economy_idea = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				OR = {
					has_socialist_government = yes
				}
			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.05
				production_factory_efficiency_gain_factor = 0.1
			}

			
			picture = generic_agrarian_reform
		
		}
		
		YUG_agricultural_development = {
		
			picture = generic_agrarian_reform

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				production_factory_efficiency_gain_factor = 0.1
			}
		}
		
		YUG_idea_peasant_councils = {

			picture = generic_communism_drift_bonus
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				has_socialist_government = yes
			}

			removal_cost = -1

			picture = generic_communism_drift_bonus
			
			modifier = {
				communism_drift = 0.2
				unionism_drift = 0.1
			}
		}
		
		YUG_support_tito = {
			
			picture = generic_communism_drift_bonus
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				has_socialist_government = yes
			}

			removal_cost = -1

			picture = generic_communism_drift_bonus
			
			modifier = {
				communism_drift = 0.1
				unionism_drift = 0.1
			}
		}
		
		
		YUG_idea_economic_aid = {

			picture = generic_goods_red_bonus
			
			available = {
				YUG = {
					NOT = { has_war_with = SOV }
					exists = yes
					has_capitulated = no
				}
			}
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				has_socialist_government = yes
			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.05
			}
		}

		YUG_artillery_regiments = {
			
			picture = generic_artillery_regiments

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				
			}

			equipment_bonus = {
				artillery_equipment = {
					build_cost_ic = -0.05
					instant = yes
				}
			}
		}
		
		YUG_support_peter_ii = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				OR = {
					has_democratic_government = yes
				}
			}

			removal_cost = -1
			
			modifier = {
				democratic_drift = 0.05
				conservative_drift = 0.05
				social_democracy_drift = 0.05
			}

			
			picture = democratic_drift
		
		}
		
		YUG_british_support = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				OR = {
					has_democratic_government = yes
				}
			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.05
				stability_factor = 0.05
			}

			
			picture = idea_stiff_upper_lip
		
		}
		
		YUG_democracy_for_yugoslavia = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				OR = {
					has_democratic_government = yes
				}
			}

			removal_cost = -1
			
			modifier = {
				democratic_drift = 0.02
				conservative_drift = 0.02
				social_democracy_drift = 0.02
				stability_factor = 0.1
			}

			
			picture = democratic_drift
		
		}
		
		YUG_liberalism = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				NOT = {
					has_government = fascism
				}
			}

			removal_cost = -1
			
			modifier = {
				stability_factor = 0.05
				production_speed_buildings_factor = 0.05
			}

			
			picture = generic_agrarian_reform
		
		}
		
		YUG_research_pact_idea = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				OR = {
					has_socialist_government = yes
					has_government = fascism
				}
			}

			removal_cost = -1
			
			modifier = {
				research_speed_factor = -0.05
			}

			
			picture = generic_research_bonus
		
		}
		
		YUG_unitary_parliamentary_monarchy = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			picture = generic_deal_with_the_devil2
			
			modifier = {
				stability_factor = 0.1
				political_power_gain = 0.15
			}
		
		}
		
		YUG_new_constitution = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			picture = idea_generic_constitutional_guarantees
			
			modifier = {
				stability_factor = 0.1
				political_power_gain = 0.15
			}
		
		}
		
		YUG_reform_economy = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				NOT = {
					has_government = fascism
				}
			}

			removal_cost = -1
			
			modifier = {
				stability_factor = 0.1
				consumer_goods_factor = -0.05
			}

			
			picture = generic_production_bonus
		
		}
		
		YUG_economy_of_war = {
		
			picture = generic_license_production
			
			allowed_civil_war = {
				always = yes
			}
			
			modifier = {
				consumer_goods_factor = -0.05
				conversion_cost_civ_to_mil_factor = -0.2
				production_speed_arms_factory_factor = 0.1
				industrial_capacity_factory = 0.025
				industrial_capacity_dockyard = 0.025
			}
		
		}

		
		YUG_idea_foreign_capital = {
			
			picture = generic_foreign_capital

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.05
			}
		}
		
		YUG_idea_orthodox_church_support = {
			
			picture = YUG_orthodox_church_support

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				political_power_factor = 0.25
			}
		}
		
		YUG_idea_all_yugoslavian_regiments = {
			
			picture = YUG_all_yugoslavian_regiments

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				conscription = 0.01
			}
		}
		
		YUG_idea_croatian_opposition = {
			
			picture = FRA_scw_intervention_republicans_focus

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				stability_factor = -0.25
			}
		}

		YUG_idea_macedonian_opposition = {
			
			picture = generic_volunteer_expedition_bonus

			allowed = {
				always = no
				country_exists = BUL
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				justify_war_goal_time = 0.25
			}
		}
		
		YUG_idea_slovene_nationalism = {
			
			picture = generic_morale_bonus

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				stability_factor = -0.1
				production_factory_efficiency_gain_factor = -0.05
			}
		}

		
		YUG_idea_croats_suppressed = {
			
			picture = generic_secret_police

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				political_power_factor = -0.25
			}
		}
		
		YUG_invest_in_economy = {
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				NOT = {
					has_government = fascism
				}
			}

			removal_cost = -1
			
			modifier = {
				stability_factor = 0.05
				production_speed_buildings_factor = 0.05
			}

			
			picture = generic_agrarian_reform

		}
		
		YUG_idea_religious_freedoms_guaranteed = {
			
			picture = generic_constitutional_guarantees

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				stability_factor = 0.1
			}
		}
		
		YUG_idea_local_militias = {
			
			picture = YUG_local_militias

			allowed = {
				always = no
			}

			allowed_civil_war = {

			}

			removal_cost = -1
			
			modifier = {
				conscription = 0.03
			}
		}
		
		YUG_abolish_the_monarchy = {
		
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				OR = {
					OR = {
						has_government = neutrality
						has_government = monarchism
					}
				}
			}
			
			removal_cost = -1
			
			modifier = {
				monarchism_drift = -0.05
				stability_factor = 0.1
			}

			
			picture = NOR_syndicalist_support
		
		}
		
		YUG_establish_anvoj = {
		
			allowed = {
			always = no
			}

			allowed_civil_war = {
				has_socialist_government = yes
			}

			removal_cost = -1

			picture = UoB_militia_idea

			modifier = {
				conscription = 0.05
				army_morale_factor = 0.025
				army_org_Factor = 0.025
				war_support_factor = 0.05
			}
		
		}
		
		YUG_land_reform = {
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				NOT = {
					has_government = fascism
				}
			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = 0.05
			}

			
			picture = generic_agrarian_reform

		}
	}

	political_advisor = {

		SER_mihailo_gavrilovic = { 
			available = { has_completed_focus = SER_Strengthen_civilian_authorities } 
			allowed = { original_tag = YUG }
			traits = { ideological_crusader } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_stojan_protic = { 
			allowed = { original_tag = YUG } 
			traits = { fortification_engineer }
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            } 
			ai_will_do = { factor = 1 } 
		}
		SER_momcilo_nincic = { 
			allowed = { original_tag = YUG } 
			traits = { armaments_organizer } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_duro_dakovic = { 
			available = { has_completed_focus = SER_Strengthen_civilian_authorities } 
			allowed = { original_tag = YUG } 
			traits = { smooth_talking_charmer } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_milovan_milovanovic = { 
			allowed = { original_tag = YUG } 
			traits = { silent_workhorse } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_milenko_vesnic = { 
			available = { has_completed_focus = SER_Strengthen_civilian_authorities } 
			allowed = { original_tag = YUG } 
			traits = { backroom_backstabber } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_stojan_novakovic = { 
			available = { has_completed_focus = SER_Strengthen_civilian_authorities } 
			allowed = { original_tag = YUG } 
			traits = { compassionate_gentleman } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_lazar_pacu = { 
			allowed = { original_tag = YUG } 
			traits = { quartermaster_general } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_svetozar_pribicevic = { 
			allowed = { original_tag = YUG } 
			traits = { prince_of_terror } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		YUG_slobodan_jovanovic = {

			picture = generic_political_advisor_europe_1
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			allowed = {
				original_tag = YUG
			}
			
			traits = { popular_figurehead }
		}

		YUG_ivan_ribar = {

			picture = generic_political_advisor_europe_5

			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			allowed = {
				original_tag = YUG
			}
			
			traits = { captain_of_industry }
		}

		YUG_milan_nedic = {

			picture = generic_army_europe_2
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			allowed = {
				original_tag = YUG
			}
			
			traits = { prince_of_terror }
		}

		YUG_war_ind = {

			picture = alexander_golovanov
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			allowed = {
				original_tag = YUG
			}
			
			traits = { war_industrialist }
		}
		
		YUG_captain = {

			picture = andrew_cunningham

			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			allowed = {
				original_tag = YUG
			}
			
			traits = { captain_of_industry }
		}
	}

	theorist = {

		SER_jovan_jovanovic_pizon = { 
			allowed = { original_tag = YUG } 
			traits = { military_theorist } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		YUG_ljubomir_maric = {
					
					
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			picture = generic_army_europe_5
			
			research_bonus = {
				land_doctrine = 0.07
			}
			
			traits = { military_theorist }
		}

		YUG_marijan_polic = {
					
					
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			picture = generic_political_advisor_europe_2
			
			research_bonus = {
				naval_doctrine = 0.07
			}
			
			traits = { naval_theorist }
		}
	}
	# MILITARY
	army_chief = {

		SER_radomir_putnik = { 
			allowed = { original_tag = YUG } 
			traits = { army_chief_defensive_1 } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_petar_bojovic = { 
			allowed = { original_tag = YUG } 
			traits = { army_chief_maneuver_1 } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_zivko_pavlovic = { 
			allowed = { original_tag = YUG } 
			traits = { army_chief_organizational_1 } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		YUG_milutin_nedic = {
			
			picture = generic_army_europe_6
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			traits = { army_chief_offensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		YUG_draza_mihailovic = {
			
			picture = generic_army_europe_3
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			traits = { army_chief_defensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		YUG_josip_broz_tito = {
			
			picture = generic_army_europe_2
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			traits = { army_chief_morale_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	air_chief = {
		
		SER_kosta_miletic = { 
			allowed = { original_tag = YUG } 
			traits = { air_close_air_support_1 } 
			available = { has_completed_focus = SER_air_staff } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		YUG_dusan_simovic = {
			
			picture = generic_air_europe_1
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			traits = { air_chief_safety_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		YUG_petar_vukcevic = {
			
			picture = generic_air_europe_2
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			traits = { air_chief_ground_support_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	navy_chief = {

		YUG_bogoljub_ilic = {
			
			picture = generic_navy_europe_2
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			traits = { navy_chief_commerce_raiding_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		YUG_petar_kosic = {
			
			picture = generic_navy_europe_1
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			traits = { navy_chief_decisive_battle_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	high_command = {

		SER_zivojin_misic = { 
			allowed = { original_tag = YUG } 
			traits = { army_regrouping_1 } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_mihailo_rasic = { 
			allowed = { original_tag = YUG } 
			traits = { army_infantry_1 } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_milos_vasic = { 
			allowed = { original_tag = YUG } 
			traits = { army_cavalry_1 } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_petar_pesic = { 
			allowed = { original_tag = YUG } 
			traits = { army_artillery_1 } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		SER_stevan_hadzic = { 
			allowed = { original_tag = YUG } 
			traits = { army_logistics_1 } 
            available = {
                date > 1910.1.1
                date < 1924.1.1
            }
            visible = {
                date > 1910.1.1
                date < 1924.1.1
            }
			ai_will_do = { factor = 1 } 
		}
		YUG_arso_jovanovic = {
			
			picture = generic_army_europe_1
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			

			
			traits = { army_chief_drill_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	
		YUG_mihajlo_lukic = {
			
			picture = generic_army_europe_2

			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			
			traits = { army_logistics_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		YUG_danilo_kalafatovic = {
			
			picture = generic_army_europe_3
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			
			
			traits = { army_chief_maneuver_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		YUG_borivoje_mirkovic = {
			
			picture = generic_air_europe_2
			
			allowed = {
				original_tag = YUG
			}
			
			visible = {
				date > 1924.1.1
				date < 1949.1.1
			}

			available = {
				date > 1924.1.1
				date < 1949.1.1
			}
			

			traits = { air_bomber_interception_1 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	# TECHNOLOGY
	
	naval_manufacturer = { 
		
		designer = yes
		
		beogradsko_brodogradiliste = { 
			allowed = { original_tag = YUG } 
			research_bonus = { naval_equipment = 0.10 } 
			available = { has_completed_focus = SER_birth_of_fleet } 
			traits = { naval_manufacturer } 
			picture = generic_naval_manufacturer_2 
		}

		jadransko_brodogradiliste = {
			
			picture = generic_naval_manufacturer_2

			
			allowed = {
				original_tag = YUG
			}
			
			research_bonus = {
				naval_equipment = 0.10
			}
			
			equipment_bonus = {
				# generic shipyard?
			}
			
			traits = { naval_manufacturer }
			
			modifier = {
			}
		}
	}
	
	aircraft_manufacturer = { 
		
		designer = yes
		
		ikarus = {
			allowed = { original_tag = YUG } 
			research_bonus = { air_equipment = 0.10 } 
			traits = { light_aircraft_manufacturer } 
			picture = generic_air_manufacturer_1
		}
		mihailo_marcep_airplane_workshop = { 
			allowed = { original_tag = YUG } 
			research_bonus = { air_equipment = 0.10 } 
			traits = { light_aircraft_manufacturer } 
			picture = generic_air_manufacturer_3 
		}
		YUG_rogozarski = {

			picture = generic_air_manufacturer_2
			
			available = {
				has_completed_focus = YUG_rogozarski
			}

			allowed = {
				original_tag = YUG
			}
			
			research_bonus = {
				air_equipment = 0.10
			}
			
			traits = { medium_aircraft_manufacturer }
			
			equipment_bonus = {
				# trainers and fighter airplanes Rogozarski PVT
			}
			
			ai_will_do = {
				factor = 1
			}
		}

		YUG_zmaj = {

			picture = generic_air_manufacturer_1
			
			available = {
				has_completed_focus = YUG_zmaj				
			}			

			allowed = {
				original_tag = YUG
			}
			
			research_bonus = {
				air_equipment = 0.10
			}
			
			traits = { heavy_aircraft_manufacturer }
			
			equipment_bonus = {
				# trainers and bombers
			}
			
			ai_will_do = {
				factor = 1
			}
		}
	}
	materiel_manufacturer = {
			
		designer = yes
		
		zastava_arms = { 
			allowed = { original_tag = YUG } 
			research_bonus = { infantry_weapons=0.10 } 
			traits = { infantry_equipment_manufacturer } 
		}
		barutana_obilicevo = { 
			allowed = { original_tag = YUG } 
			research_bonus = { infantry_weapons=0.10 } 
			traits = { infantry_equipment_manufacturer } 
			picture = generic_infantry_equipment_manufacturer_3 
		}
		topolivnica_kragujevac = { 
			allowed = { original_tag = YUG } 
			research_bonus = { infantry_weapons=0.10 } 
			traits = { infantry_equipment_manufacturer } 
			picture = generic_infantry_equipment_manufacturer_1 
		}
	}
	industrial_concern = {
		designer = yes 
		memorandum_teleoptik = { 
			allowed = { original_tag = YUG } 
			research_bonus = {electronics = 0.10} 
			traits = { electronics_concern } 
			picture = generic_tank_manufacturer_2 
		}
		serbian_state_railways = { 
			allowed = { original_tag = YUG } 
			research_bonus = {industry = 0.05} 
			cost = 75
			traits = { industrial_concern } 
			picture = generic_tank_manufacturer_3 
		}
		sartid_smederevo = { 
			allowed = { original_tag = YUG }
			research_bonus = {industry = 0.10} 
			traits = { industrial_concern } 
			picture = generic_tank_manufacturer_1 
		}
	}
}