ideas = {

	political_advisor = {
		VIN_jules_brevie = {
			picture = generic_political_advisor_europe_1
			allowed = {
				original_tag = VIN
			}
			traits = { captain_of_industry }
		}
		
		VIN_fortification_engineer = {
			allowed = {
				original_tag = VIN
			}
			traits = { smooth_talking_charmer }
		}

		VIN_war_industrialist = {
			allowed = {
				original_tag = VIN
			}
			traits = { compassionate_gentleman }
		}
	}

	army_chief = {
		VIN_voong_huu_trieu = {
			picture = generic_army_asia_4
			allowed = {
				original_tag = VIN
			}
			traits = { army_chief_defensive_1 }
			
			ai_will_do = {
				factor = 1
			}
		}		
	}
	
	air_chief = {
		VIN_lai_de = {
			picture = generic_air_asia_1
			allowed = {
				original_tag = VIN
			}
			traits = { air_chief_all_weather_1 }
			ai_will_do = {
				factor = 1
			}
		}
	}
	
	navy_chief = {
		VIN_hang_dinh_xuan = {
			picture = generic_navy_asia_1
			allowed = {
				original_tag = VIN
			}
			traits = { navy_chief_commerce_raiding_1 }
			ai_will_do = {
				factor = 1
			}
		}
	}
	
	high_command = {
		VIN_voong_van_dao = {
			picture = generic_army_asia_1
			allowed = {
				original_tag = VIN
			}
			traits = { army_logistics_2 }
			ai_will_do = {
				factor = 1
			}
		}
		
		VIN_tuan_ngoc_minh = {
			picture = generic_army_asia_2
			allowed = {
				original_tag = VIN
			}
			traits = { army_infantry_2 }
			ai_will_do = {
				factor = 1
			}
		}
		
		VIN_lai_thao = {
			picture = generic_army_asia_3
			allowed = {
				original_tag = VIN
			}
			traits = { army_entrenchment_2 }
			ai_will_do = {
				factor = 1
			}
		}
	}
	
	theorist = {
		VIN_jean_tassigny = {
			picture = generic_army_europe_1
			allowed = {
				original_tag = VIN
			}
			research_bonus = {
				land_doctrine = 0.07
			}
			traits = { military_theorist }
		}
		
		VIN_georges_catroux = {
			picture = generic_air_europe_1
			allowed = {
				original_tag = VIN
			}
			research_bonus = {
				air_doctrine = 0.07
			}
			traits = { air_warfare_theorist }
		}

		VIN_jean_decoux = {
			picture = generic_navy_europe_1
			allowed = {
				original_tag = VIN
			}
			research_bonus = {
				naval_doctrine = 0.07
			}
			traits = { naval_theorist }
		}

	}
	
}