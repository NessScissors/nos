################
##### BRA ######
################

BRA_military_interventions_category = {
	allowed = { original_tag = BRA } 
	visible = { has_focus_tree = ww1_brazl }
	icon = GFX_decision_generic_assassination
	picture = GFX_decision_bra_politica_das_salvacoes
 	priority = 80
}
BRA_contestado_war = {
	allowed = { original_tag = BRA } 
	visible = { has_focus_tree = ww1_brazl }
	icon = GFX_decision_category_border_conflicts
	picture = GFX_decision_bra_contestado_war
 	priority = 90
}
BRA_spreading_revolution_category = {
	allowed = { original_tag = BRA } 
	visible = { has_focus_tree = ww1_brazl }
	icon = saf_anti_colonialist_crusade
 	priority = 80
}


BRA_cat_proalcool = {
	
	picture = GFX_decision_cat_proalcool
	visible = { has_focus_tree = ww2_brazl }
	icon = generic_political_actions
	allowed = {
		original_tag = BRA
	}
}

BRA_uiracu = {

	picture = GFX_decision_cat_uiracu
	visible = { has_focus_tree = ww2_brazl }
	icon = ger_military_buildup
	allowed = {
		original_tag = BRA
	}
}

BRA_elect_neutrality = {
	
	picture = GFX_decision_cat_elect_neutrality
	visible = { has_focus_tree = ww2_brazl }
	icon = generic_political_actions
	allowed = {
		original_tag = BRA
	}
}

BRA_elect_democratic = {

	picture = GFX_decision_cat_elect_democratic
	visible = { has_focus_tree = ww2_brazl }
	icon = generic_democracy
	allowed = {
		original_tag = BRA
	}
}

BRA_elect_fascism = {

	picture = GFX_decision_cat_elect_fascism
	visible = { has_focus_tree = ww2_brazl }
	icon = generic_fascism
	allowed = {
		original_tag = BRA
	}
}

BRA_elect_communism = {

	picture = GFX_decision_cat_elect_communism
	visible = { has_focus_tree = ww2_brazl }
	icon = generic_communism
	allowed = {
		original_tag = BRA
	}
}


BRA_voz_ligada_cat = {
	
	picture = GFX_decision_cat_voz_ligada
	visible = { has_focus_tree = ww2_brazl }
	icon = generic_political_actions
	allowed = {
		original_tag = BRA
	}
}