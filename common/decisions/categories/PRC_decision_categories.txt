PRC_infiltrate_nationalist_areas = {
	icon = infiltration
	picture = GFX_decision_cat_picture_chi_infiltration
	allowed = {
		is_potentially_communist_china = yes
	}

	visible = {
		has_completed_focus = PRC_infiltration		
	}
}

PRC_anti_japanese_expedition = {
	icon = infiltration
	picture = GFX_decision_cat_picture_chi_infiltration
	allowed = {
		is_potentially_communist_china = yes
	}

	visible = {
		has_completed_focus = PRC_anti_japanese_expedition
		OR = {
			609 = {
				OR = {
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
						OR = {
							original_tag = MAN
							original_tag = MEN
							original_tag = JAP
						}
					}
					PRC_is_infiltrated_by_PREV = yes
				}
			}
			610 = {
				OR = {
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
						OR = {
							original_tag = MAN
							original_tag = MEN
							original_tag = JAP
						}
					}
					PRC_is_infiltrated_by_PREV = yes
				}
			}
			611 = {
				OR = {
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
						OR = {
							original_tag = MAN
							original_tag = MEN
							original_tag = JAP
						}
					}
					PRC_is_infiltrated_by_PREV = yes
				}
			}
			612 = {
				OR = {
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
						OR = {
							original_tag = MAN
							original_tag = MEN
							original_tag = JAP
						}
					}
					PRC_is_infiltrated_by_PREV = yes
				}
			}
			714 = {
				OR = {
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
						OR = {
							original_tag = MAN
							original_tag = MEN
							original_tag = JAP
						}
					}
					PRC_is_infiltrated_by_PREV = yes
				}
			}
			715 = {
				OR = {
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
						OR = {
							original_tag = MAN
							original_tag = MEN
							original_tag = JAP
						}
					}
					PRC_is_infiltrated_by_PREV = yes
				}
			}
		}
	}
}

PRC_border_clashes = {
	icon = military_operation
	allowed = {
		is_potentially_communist_china = yes
	}
}