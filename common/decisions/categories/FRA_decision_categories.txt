################
##### FRA ######
################

FRA_vichy_france = {
	
	visible = {
		
	}

	allowed = {
		is_literally_france = yes
	}
}

FRA_spanish_intervention_category = {
	allowed = {
		is_literally_france = yes
	}
	visible = {
		has_completed_focus = FRA_intervention_in_spain
		SPR_scw_in_progress = yes
		has_focus_tree = ww2_france
	}
}

VIC_concessions_to_the_germans = {
	allowed = {
		is_literally_france = yes
	}
	visible = {
		has_focus_tree = ww2_france_vichy
		OR = {
			has_completed_focus = VIC_concessions_to_the_germans
			controls_state = 16
		}
	}
}

FRA_intervention_in_overseas_territories = {
	allowed = {
		is_literally_france = yes
	}
	visible = {
		has_focus_tree = ww2_france_free
		has_completed_focus = FRA_appeal_to_overseas_territories
	}
}

FRA_weapons_purchases_category = {
	allowed = {
		is_literally_france = yes
	}
	visible = {
		has_country_flag = FRA_arms_purchases_permitted
		NOT = { has_war_with = USA }
	}
}

FRA_decolonization = {
	allowed = { is_literally_france = yes }
}