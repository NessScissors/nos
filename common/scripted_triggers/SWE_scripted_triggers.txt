#scripted trigger for Sweden

is_democratic_puppet = {
	OR = {
		has_autonomy_state = autonomy_free_regime
		has_autonomy_state = autonomy_protected_state
		has_autonomy_state = autonomy_federal_puppet
		has_autonomy_state = autonomy_territory
	}
}

is_available_strat_SWE = {
	OR = {
		AND = {
			has_tech = strategic_bomber1
			SWE = { 
				NOT = { has_tech = strategic_bomber2 }
			}
		}
		AND = {
			has_tech = strategic_bomber2
			SWE = { 
				NOT = { has_tech = strategic_bomber2 }
			}
		}
		AND = {
			has_tech = strategic_bomber3
			SWE = { 
				NOT = { has_tech = strategic_bomber3 }
			}
		}
	}
	NOT = {
		has_war_with = SWE
	}
}

is_available_nav_bomber_SWE = {
	OR = {
		AND = {
			has_tech = naval_bomber1
			SWE = { 
				NOT = { has_tech = naval_bomber1 }
			}
		}
		AND = {
			has_tech = naval_bomber2
			SWE = { 
				NOT = { has_tech = naval_bomber2 }
			}
		}
		AND = {
			has_tech = naval_bomber3
			SWE = { 
				NOT = { has_tech = naval_bomber3 }
			}
		}
	}
	NOT = {
		has_war_with = SWE
	}
}

is_available_cas_SWE = {
	OR = {
		AND = {
			has_tech = CAS1
			SWE = { 
				NOT = { has_tech = CAS1}
			}
		}
		AND = {
			has_tech = CAS2
			SWE = { 
				NOT = { has_tech = CAS2}
			}
		}
		AND = {
			has_tech = CAS3
			SWE = { 
				NOT = { has_tech = CAS3}
			}
		}
	}
	NOT = {
		has_war_with = SWE
	}
}

is_available_tac_SWE = {
	OR = {
		AND = {
			has_tech = tactical_bomber1
			SWE = { 
				NOT = { has_tech = tactical_bomber1}
			}
		}
		AND = {
			has_tech = tactical_bomber2
			SWE = { 
				NOT = { has_tech = tactical_bomber2}
			}
		}
		AND = {
			has_tech = tactical_bomber3
			SWE = { 
				NOT = { has_tech = tactical_bomber3}
			}
		}
	}
	NOT = {
		has_war_with = SWE
	}
}