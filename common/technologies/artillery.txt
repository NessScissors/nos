technologies = {

	gw_artillery = {	#Полевая артиллерия - наступательная артиллерия. Минометы - оборонительная артиллерия. Тяжелая артиллерия - разрушительная(для штурма укреплений).

		enable_equipments = {
			wwi_artillery_equipment_1
		}
		
		start_year = 1910
		
		path = {
			leads_to_tech = wwi_interwar_artillery
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = howitzer1
			research_cost_coeff = 1
		}
		research_cost = 1.5
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 0 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	wwi_interwar_artillery = {

		artillery = {
			soft_attack = 0.1
		}
		artillery_brigade = {
			soft_attack = 0.1
		}
		mot_artillery_brigade = {
			soft_attack = 0.1
		}
		
		path = {
			leads_to_tech = wwi_artillery1
			research_cost_coeff = 1
		}
		
		research_cost = 1.5

		start_year = 1910
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 2 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2

			modifier = {
				factor = 2
				date > "1912.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1913.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1914.1.1"
			}
		}
	}
	
	wwi_artillery1 = {
	
		enable_equipments = {
			wwi_artillery_equipment_2
		}
		
		path = {
			leads_to_tech = wwi_artillery2
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1914
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 4 }
		}
		
		path = {
			leads_to_tech = wwi_rocket_artillery #mortars
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = wwi_interwar_antitank
			research_cost_coeff = 1
		}		
		path = {
			leads_to_tech = wwi_interwar_antiair
			research_cost_coeff = 1
		}
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 25
			modifier = {
				date < 1913.1.1
				factor = 0
			}
			modifier = {
				factor = 2
				date > "1913.1.9"
			}
			
			modifier = {
				factor = 2
				date > "1914.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1914.1.6"
			}
		}
	}
	
	wwi_artillery2 = {

		artillery = {
			soft_attack = 0.1
		}
		artillery_brigade = {
			soft_attack = 0.1
		}
		mot_artillery_brigade = {
			soft_attack = 0.1
		}
		
		path = {
			leads_to_tech = wwi_artillery3
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1916
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 6 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1915.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1916.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1917.1.1"
			}
		}
	}
	
	wwi_artillery3 = {

		artillery = {
			soft_attack = 0.1
		}
		artillery_brigade = {
			soft_attack = 0.1
		}
		mot_artillery_brigade = {
			soft_attack = 0.1
		}
		
		path = {
			leads_to_tech = wwi_artillery4
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1918
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 8 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1917.1.6"
			}
			
			modifier = {
				factor = 5
				date > "1919.1.1"
			}
		}
	}
	
	wwi_artillery4 = {
	
		enable_equipments = {
			wwi_artillery_equipment_3
		}

		#artillery = { 
		#	soft_attack = 0.1
		#}
		#artillery_brigade = {
		#	soft_attack = 0.1
		#}
		
		path = {
			leads_to_tech = wwi_artillery5
			research_cost_coeff = 1
		}
		
		research_cost = 2

		start_year = 1920
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 10 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1919.1.9"
			}
			
			modifier = {
				factor = 2
				date > "1920.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1920.1.6"
			}
		}
	}
	
	howitzer1 = { #TGW Redux

		enable_equipments = {
			howitzer_equipment_1
		}
		
		start_year = 1910
		
		path = {
			leads_to_tech = howitzer2
			research_cost_coeff = 1
		}
		
		research_cost = 2.0
		
		folder = {
			name = artillery_folder
			position = { x = 6 y = 2 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	howitzer2 = { #TGW Redux

		howitzer = {
			soft_attack = 0.1
		}
		howitzer_brigade = {
			soft_attack = 0.1
		}
		
		start_year = 1914
		
		path = {
			leads_to_tech = howitzer3
			research_cost_coeff = 1
		}
		
		research_cost = 2.0
		
		folder = {
			name = artillery_folder
			position = { x = 6 y = 4 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				date < 1914.1.1
				factor = 0
			}
		}
	}
	howitzer3 = { #TGW Redux

		howitzer = {
			soft_attack = 0.1
		}
		howitzer_brigade = {
			soft_attack = 0.1
		}
		
		start_year = 1916
		
		path = {
			leads_to_tech = howitzer4
			research_cost_coeff = 1
		}
		
		research_cost = 2.0
		
		folder = {
			name = artillery_folder
			position = { x = 6 y = 6 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				date < 1915.1.1
				factor = 0
			}
		}
	}
	howitzer4 = { #TGW Redux

		enable_equipments = {
			howitzer_equipment_2
		}
		
		start_year = 1918
		
		research_cost = 2.0
		
		folder = {
			name = artillery_folder
			position = { x = 6 y = 8 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	wwi_artillery5 = {

		artillery = {
			soft_attack = 0.1
		}
		artillery_brigade = {
			soft_attack = 0.1
		}
		mot_artillery_brigade = {
			soft_attack = 0.1
		}
		
		research_cost = 1

		start_year = 1922
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 12 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1921.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1922.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1923.1.1"
			}
		}
	}

	wwi_rocket_artillery = {

		enable_equipments = {
			wwi_rocket_artillery_equipment_1
		}
		
		path = {
			leads_to_tech = wwi_rocket_artillery2
			research_cost_coeff = 1
		}
		
		research_cost = 1.5

		start_year = 1914
		
		folder = {
			name = artillery_folder
			position = { x = 3 y = 6 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 5
				OR = {
					tag = ENG
					tag = USA
				}
			}
		}
		
		categories = {
			rocketry
		}
	}
	
	wwi_rocket_artillery2 = {

		rocket_artillery = {
			soft_attack = 0.15
		}
		rocket_artillery_brigade = {
			soft_attack = 0.15
		}
		
		path = {
			leads_to_tech = wwi_rocket_artillery3
			research_cost_coeff = 1
		}
		
		research_cost = 1.5

		start_year = 1916
		
		folder = {
			name = artillery_folder
			position = { x = 3 y = 8 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 5
				OR = {
					tag = ENG
					tag = USA
				}
			}
		}
		
		categories = {
			rocketry
		}
	}
	
	wwi_rocket_artillery3 = {

		rocket_artillery = {
			soft_attack = 0.15
		}
		rocket_artillery_brigade = {
			soft_attack = 0.15
		}

		path = {
			leads_to_tech = wwi_rocket_artillery4
			research_cost_coeff = 1
		}
		
		research_cost = 1.5

		start_year = 1918
		
		folder = {
			name = artillery_folder
			position = { x = 3 y = 10 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 5
				OR = {
					tag = ENG
					tag = USA
				}
			}
		}
		
		categories = {
			rocketry
		}
	}
	
	wwi_rocket_artillery4 = {
	
		enable_equipments = {
			wwi_rocket_artillery_equipment_2
		}

		#rocket_artillery = {
		#	soft_attack = 0.15
		#}
		#rocket_artillery_brigade = {
		#	soft_attack = 0.15
		#}

		research_cost = 1.5

		start_year = 1920
		
		folder = {
			name = artillery_folder
			position = { x = 3 y = 12 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 5
				OR = {
					tag = ENG
					tag = USA
				}
			}
		}
		
		categories = {
			rocketry
		}
	}
	
	wwi_interwar_antiair = {

		enable_equipments = {
			wwi_anti_air_equipment_1
		}

		enable_equipment_modules = { 
			wwi_ship_anti_air_2
		}

		enable_building = {
			building = anti_air
			level = 5
		}
		
		path = {
			leads_to_tech = wwi_antiair1
			research_cost_coeff = 1
		}
		
		research_cost = 2.0

		start_year = 1914
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 6 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0.5

				OR = {
					tag = CHI
					tag = PRC
					tag = XSM
					tag = SIK
					tag = GXC
					tag = SHX
					tag = YUN
				}
			}
			modifier = {
				factor = 2
				date > "1937.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1938.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1939.1.1"
			}
		}
		
		categories = {
			artillery
		}
	}
	
	wwi_antiair1 = {

		anti_air = {
			air_attack = 0.1
		}
		anti_air_brigade = {
			air_attack = 0.1
		}
		#mot_anti_air_brigade = {
		#	air_attack = 0.1
		#}

		tech_air_damage_factor = -0.02
		
		path = {
			leads_to_tech = wwi_antiair2
			research_cost_coeff = 1
		}
		
		research_cost = 1.5

		start_year = 1916
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 8 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	
	wwi_antiair2 = {
	
		enable_equipments = {
			wwi_anti_air_equipment_2
		}

		static_anti_air_damage_factor = 0.1

		enable_equipment_modules = { 
			wwi_ship_anti_air_3
		}
		
		path = {
			leads_to_tech = wwi_antiair3
			research_cost_coeff = 1
		}
		
		research_cost = 2.0

		start_year = 1918
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 10 }
		}
		
		ai_will_do = {
			factor = 2
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	
	wwi_antiair3 = {

		anti_air = {
			air_attack = 0.1
		}
		anti_air_brigade = {
			air_attack = 0.1
		}
		#mot_anti_air_brigade = {
		#	air_attack = 0.1
		#}
		
		tech_air_damage_factor = -0.02
		
		research_cost = 1.5

		start_year = 1920
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 12 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	
	wwi_interwar_antitank = {

		enable_equipments = {
			wwi_anti_tank_equipment_1
		}
		
		path = {
			leads_to_tech = wwi_antitank1
			research_cost_coeff = 1
		}
		
		research_cost = 2.0

		start_year = 1914
		
		folder = {
			name = artillery_folder
			position = { x = -6 y = 6 }
		}
		
		ai_will_do = {
			factor = 0

			modifier = {
				factor = 2
				date > "1917.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1920.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1921.1.1"
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	
	wwi_antitank1 = {

		anti_tank = {
			hard_attack = 0.1
			ap_attack = 0.2
		}
		anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.2
		}
		mot_anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.2
		}
		
		path = {
			leads_to_tech = wwi_antitank2
			research_cost_coeff = 1
		}
		
		research_cost = 1.5

		start_year = 1916
		
		folder = {
			name = artillery_folder
			position = { x = -6 y = 8 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 10
				any_enemy_country = {
					ROOT = {
						estimated_intel_max_armor = {
							tag = PREV
							value > 30
						}
					}
				}
			}
			modifier = {
				tag = JAP
				not = {
					OR = {
						has_War_with = SOV
						has_war_with = USA
					}
				}
				factor = 0.5
			}
			modifier = {
				factor = 2
				date > "1939.1.6"
			}
			
			modifier = {
				factor = 5
				date > "1940.1.1"
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	
	wwi_antitank2 = {
		
		enable_equipments = {
			wwi_anti_tank_equipment_2
		}
		
		#anti_tank = {
		#	hard_attack = 0.1
		#	ap_attack = 0.05
		#}
		#anti_tank_brigade = {
		#	hard_attack = 0.1
		#	ap_attack = 0.05
		#}
		
		path = {
			leads_to_tech = wwi_antitank3
			research_cost_coeff = 1
		}
		
		research_cost = 2.0

		start_year = 1920
		
		folder = {
			name = artillery_folder
			position = { x = -6 y = 10 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1939.1.9"
			}
			modifier = {
				tag = JAP
				not = {
					OR = {
						has_War_with = SOV
						has_war_with = USA
					}
				}
				factor = 0.5
			}
			modifier = {
				factor = 2
				date > "1940.1.1"
			}
			modifier = {
				tag = USA
				has_War = no
				factor = 0.5 #this tank nonsense will never catch on
			}
			modifier = {
				factor = 5
				date > "1940.1.6"
			}
			modifier = {
				factor = 10
				any_enemy_country = {
					ROOT = {
						estimated_intel_max_armor = {
							tag = PREV
							value > 32
						}
					}
				}
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	
	wwi_antitank3 = {

		anti_tank = {
			hard_attack = 0.1
			ap_attack = 0.1
		}
		anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.1
		}
		mot_anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.1
		}
		
		research_cost = 1.5

		start_year = 1924
		
		folder = {
			name = artillery_folder
			position = { x = -6 y = 12 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1941.1.6"
			}
			modifier = {
				tag = JAP
				not = {
					OR = {
						has_War_with = SOV
						has_war_with = USA
					}
				}
				factor = 0.5
			}
			modifier = {
				factor = 2
				date > "1942.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1943.1.1"
			}
			modifier = {
				factor = 10
				any_enemy_country = {
					ROOT = {
						estimated_intel_max_armor = {
							tag = PREV
							value > 32
						}
					}
				}
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}

	interwar_artillery = {

		dependencies = {
			wwi_artillery5 = 1
		}
		
		artillery = {
			soft_attack = 0.1
		}
		artillery_brigade = {
			soft_attack = 0.1
		}
		mot_artillery_brigade = {
			soft_attack = 0.1
		}
		
		path = {
			leads_to_tech = artillery1
			research_cost_coeff = 1
		}
		
		research_cost = 1.5

		start_year = 1936
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 2 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2

			modifier = {
				factor = 2
				date > "1937.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1938.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1939.1.1"
			}
		}
	}
	
	artillery1 = {
	
		enable_equipments = {
			artillery_equipment_2
		}

		#artillery = {
		#	soft_attack = 0.1
		#}
		#artillery_brigade = {
		#	soft_attack = 0.1
		#}
		
		path = {
			leads_to_tech = artillery2
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1939
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 4 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1938.1.9"
			}
			
			modifier = {
				factor = 2
				date > "1939.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1939.1.6"
			}
		}
	}
	
	artillery2 = {

		artillery = {
			soft_attack = 0.1
		}
		artillery_brigade = {
			soft_attack = 0.1
		}
		mot_artillery_brigade = {
			soft_attack = 0.1
		}
		
		path = {
			leads_to_tech = artillery3
			research_cost_coeff = 1
		}
		
		path = {
			leads_to_tech = rocket_artillery
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1940
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 6 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1940.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1941.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1942.1.1"
			}
		}
	}
	
	artillery3 = {

		artillery = {
			soft_attack = 0.1
		}
		artillery_brigade = {
			soft_attack = 0.1
		}
		mot_artillery_brigade = {
			soft_attack = 0.1
		}
		
		path = {
			leads_to_tech = artillery4
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1941
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 8 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1941.1.6"
			}
			
			modifier = {
				factor = 5
				date > "1942.1.1"
			}
		}
	}
	
	artillery4 = {
	
		enable_equipments = {
			artillery_equipment_3
		}

		#artillery = { 
		#	soft_attack = 0.1
		#}
		#artillery_brigade = {
		#	soft_attack = 0.1
		#}
		
		path = {
			leads_to_tech = artillery5
			research_cost_coeff = 1
		}
		
		research_cost = 2

		start_year = 1942
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 10 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1941.1.9"
			}
			
			modifier = {
				factor = 2
				date > "1942.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1942.1.6"
			}
		}
	}
	
	artillery5 = {

		artillery = {
			soft_attack = 0.1
		}
		artillery_brigade = {
			soft_attack = 0.1
		}
		mot_artillery_brigade = {
			soft_attack = 0.1
		}
		path = {
			leads_to_tech = cwart1
			research_cost_coeff = 1
		}		
		research_cost = 1

		start_year = 1943
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 12 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1945.1.1"
			}
		}
	}
	cwart1 = {
	
		enable_equipments = {
			artillery_equipment_4
		}

		#artillery = { 
		#	soft_attack = 0.1
		#}
		#artillery_brigade = {
		#	soft_attack = 0.1
		#}
		
		path = {
			leads_to_tech = cwart2
			research_cost_coeff = 1
		}
		
		research_cost = 2

		start_year = 1950
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 14 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1948.1.9"
			}
		}
	}
	cwart2 = {
	
		enable_equipments = {
			artillery_equipment_5
		}

		#artillery = { 
		#	soft_attack = 0.1
		#}
		#artillery_brigade = {
		#	soft_attack = 0.1
		#}
		
		path = {
			leads_to_tech = cwart3
			research_cost_coeff = 1
		}
		
		research_cost = 2

		start_year = 1960
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 16 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1958.1.9"
			}
		}
	}
	cwart3 = {
	
		enable_equipments = {
			artillery_equipment_6
		}

		#artillery = { 
		#	soft_attack = 0.1
		#}
		#artillery_brigade = {
		#	soft_attack = 0.1
		#}
		
		path = {
			leads_to_tech = cwart4
			research_cost_coeff = 1
		}
		
		research_cost = 2

		start_year = 1970
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 18 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1968.1.9"
			}
		}
	}
	cwart4 = {
	
		enable_equipments = {
			artillery_equipment_7
		}

		#artillery = { 
		#	soft_attack = 0.1
		#}
		#artillery_brigade = {
		#	soft_attack = 0.1
		#}
		
		path = {
			leads_to_tech = cwart5
			research_cost_coeff = 1
		}
		
		research_cost = 2

		start_year = 1980
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 20 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1978.1.9"
			}
		}
	}
	cwart5 = {
	
		enable_equipments = {
			artillery_equipment_8
		}

		#artillery = { 
		#	soft_attack = 0.1
		#}
		#artillery_brigade = {
		#	soft_attack = 0.1
		#}
		
		research_cost = 2

		start_year = 1990
		
		folder = {
			name = artillery_folder
			position = { x = 0 y = 22 }
		}
		
		categories = {
			artillery
		}
		
		ai_will_do = {
			factor = 2
			
			modifier = {
				factor = 2
				date > "1988.1.9"
			}
		}
	}

	mountain_gun = {

		research_cost = 1

		# only from focus!!! #####
		allow = {
			always = no
		}

		artillery_brigade = {
			mountain = {
				movement = 0.15 #If you change this - Change the custom tooltip too: RAJ_revive_the_screw_guns_effect ROM_modern_at_guns_tech_tt
			}
		}

		anti_tank_brigade = {
			mountain = {
				movement = 0.15 #If you change this - Change the custom tooltip too: RAJ_revive_the_screw_guns_effect ROM_modern_at_guns_tech_tt
			}
		}
	}
	
	rocket_artillery = {

		enable_equipments = {
			rocket_artillery_equipment_1
		}
#		enable_subunits = {
#			rocket_artillery_brigade
#		}
		
		path = {
			leads_to_tech = rocket_artillery2
			research_cost_coeff = 1
		}
		
		research_cost = 2

		start_year = 1940
		
		folder = {
			name = artillery_folder
			position = { x = 3 y = 6 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				tag = SOV
			}
		}
		
		categories = {
			rocketry
		}
	}
	
	rocket_artillery2 = {

		rocket_artillery = {
			soft_attack = 0.15
		}
		rocket_artillery_brigade = {
			soft_attack = 0.15
		}
		mot_rocket_artillery_brigade = {
			soft_attack = 0.15
		}
		motorized_rocket_brigade = {
			soft_attack = 0.15
		}
		
		path = {
			leads_to_tech = rocket_artillery3
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1941
		
		folder = {
			name = artillery_folder
			position = { x = 3 y = 8 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				tag = SOV
			}
		}
		
		categories = {
			rocketry
		}
	}
	
	rocket_artillery3 = {

		rocket_artillery = {
			soft_attack = 0.15
		}
		rocket_artillery_brigade = {
			soft_attack = 0.15
		}
		mot_rocket_artillery_brigade = {
			soft_attack = 0.15
		}
		motorized_rocket_brigade = {
			soft_attack = 0.15
		}

		path = {
			leads_to_tech = rocket_artillery4
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1942
		
		folder = {
			name = artillery_folder
			position = { x = 3 y = 10 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				tag = SOV
			}
		}
		
		categories = {
			rocketry
		}
	}
	
	rocket_artillery4 = {
	
		enable_equipments = {
			rocket_artillery_equipment_2
		}
		motorized_rocket_brigade = {
			soft_attack = 0.3
		}

		#rocket_artillery = {
		#	soft_attack = 0.15
		#}
		#rocket_artillery_brigade = {
		#	soft_attack = 0.15
		#}

		research_cost = 1

		start_year = 1943
		
		folder = {
			name = artillery_folder
			position = { x = 3 y = 12 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				tag = SOV
			}
		}
		
		categories = {
			rocketry
		}
	}
	
	interwar_antiair = {

		dependencies = {
			wwi_antiair3 = 1
		}

		enable_equipments = {
			anti_air_equipment_1
		}

		enable_equipment_modules = { 
			ship_anti_air_2
		}

		enable_building = {
			building = anti_air
			level = 5
		}
		
		path = {
			leads_to_tech = antiair1
			research_cost_coeff = 1
		}
		
		research_cost = 1.5

		start_year = 1936
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 2 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0.5

				OR = {
					tag = CHI
					tag = PRC
					tag = XSM
					tag = SIK
					tag = GXC
					tag = SHX
					tag = YUN
				}
			}
			modifier = {
				factor = 2
				date > "1937.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1938.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1939.1.1"
			}
		}
		
		categories = {
			artillery
		}
	}
	
	antiair1 = {

		anti_air = {
			air_attack = 0.1
		}
		anti_air_brigade = {
			air_attack = 0.1
		}
		mot_anti_air_brigade = {
			air_attack = 0.1
		}

		tech_air_damage_factor = -0.02
		
		path = {
			leads_to_tech = antiair2
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1939
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 4 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	
	antiair2 = {
	
		enable_equipments = {
			anti_air_equipment_2
		}

		static_anti_air_damage_factor = 0.1

		enable_equipment_modules = { 
			ship_anti_air_3
		}
		
		path = {
			leads_to_tech = antiair3
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1940
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 6 }
		}
		
		ai_will_do = {
			factor = 2
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	
	antiair3 = {

		anti_air = {
			air_attack = 0.1
		}
		anti_air_brigade = {
			air_attack = 0.1
		}
		mot_anti_air_brigade = {
			air_attack = 0.1
		}
		
		tech_air_damage_factor = -0.02

		path = {
			leads_to_tech = antiair4
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1941
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 8 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	
	antiair4 = {

		anti_air = {
			air_attack = 0.1
		}
		anti_air_brigade = {
			air_attack = 0.1
		}
		mot_anti_air_brigade = {
			air_attack = 0.1
		}

		tech_air_damage_factor = -0.02
		
		path = {
			leads_to_tech = antiair5
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1942
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 10 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	
	antiair5 = {

		enable_equipments = {
			anti_air_equipment_3
		}
		path = {
			leads_to_tech = antiair6
			research_cost_coeff = 1
		}
		static_anti_air_damage_factor = 0.1
		
		enable_equipment_modules = { 
			ship_anti_air_4
		}
		
		research_cost = 1

		start_year = 1943
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 12 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	antiair6 = {

		enable_equipments = {
			anti_air_equipment_4
		}
		path = {
			leads_to_tech = antiair7
			research_cost_coeff = 1
		}
		static_anti_air_damage_factor = 0.1
		
		research_cost = 1

		start_year = 1950
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 14 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	antiair7 = {

		enable_equipments = {
			anti_air_equipment_5
		}
		path = {
			leads_to_tech = antiair8
			research_cost_coeff = 1
		}
		static_anti_air_damage_factor = 0.1
		
		research_cost = 1

		start_year = 1960
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 16 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	antiair8 = {

		enable_equipments = {
			anti_air_equipment_6
		}
		path = {
			leads_to_tech = antiair9
			research_cost_coeff = 1
		}
		static_anti_air_damage_factor = 0.1
		
		research_cost = 1

		start_year = 1970
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 18 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	antiair9 = {

		enable_equipments = {
			anti_air_equipment_7
		}
		path = {
			leads_to_tech = antiair10
			research_cost_coeff = 1
		}
		static_anti_air_damage_factor = 0.1
		
		research_cost = 1

		start_year = 1980
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 20 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	antiair10 = {

		enable_equipments = {
			anti_air_equipment_8
		}
		static_anti_air_damage_factor = 0.1
		
		research_cost = 1

		start_year = 1990
		
		folder = {
			name = artillery_folder
			position = { x = -3 y = 22 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				OR = {
					tag = JAP
					tag = ENG
					tag = USA
				}
				OR = {
					has_war_with = JAP
					has_war_with = USA
					has_war_with = ENG
				}
				factor = 5
			}
		}
		
		categories = {
			artillery
		}
	}
	
	interwar_antitank = {

		dependencies = {
			wwi_antitank3 = 1
		}
		
		enable_equipments = {
			anti_tank_equipment_1
		}
		
		path = {
			leads_to_tech = antitank1
			research_cost_coeff = 1
		}
		
		research_cost = 1.5

		start_year = 1936
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 2 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0.2

				OR = {
					tag = CHI
					tag = PRC
					tag = XSM
					tag = SIK
					tag = GXC
					tag = SHX
					tag = YUN
				}
			}
			modifier = {
				tag = JAP
				not = {
					OR = {
						has_War_with = SOV
						has_war_with = USA
					}
				}
				factor = 0.5
			}
			modifier = {
				factor = 2
				date > "1937.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1938.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1939.1.1"
			}
			modifier = {
				factor = 10
				any_enemy_country = {
					ROOT = {
						estimated_intel_max_armor = {
							tag = PREV
							value > 30
						}
					}
				}
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	
	antitank1 = {

		anti_tank = {
			hard_attack = 0.1
			ap_attack = 0.2
		}
		anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.2
		}
		mot_anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.2
		}
		
		path = {
			leads_to_tech = antitank2
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1939
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 4 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 10
				any_enemy_country = {
					ROOT = {
						estimated_intel_max_armor = {
							tag = PREV
							value > 30
						}
					}
				}
			}
			modifier = {
				tag = JAP
				not = {
					OR = {
						has_War_with = SOV
						has_war_with = USA
					}
				}
				factor = 0.5
			}
			modifier = {
				factor = 2
				date > "1939.1.6"
			}
			
			modifier = {
				factor = 5
				date > "1940.1.1"
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	
	antitank2 = {
		
		enable_equipments = {
			anti_tank_equipment_2
		}
		
		#anti_tank = {
		#	hard_attack = 0.1
		#	ap_attack = 0.05
		#}
		#anti_tank_brigade = {
		#	hard_attack = 0.1
		#	ap_attack = 0.05
		#}
		
		path = {
			leads_to_tech = antitank3
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1940
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 6 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1939.1.9"
			}
			modifier = {
				tag = JAP
				not = {
					OR = {
						has_War_with = SOV
						has_war_with = USA
					}
				}
				factor = 0.5
			}
			modifier = {
				factor = 2
				date > "1940.1.1"
			}
			modifier = {
				tag = USA
				has_War = no
				factor = 0.5 #this tank nonsense will never catch on
			}
			modifier = {
				factor = 5
				date > "1940.1.6"
			}
			modifier = {
				factor = 10
				any_enemy_country = {
					ROOT = {
						estimated_intel_max_armor = {
							tag = PREV
							value > 32
						}
					}
				}
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	
	antitank3 = {

		anti_tank = {
			hard_attack = 0.1
			ap_attack = 0.1
		}
		anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.1
		}
		mot_anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.1
		}
		
		path = {
			leads_to_tech = antitank4
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1941
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 8 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1941.1.6"
			}
			modifier = {
				tag = JAP
				not = {
					OR = {
						has_War_with = SOV
						has_war_with = USA
					}
				}
				factor = 0.5
			}
			modifier = {
				factor = 2
				date > "1942.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1943.1.1"
			}
			modifier = {
				factor = 10
				any_enemy_country = {
					ROOT = {
						estimated_intel_max_armor = {
							tag = PREV
							value > 32
						}
					}
				}
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	
	antitank4 = {

		anti_tank = {
			hard_attack = 0.1
			ap_attack = 0.1
		}
		anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.1
		}
		mot_anti_tank_brigade = {
			hard_attack = 0.1
			ap_attack = 0.1
		}
	
		path = {
			leads_to_tech = antitank5
			research_cost_coeff = 1
		}
		
		research_cost = 1

		start_year = 1942
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 10 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1942.1.6"
			}
			modifier = {
				tag = JAP
				not = {
					OR = {
						has_War_with = SOV
						has_war_with = USA
					}
				}
				factor = 0.5
			}
			modifier = {
				factor = 5
				date > "1943.1.1"
			}
			modifier = {
				factor = 10
				any_enemy_country = {
					ROOT = {
						estimated_intel_max_armor = {
							tag = PREV
							value > 35
						}
					}
				}
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	
	antitank5 = {
			
		enable_equipments = {
			anti_tank_equipment_3
		}	
			
		#anti_tank = {
		#	hard_attack = 0.1
		#	ap_attack = 0.05
		#}
		#anti_tank_brigade = {
		#	hard_attack = 0.1
		#	ap_attack = 0.05
		#}
		path = {
			leads_to_tech = antitank6
			research_cost_coeff = 1
		}		
		research_cost = 1

		start_year = 1943
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 12 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1942.1.9"
			}
			modifier = {
				tag = JAP
				not = {
					OR = {
						has_War_with = SOV
						has_war_with = USA
					}
				}
				factor = 0.5
			}
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1943.1.6"
			}
			modifier = {
				factor = 10
				any_enemy_country = {
					ROOT = {
						estimated_intel_max_armor = {
							tag = PREV
							value > 35
						}
					}
				}
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	antitank6 = {
			
		enable_equipments = {
			anti_tank_equipment_4
		}	
		path = {
			leads_to_tech = antitank7
			research_cost_coeff = 1
		}		
		research_cost = 1

		start_year = 1950
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 14 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1948.1.9"
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	antitank7 = {
			
		enable_equipments = {
			anti_tank_equipment_5
		}	
		path = {
			leads_to_tech = antitank8
			research_cost_coeff = 1
		}		
		research_cost = 1

		start_year = 1960
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 16 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1958.1.9"
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	antitank8 = {
			
		enable_equipments = {
			anti_tank_equipment_6
		}	
		path = {
			leads_to_tech = antitank9
			research_cost_coeff = 1
		}		
		research_cost = 1

		start_year = 1970
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 18 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1968.1.9"
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	antitank9 = {
			
		enable_equipments = {
			anti_tank_equipment_7
		}	
		path = {
			leads_to_tech = antitank10
			research_cost_coeff = 1
		}		
		research_cost = 1

		start_year = 1980
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 20 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1978.1.9"
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	antitank10 = {
			
		enable_equipments = {
			anti_tank_equipment_8
		}			
		research_cost = 1

		start_year = 1990
		
		folder = {
			name = artillery_folder
			position = { x = 7 y = 22 }
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1988.1.9"
			}
		}
		
		categories = {
			artillery
			cat_anti_tank
		}
	}
	
}