## faction_fascist_paramilitary_militia
## faction_fascist_paramilitary
## faction_fascist_paramilitary_heavy
## enable_bersaglieri_tech

technologies = {

	faction_fascist_paramilitary_militia = {
	    enable_subunits = {
            fascist_militia
        }

        research_cost = 1
        start_year = 1925

        # not researchable #####
        allow = {
            always = no
        }

        ai_will_do = {
            factor = 0
        }
	}

	faction_fascist_paramilitary = {
	    enable_subunits = {
            fascist_infantry
            fascist_cavalry
            fascist_motorized
        }

        research_cost = 1
        start_year = 1925

        # not researchable #####
        allow = {
            always = no
        }

        ai_will_do = {
            factor = 0
        }
	}

	faction_fascist_paramilitary_heavy = {
	    enable_subunits = {
            fascist_mechanized
            fascist_light_armor
            fascist_medium_armor
            fascist_heavy_armor
        }

        research_cost = 1
        start_year = 1925

        # not researchable #####
        allow = {
            always = no
        }

        ai_will_do = {
            factor = 0
        }
	}
}