characters={

######## GREAT WAR LEADERS ##########################################
	AFG_habdulah_khan={
		name="Emir Habibullah Khan"
		portraits={
			civilian={
				large="gfx/leaders/AFG/AFG_habibullah.dds"
			}
		}
		country_leader={
			ideology=despotism
			expire="1919.2.20.1"
			id=-1
		}
		corps_commander={
			traits={ desert_fox }
			skill = 2
			attack_skill = 2
			defense_skill = 2
			planning_skill = 2
			logistics_skill = 2
			legacy_id=-1
		}
	}

######## GREAT WAR GENERALS ##########################################
	AFG_amanullah_khan={
		name = "Amanullah Khan"
		portraits={
			army={
				large="gfx/leaders/AFG/AFG_amanullah_khan.dds"
				small="gfx/interface/ideas/idea_LBA_generic_land_3.dds"
			}
		}
		corps_commander={
			traits={ desert_fox }
			skill = 2
			attack_skill = 2
			defense_skill = 2
			planning_skill = 1
			logistics_skill = 3
		}
	}
	AFG_nadir_shah_mohamad={
		name = "Nadir Shah Mohamad"
		portraits={
			army={
				large="gfx/leaders/AFG/AFG_nadir_khan.dds"
				small="gfx/interface/ideas/idea_LBA_generic_land_2.dds"
			}
		}
		corps_commander={
			traits={ desert_fox }
			skill = 2
			attack_skill = 2
			defense_skill = 3
			planning_skill = 2
			logistics_skill = 1
		}
	}
	AFG_muhammad_umar_khan={
		name = "Muhammad Umar Khan"
		portraits={
			army={
				large="gfx/leaders/AFG/Portrait_Arabia_Generic_land_1.dds"
				small="gfx/interface/ideas/idea_LBA_generic_land_1.dds"
			}
		}
		corps_commander={
			traits={ }
			skill = 1
			attack_skill = 1
			defense_skill = 1
			planning_skill = 2
			logistics_skill = 2
		}
	}
	AFG_abdul_wakil={
		name = "Abdul Wakil Nuristani"
		portraits={
			army={`
				large="gfx/leaders/AFG/Portrait_Arabia_Generic_land_2.dds"
				small="gfx/interface/ideas/idea_LBA_generic_land_2.dds"
			}
		}
		corps_commander={
			traits={ }
			skill = 1
			attack_skill = 2
			defense_skill = 1
			planning_skill = 1
			logistics_skill = 1
		}
	}

######## WW2 LEADERS ##########################################
	AFG_mohammed_zahir_shah={
		name="Mohammed Zahir Shah"
		portraits={
			civilian={
				large="gfx/leaders/AFG/Portrait_Afghanistan_Mohammed_Zahir_Shah.dds"
			}
		}
		country_leader={
			ideology=absolutism
			expire="1965.1.1.1"
			id=-1
		}
	}
	AFG_akram_sattari={
		name="Akram Sattari"
		portraits={
			civilian={
				large="gfx/leaders/SAU/Portrait_Arabia_Generic_2.dds"
			}
		}
		country_leader={
			ideology=socialism
			traits={ captain_of_industry dealbroker }
			expire="1955.1.1.12"
			id=-1
		}
	}
	AFG_yaqub_khan={
		name="Yaqub Khan"
		portraits={
			civilian={
				large="gfx/leaders/SAU/Portrait_Arabia_Generic_1.dds"
			}
		}
		country_leader={
			ideology=stalinism
			expire="1944.1.1.12"
			id=-1
		}
	}
	AFG_mihrdil_shahnawaz={
		name="Mihrdil Shahnawaz"
		portraits={
			civilian={
				large="gfx/leaders/SAU/Portrait_Arabia_Generic_3.dds"
			}
		}
		country_leader={
			ideology=fascism_ideology
			expire="1949.1.1.12"
			id=-1
		}
	}
	
######## WW2 GENERALS ##########################################
	AFG_sardar_shah_wali_khan={
		name="Sardar Shah Wali Khan"
		portraits={
			army={
				large="gfx/leaders/SAU/Portrait_Arabia_Generic_land_3.dds"
				small="gfx/interface/ideas/idea_LBA_generic_land_3.dds"
			}
		}
		corps_commander={
			traits={ desert_fox }
			skill=4
			attack_skill=5
			defense_skill=2
			planning_skill=3
			logistics_skill=3
			legacy_id=-1
		}
	}
}
