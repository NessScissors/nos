characters={
	ADR_general_council_cons={
		name="Consell General"
		portraits={
			civilian={
				large="gfx/leaders/ADR/Portrait_Andorra_General_council.dds"
			}
		}
		country_leader={
			ideology=conservatism
			id=-1
		}
	}
	ADR_general_council_lib={
		name="Consell General"
		portraits={
			civilian={
				large="gfx/leaders/ADR/Portrait_Andorra_General_council.dds"
			}
		}
		country_leader={
			ideology=liberalism
			id=-1
		}
	}
	ADR_general_council_socdem={
		name="Consell General"
		portraits={
			civilian={
				large="gfx/leaders/ADR/Portrait_Andorra_General_council.dds"
			}
		}
		country_leader={
			ideology=socialism
			id=-1
		}
	}
	ADR_general_council_soc={
		name="Consell General"
		portraits={
			civilian={
				large="gfx/leaders/ADR/Portrait_Andorra_General_council.dds"
			}
		}
		country_leader={
			ideology=anarchist_communism
			id=-1
		}
	}
	ADR_general_council_com={
		name="Consell General"
		portraits={
			civilian={
				large="gfx/leaders/ADR/Portrait_Andorra_General_council.dds"
			}
		}
		country_leader={
			ideology=marxism
			id=-1
		}
	}
	ADR_general_council_neu={
		name="Consell General"
		portraits={
			civilian={
				large="gfx/leaders/ADR/Portrait_Andorra_General_council.dds"
			}
		}
		country_leader={
			ideology=despotism
			id=-1
		}
	}
	ADR_general_council_fac={
		name="Consell General"
		portraits={
			civilian={
				large="gfx/leaders/ADR/Portrait_Andorra_General_council.dds"
			}
		}
		country_leader={
			ideology=fascism_ideology
			id=-1
		}
	}
	ADR_general_council_mon={
		name="Consell General"
		portraits={
			civilian={
				large="gfx/leaders/ADR/Portrait_Andorra_General_council.dds"
			}
		}
		country_leader={
			ideology=constitutionalism
			id=-1
		}
	}
}
