
NDefines_Graphics.NMapIcons.STATES_PRIORITY_VICTORY_POINTS = 8
NDefines_Graphics.NGraphics.COUNTRY_COLOR_HUE_MODIFIER = 0.0 -- 0.0
NDefines_Graphics.NGraphics.COUNTRY_COLOR_SATURATION_MODIFIER = 1.0 -- 0.6
NDefines_Graphics.NGraphics.COUNTRY_COLOR_BRIGHTNESS_MODIFIER = 0.75 -- 0.8

NDefines_Graphics.NGraphics.PROVINCE_BORDER_FADE_NEAR = 350
NDefines_Graphics.NGraphics.PROVINCE_BORDER_FADE_FAR = 355
NDefines_Graphics.NMapMode.FACTIONS_COLOR_NOT_MEMBER = { 0.5, 0.5, 0.5, 0.1 } -- 0.6, 0.6, 0.6, 0.25

