
USA_civilwar = {
	name = "Second Civil War Plan"
	desc = "Plan for the US to go have another Civil War"

	allowed = {
		original_tag = USA
	}
	enable = {
		OR = {
			has_country_flag = USA_AI_RANDOM_CIVILWAR
			has_game_rule = {
				rule = USA_ai_behavior
				option = CIVILWAR
			}
		}
	}
	abort = {
		
	}

	ai_national_focuses = {
		USA_political_chaos
	}

	research = {

	}

	ideas = {
		earl_browder = 10
	}
	traits = {
		communist_revolutionary = 15
		
	}

	

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

}
