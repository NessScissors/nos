USA_historical_plan_1 = {
	name = "US historical plan"
	desc = "Historical behavior for US"

	allowed = {
		original_tag = USA
	}
	enable = {
		has_focus_tree = ww1_usa
		OR = {
			AND = {
				is_historical_focus_on = yes
				OR = {
					has_game_rule = {
						rule = USA_ai_behavior
						option = DEFAULT
					}
				}
			}
			has_country_flag = USA_AI_DEMOCRATIC_HISTORICAL
			has_country_flag = democratic_usa_won_civil_war
			has_game_rule = {
				rule = USA_ai_behavior
				option = DEMOCRATIC_HISTORICAL
			}
		}
	}
	abort = {
		NOT = { has_focus_tree = ww1_usa }
		OR = {
			AND = {
				has_game_rule = {
					rule = USA_ai_behavior
					option = DEFAULT
				}
			}
		}
	}

	ai_national_focuses = {
		USA_do_not_stand_aside

		USA_immigration_act
		USA_migration_quotas
		USA_reformist_progressivism
		USA_international_conference_of_american_states
		USA_great_white_fleet
		USA_old_neutrality_act
		# 11
		USA_the_storm_is_coming
		USA_border_war
		USA_reorganize_army_corps
		USA_protect_the_south_america
		USA_strike_central_america
		# 12
		USA_marine_forces
		USA_limited_support_of_mexican_revolution
		USA_protect_the_caribbean
		USA_maneuver_divison
		USA_estate_tax
		# 13
		USA_aeronautical_divisions
		USA_federal_reserve
		USA_land_of_inventors
		USA_underwood_tariff
		USA_shotgun
		# 14
		USA_volunteer_act
		USA_natural_bulwarks
		USA_strike_caribbean_islands
		USA_protest_diplomacy
		USA_aviation_section
		# 15
		USA_doctrine_of_armored_raids
		USA_federal_trade_comission
		USA_antitrust_act
		USA_principle_of_workmens_compensation
		USA_arms_tax
		USA_coast_guard_act #1/2
		# 16
		USA_regular_divisions
		USA_big_navy_act
		USA_limited_rearmament
		USA_artillery_support
		USA_road_aid_act
		# 17
		USA_military_training_corps
		USA_continental_corps
		USA_war_risk_insurance_act #1/2
		USA_support_aviation
		# 18
		USA_expeditionary_forces
		USA_lafayette_escadrille
		USA_air_service
	}

	focus_factors = {
		USA_suspend_the_presecution = 0 # well done, not medium rare
	}

	research = {

	}

	ideas = {

	}
	traits = {
		war_industrialist = 5
		financial_expert = 10
		silent_workhorse = 25
	}

	ai_strategy = {
		type = support
		id = "ENG"			
		value = 200
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

}

USA_historical_plan_2 = {
	name = "US historical plan"
	desc = "Historical behavior for US"

	allowed = {
		original_tag = USA
	}
	enable = {
		has_focus_tree = ww2_usa
		OR = {
			AND = {
				is_historical_focus_on = yes
				OR = {
					has_game_rule = {
						rule = USA_ai_behavior
						option = DEFAULT
					}
				}
			}
			has_country_flag = USA_AI_DEMOCRATIC_HISTORICAL
			has_country_flag = democratic_usa_won_civil_war
			has_game_rule = {
				rule = USA_ai_behavior
				option = DEMOCRATIC_HISTORICAL
			}
		}
	}
	abort = {
		NOT = { has_focus_tree = ww2_usa }
		OR = {
			AND = {
				has_game_rule = {
					rule = USA_ai_behavior
					option = DEFAULT
				}
				OR = {	
					GER = {
						has_completed_focus = GER_oppose_hitler
					}
					ENG = {
						has_democratic_government = no
					}
					has_war_with = CAN
					has_war_with = MEX
					JAP = {
						not = {
							OR = {
								has_government = fascism
								has_government = monarchism
								has_government = neutrality
							}
						}
					}
				}
			}
		}
	}

	ai_national_focuses = {
		USA_split_the_atom
		USA_manhattan_project
		USA_Soil_Conservation_and_Domestic_Allotment_Act
		USA_The_Next_Election
		#USA_National_Foundation_for_Infant_Paralysis
		#USA_Merchant_Marine_Act
		USA_Good_Neighbor_Policy
		USA_Renew_American_Interest
		USA_Increase_Military_Funding
		USA_B17s
		#350
		USA_Agricultural_Adjustments_Act
		USA_Enlarge_the_Supreme_Court
		USA_Liberal_I
		#525
		USA_Trust_Indenture_Act
		USA_Investment_Company_Act
		USA_Expand_the_SEC
		#665
		USA_Proclaim_American_Neutrality
		USA_Pass_a_Neutrality_Act
		
		#1938
		USA_Lend_Lease
		USA_destroyers_for_bases
		USA_National_Cancer_Institute_Act
		USA_National_Housing_Act
		#805
		USA_Fair_Labor_Standards_Act
		USA_Federal_Food_Drug_and_Cosmetic_Act
		USA_Public_Health_Service_Act
		USA_Welcome_Refugee_Scientists
		#1085
		USA_Employment_Act
		USA_Subsidy_Programs_for_Mechanization_of_US_Agriculture
		USA_Foreign_Agents_Registration_Act
		#1295
		USA_Raise_Taxes
		USA_Hatch_Act
		USA_Smith_Act
		USA_The_First_Moscow_Conference
		USA_Lend_Lease_to_the_USSR
		#1505
		
		#1940ish
		USA_Naval_Act
		USA_Embargo_Japan
		USA_Community_Facilities_Act
		USA_Embargo_Germany_and_Italy
		USA_Two_Ocean_Navy_Act
		#1659
		USA_Louisiana_Maneuvers
		USA_Expand_Pearl_Harbor
		USA_Modernize_the_Artillery
		#1799
		USA_Push_Through_the_Merchant_Marine_Act
		
		#1941ish
		USA_War_Mobilization
		USA_Renew_American_Interest
		USA_Welcome_Refugee_Scientists
		USA_Establish_the_USAAF
		USA_Occupy_Greenland_and_Iceland
		USA_Norden_Bombsite
		USA_Liberal_II
	}

	focus_factors = {
		USA_suspend_the_presecution = 0 # well done, not medium rare
	}

	research = {

	}

	ideas = {

	}
	traits = {
		war_industrialist = 5
		financial_expert = 10
		silent_workhorse = 25
	}

	ai_strategy = {
		type = support
		id = "ENG"			
		value = 200
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

}