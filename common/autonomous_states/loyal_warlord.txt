autonomy_state = {
	id = autonomy_loyal_warlord
	
	default = yes
	
	min_freedom_level = 0.5
	use_overlord_color = yes
	manpower_influence = 0.6
	
	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
		units_deployed_to_overlord = no
		can_be_spymaster = no
		contributes_operatives = no
		can_create_collaboration_government = no
	}
	
	modifier = {
		autonomy_manpower_share = 0.3
		extra_trade_to_overlord_factor = 0.6
		overlord_trade_cost_factor = -0.6
		cic_to_overlord_factor = 0.15
		mic_to_overlord_factor = 0.6
		research_sharing_per_country_bonus_factor = -0.3
	}
	
	ai_subject_wants_higher = {
		factor = 1.0
	}
	
	ai_overlord_wants_lower = {
		factor = 1.0
	}

	ai_overlord_wants_garrison = {
		always = no
	}

	allowed = {
		has_dlc = "Together for Victory" 
		is_chinese_warlord = yes
		OVERLORD = {
			has_idea = CHI_china_broken
		}
	}

	can_take_level = {
		#trigger here
	}

	can_lose_level = {
		#trigger here
	}
}