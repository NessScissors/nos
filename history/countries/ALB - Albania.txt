capital = 44

set_stability = 0.5
set_war_support = 0.1

1910.1.1 = {
	oob = "ALB_1910"
	set_popularities = {
		democratic = 0
		fascism = 0
		communism = 0
		monarchism = 100
	}

	set_politics = {
		ruling_party = monarchism
		last_election = "1908.3.5"
		election_frequency = 48
		elections_allowed = yes
	}

	recruit_character = ALB_king_vidi
	recruit_character = ALB_prenk_pasha
	recruit_character = ALB_estat_toptani

	set_technology = {
		infantry_weapons = 1
		tech_mountaineers = 1
		tech_support = 1
		gw_artillery = 1
	}
}

1933.1.1 = {
	load_focus_tree = ww2_albania

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	if = {
		limit = { NOT = { has_dlc = "No Step Back" } }
		update_armor_tech_1936 = yes
	}

	remove_ideas = {
	}

	OOB = "ALB_1936"

	set_popularities = {
		democratic = 0
		fascism = 0
		communism = 0
		monarchism = 100
	}

	set_politics = {
		ruling_party = monarchism
		last_election = "1933.3.5"
		election_frequency = 48
		elections_allowed = yes
	}

	retire_character = ALB_king_vidi
	retire_character = ALB_prenk_pasha
	retire_character = ALB_estat_toptani

	recruit_character = ALB_king_zog
	recruit_character = ALB_enver_hoxha
	recruit_character = ALB_xhemal_aranitasi

	set_convoys = 20
}

1944.1.1 = {
	set_popularities = {
		conservative = 2
		fascism = 11
		monarchism = 21
		communism = 83
	}

	set_politics = {
		ruling_party = communism
		last_election = "1945.6.21"
		election_frequency = 48
		elections_allowed = no
	}
}