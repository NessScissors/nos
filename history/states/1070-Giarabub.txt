state={
	id=1070
	name="STATE_1070"

	impassable = yes

	history={
		owner = ENG
		buildings = {
			infrastructure = 0

		}
		add_core_of = LBA
		1933.1.1 = {
			owner = ITA
		}
		1944.1.1 = {
			owner = ENG
		}
	}

	provinces={
		1060 
	}
	manpower=2110
	buildings_max_level_factor=1.000
	state_category=wasteland
}
