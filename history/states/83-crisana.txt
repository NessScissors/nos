
state={
	id=83
	name="STATE_83"
	manpower = 632900
	
	state_category = town

	history={
		owner = AUS
		add_core_of = ROM
		add_core_of = TRA
		add_core_of = AUS
		add_core_of = HUN
		victory_points = {
			6682 1 
		}
		buildings = {
			infrastructure = 1
		}
		1933.1.1 = {
			owner = ROM
			remove_core_of = AUS
			remove_core_of = HUN
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 1
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 2
			}
		}
	}

	provinces={
		696 6682 6697 9640 
	}
}
