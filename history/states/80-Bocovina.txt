state={
	id=80
	name="STATE_80"
	manpower = 474600
	
	state_category = rural
	
	history=
	{
		victory_points = {
			577 1 
		}
		owner = AUS
		buildings = {
			infrastructure = 2
		}
		add_core_of = ROM
		add_core_of = UKR
		add_core_of = AUS
		1933.1.1 = {
			owner = ROM
			remove_core_of = AUS
			buildings = {
				infrastructure = 4
				arms_factory = 1
			}
		}
	}
	provinces={
		577 9548
	}
}
