state={
	id=884
	name="STATE_884"

	history=
	{
		owner = AUS
		buildings = {
			infrastructure = 1
			industrial_complex = 1
		}
		add_core_of = CZE
		add_core_of = AUS
		1933.1.1 = {
			owner = CZE
			remove_core_of = AUS
			buildings = {
				infrastructure = 2
				industrial_complex = 1
			}
		}
	}

	provinces={
		13266 
	}
	manpower=10000
	buildings_max_level_factor=1.000
	state_category = wasteland
}
