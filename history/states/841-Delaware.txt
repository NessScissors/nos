state={
	id=841
	name="STATE_841"

	history={
		owner = USA
		add_core_of = USA

		buildings = {
			infrastructure = 5
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 7
				industrial_complex = 1
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 8
				industrial_complex = 2
			}
		}
	}

	provinces={
		9786 
	}
	manpower=43010
	buildings_max_level_factor=1.000
	state_category=town
}
