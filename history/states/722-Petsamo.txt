state={
	id=722
	name="STATE_722"
	manpower = 2371

	state_category = enclave

	history={
		owner = RUS
		buildings = {
			infrastructure = 1
			9140 = {
				naval_base = 1
			}
		}
		add_core_of = FIN
		add_core_of = RUS
		1933.1.1 = {
			owner = FIN
			remove_core_of = RUS
			buildings = {
				infrastructure = 2
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 3
			}
		}
	}
	
	provinces={
		6012 6183 9140 11142 
	}
}
