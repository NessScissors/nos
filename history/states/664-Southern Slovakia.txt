state={
	id=664
	name="STATE_664"
	manpower = 854300
	resources={
		steel=13 # was: 20
		chromium = 3 # was: 4
	}
	
	state_category = town
	
	history={
		owner = AUS
		buildings = {
			infrastructure = 2
			industrial_complex = 1
		}
		victory_points = {
			6573 1 
		}
		add_core_of = SLO
		add_core_of = HUN
		add_core_of = AUS
		1933.1.1 = {
			owner = CZE
			add_core_of = CZE
			remove_core_of = AUS
			remove_core_of = HUN
			add_claim_by = HUN
			buildings = {
				infrastructure = 4
				industrial_complex = 2
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 5
				industrial_complex = 3
			}
		}
	}
	
	provinces={
		3565 3716 6561 6573 9537 11679 
	}
}
