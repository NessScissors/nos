
state={
	id=64
	name="STATE_64"

	history={
		owner = GER
		add_core_of = GER
		victory_points = {
			13292 5
		}
		buildings = {
			infrastructure = 6
			anti_air_building = 1
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 8
				anti_air_building = 2
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 9
				industrial_complex = 1
				anti_air_building = 4
			}
		}
	}

	provinces={
		13292 
	}
	manpower=1111000
	buildings_max_level_factor=1.000
	state_category=city
}
