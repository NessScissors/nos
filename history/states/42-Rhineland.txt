
state={
	id=42
	name="STATE_42"
	resources={
		steel=59.000
		aluminium=14.000
	}

	history={
		owner = GER
		add_core_of = GER
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		victory_points = {
			3558 5 
		}
		victory_points = {
			11560 10
		}
		victory_points = {
			13236 1
		}
		1933.1.1 = {
			set_demilitarized_zone = yes
			buildings = {
				infrastructure = 4

			}
		}
		1944.1.1 = {
			set_demilitarized_zone = no
			buildings = {
				infrastructure = 5
				industrial_complex = 2

			}
		}

	}

	provinces={
		563 3423 3558 11547 11560 13236 
	}
	manpower=2795303
	buildings_max_level_factor=1.000
	state_category=large_city
}
