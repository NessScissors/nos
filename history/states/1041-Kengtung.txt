state={
	id=1041
	name="STATE_1041"

	history={
		owner = RAJ
		add_core_of = BRM
		victory_points = {
			7909 1 
		}
		buildings = {
			infrastructure = 1
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 2
			}
		}
		1944.1.1 = {
			owner = BRM
			buildings = {
				infrastructure = 3
			}
		}
	}

	provinces={
		4175 4210 7400 7647 7909 7974 12317 
	}
	manpower=402300
	buildings_max_level_factor=1.000
	state_category=pastoral
}
