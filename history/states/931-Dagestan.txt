state={
	id=931
	name="STATE_931"

	history={
		owner = RUS
		victory_points = {
			3668 1 
		}
		buildings = {
			infrastructure = 2

		}
		add_core_of = RUS
		1933.1.1 = {
			owner = SOV
			remove_core_of = RUS
			add_core_of = SOV
			buildings = {
				infrastructure = 4

			}
		}
	}

	provinces={
		700 712 728 6701 6730 6748 
	}
	manpower=810010
	buildings_max_level_factor=1.000
	state_category=rural
}
