state={
	id=758
	name="STATE_758"
	provinces={
		1961 5094 8029 10802 12784 12801 
	}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = pastoral
	history={
		owner = MAN
		buildings = {
			infrastructure = 1
		}
		add_core_of = TIB
		remove_core_of = CHI
		remove_core_of = PRC
		add_core_of = MAN
		1933.1.1 = {
			owner = TIB
			remove_core_of = MAN
			add_core_of = CHI
			add_core_of = PRC

		}

	}
}
