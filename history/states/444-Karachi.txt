
state={
	id=444
	name="STATE_444" # Baluchistan
	manpower = 463500
	
	state_category = wasteland

	history={
		owner = RAJ
		victory_points = {
			8066 1
		}
		buildings = {
			infrastructure = 1
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 2
			}
		}
		1944.1.1 = {
			owner = PAK
			add_core_of = PAK
			buildings = {
				infrastructure = 3
			}
		}
	}

	provinces={
		1179 5043 5075 7917 8013 8066 12761 12774 
	}
}
