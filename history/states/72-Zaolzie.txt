state={
	id=72
	name="STATE_72"
	manpower = 152000
	
	state_category = rural
	
	history={
		owner = AUS
		buildings = {
			infrastructure = 2
		}
		add_core_of = CZE
		add_core_of = AUS
		1933.1.1 = {
			owner = CZE
			remove_core_of = AUS
			buildings = {
				infrastructure = 4
				industrial_complex = 1
			}
		}
	}

	provinces={
		9551 9567 
	}
}
