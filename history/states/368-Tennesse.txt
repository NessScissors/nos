
state={
	id=368
	name="STATE_368"
	manpower = 2616555
	
	state_category = city
	
	resources={
		steel=60 # was: 112
	}


	history={
		owner = USA
		add_core_of = USA
		victory_points = {
			12501 3 
		}
		victory_points = {
			7797 1 
		}
		victory_points = {
			10909 1 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 5
				industrial_complex = 3
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 7
				industrial_complex = 4
			}
		}
	}

	provinces={
		913 1758 1987 7615 7791 7797 8014 8083 10281 10615 10657 10824 10909 12501 12670 
	}
}
