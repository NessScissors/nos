
state={
	id=365
	name="STATE_365"
	manpower = 2908504
	
	state_category = city

	history={
		owner = USA
		add_core_of = USA
		victory_points = {
			12384 20 
		}
		victory_points = {
			11975 1
		}
		victory_points = {
			10394 1
		}
		victory_points = {
			7612 1
		}

		buildings = {
			infrastructure = 5
			industrial_complex = 1
			air_base = 1
			11975 = {
				naval_base = 1
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 6
				industrial_complex = 3
				air_base = 2
			}
		}
		1944.1.1 = {
			buildings = {
				industrial_complex = 4
			}
		}
	}

	provinces={
		638 968 1433 1480 5090 7118 7583 7612 9949 9996 10394 10437 10465 10492 11975 12325 12384 12470 12498 
	}
}
