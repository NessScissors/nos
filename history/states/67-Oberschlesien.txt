
state={
	id=67
	name="STATE_67"
	resources={
		steel=19.000
		aluminium=6.000
	}

	history={
		owner = GER
		add_core_of = GER
		victory_points = {
			11467 5 
		}
		victory_points = {
			9511 3 
		}
		buildings = {
			infrastructure = 5
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 6
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 7
			}
		}
	}

	provinces={
		479 9511 11467 
	}
	manpower=902290
	buildings_max_level_factor=1.000
	state_category=city
}
