state={
	id=1000
	name="STATE_1000"
	resources={
		oil=6.000
	}

	history={
		owner = RUS
		add_core_of = RUS
		victory_points = {
			10256 3 
		}
		buildings = {
			infrastructure = 1
		}
		1933.1.1 = {
			owner = SOV
			add_core_of = SOV
			remove_core_of = RUS
			buildings = {
				infrastructure = 2
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 3
			}
		}
	}

	provinces={
		1334 4215 4411 7281 7328 7401 10196 10224 10256 
	}
	manpower=720451
	buildings_max_level_factor=1.000
	state_category=rural
}
