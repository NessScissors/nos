
state={
	id=212
	name="STATE_212"

	history={
		owner = TUR
		add_core_of = BUL
		victory_points = {
			6923 5 
		}
		victory_points = {
			9862 1 
		}
		buildings = {
			infrastructure = 1
			air_base = 1
		}
		1933.1.1 = {
			owner = BUL
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 2
			}
		}
	}

	provinces={
		878 893 3937 6814 6923 6952 6982 9862 11813 
	}
	manpower=1611000
	buildings_max_level_factor=1.000
	state_category=large_town
}
