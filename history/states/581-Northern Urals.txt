
state={
	id=581
	name="STATE_581"
	resources={
		chromium=40.000
		aluminium=27.000
	}

	history={
		owner = RUS
		add_core_of = RUS
		1933.1.1 = {
			owner = SOV
			add_core_of = SOV
			remove_core_of = RUS
			buildings = {
				infrastructure = 1
			}
		}
	}

	provinces={
		4874 7824 10216 10551 10677 12502 12659 13076 
	}
	manpower=48831
	buildings_max_level_factor=1.000
	state_category=wasteland
}
