state={
	id=752
	name="STATE_752"
	provinces={
		1918 7988 8104 12724 12837 4999
	}
	manpower=300000
	buildings_max_level_factor=1.000
	state_category = pastoral
	history={
		owner = MAN
		add_core_of = CHI
		victory_points = {
			4999 1
		}
		add_core_of = MAN
		1933.1.1 = {
			owner = CHI
			remove_core_of = MAN
			add_core_of = PRC
			buildings = {
				infrastructure = 1
			}
		}

	}
}
