
state={
	id=97
	name="STATE_97"

	history={
		owner = RUS
		victory_points = {
			11301 3 
		}
		buildings = {
			infrastructure = 3
			air_base = 1

		}
		add_core_of = POL
		add_core_of = RUS
		1933.1.1 = {
			owner = POL
			remove_core_of = RUS
			buildings = {
				infrastructure = 4
				industrial_complex = 1
				air_base = 2
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 5
				industrial_complex = 2
			}
		}
	}

	provinces={
		290 347 400 11247 11274 11301 11329 11357 
	}
	manpower=1033200
	buildings_max_level_factor=1.000
	state_category=rural
}
