
state={
	id=951
	name="STATE_951"

	history={
		owner = AUS
		add_core_of = AUS
		victory_points = {
			688 10 
		}
		victory_points = {
			3673 3 
		}
		buildings = {
			infrastructure = 5
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 6
				industrial_complex = 1
			}
		}
		1944.1.1 = {
			owner = USA
			buildings = {
				industrial_complex = 2
			}
		}
	}

	provinces={
		688 3675 6691 11634 
	}
	manpower=1050133
	buildings_max_level_factor=1.000
	state_category=town
}
