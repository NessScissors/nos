state={
	id=1063
	name="STATE_1063"

	history={
		owner = TUR

		victory_points = {
			9930 1 
		}

		buildings = {
			infrastructure = 1

		}
		add_core_of = GRE
		add_core_of = BUL
		add_core_of = TUR
		1933.1.1 = {
			owner = GRE
			remove_core_of = BUL
			add_claim_by = BUL
			remove_core_of = TUR
			buildings = {
				infrastructure = 2
			}
		}

	}

	provinces={
		6990 9791 9930 
	}
	manpower=191000
	buildings_max_level_factor=1.000
	state_category=rural
}
