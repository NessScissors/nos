
state={
	id=281
	name="STATE_281"
	manpower=79300
	
	state_category = enclave

	history={
		owner = ENG
		add_core_of = MLD
		victory_points = {
			4967 1
		}
		buildings = {
			infrastructure = 1
			4967 = {
				naval_base = 1
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 2
				4967 = {
					naval_base = 2
				}
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 3
				4967 = {
					naval_base = 3
				}
			}
		}
	}

	provinces={
		4967 
	}
}
