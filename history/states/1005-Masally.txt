state={
	id=1005
	name="STATE_1005"

	history={
		owner = RUS
		buildings = {
			infrastructure = 1

		}
		add_core_of = RUS
		add_core_of = AZR
		1933.1.1 = {
			owner = SOV
			add_core_of = SOV
			remove_core_of = RUS
			buildings = {
				infrastructure = 2
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 3
			}
		}
	}

	provinces={
		4473 12434 
	}
	manpower=31020
	buildings_max_level_factor=1.000
	state_category=pastoral
}
