state = {
	id = 832
	name = "STATE_832"

	history = {
	
		victory_points = {
			1493 1
		}

		add_core_of = KHI
		add_core_of = TMS
		owner = RUS
		buildings = {
			infrastructure = 1
		}
		add_core_of = RUS
		1933.1.1 = {
			owner = SOV
			add_core_of = SOV
			remove_core_of = RUS
			buildings = {
				industrial_complex = 1
			}
		}
	}

	provinces = {
		1493 1649 7618 10485
	}
	manpower = 133065
	buildings_max_level_factor = 1.000
	state_category = pastoral

	local_supplies = 4.0 
	
}
