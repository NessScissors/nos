state={
	id=644
	name="STATE_644"

	impassable = yes

	history={
		owner = RUS
		buildings = {
			infrastructure = 0
		}
		add_core_of = RUS
		add_core_of = YAK
		1933.1.1 = {
			owner = SOV
			add_core_of = SOV
			remove_core_of = RUS
		}

	}

	provinces={
		1722 1745 1819 2853 2877 3184 4006 4229 4455 4878 6211 12549 12592 12634 13061 
	}
	manpower=68364
	buildings_max_level_factor=1
	state_category="wasteland"
}
