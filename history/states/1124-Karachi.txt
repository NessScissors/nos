state={
	id=1124
	name="STATE_1124"

	history={
		owner = RAJ
		victory_points = {
			3456 3 
		}
		buildings = {
			infrastructure = 3
			3456 = {
				naval_base = 2
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 5
				3456 = {
					naval_base = 4
				}
			}
		}
		1944.1.1 = {
			owner = PAK
			add_core_of = PAK
			buildings = {
				infrastructure = 6
				3456 = {
					naval_base = 5
				}
			}
		}
	}

	provinces={
		3456 7049 10108 10735 
	}
	manpower=1530000
	buildings_max_level_factor=1.000
	state_category=pastoral
}
