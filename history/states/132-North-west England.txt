state= {
	id=132
	name="STATE_132" # Lancashire
	manpower = 5770454
	
	state_category = metropolis
	
	resources={
		steel=31 # was: 56
		aluminium=2 # was: 4
	}

	history= {
		owner = ENG
		add_core_of = ENG
		victory_points = {
			6384 30 
		}
		victory_points = {
			6318 5 
		}
		buildings = {
			infrastructure = 6
			dockyard = 2
			air_base = 3
			6384 = {
				naval_base = 5
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 8
				arms_factory = 1
				industrial_complex = 1
				dockyard = 3
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 9
				arms_factory = 3
				industrial_complex = 2
				dockyard = 4
			}
		}
	}
	provinces= {
		3205 6318 6335 6384
 	}
}
