
state={
	id=670
	name="STATE_670"

	history={
		owner = VIN
		add_core_of = VIN
		add_core_of = LAO
		victory_points = {
			1464 1 
		}
		buildings = {
			infrastructure = 1
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 2
			}
		}
		1944.1.1 = {
			owner = LAO
			remove_core_of = VIN
			buildings = {
				infrastructure = 3
			}
		} 
	}

	provinces={
		1464 1507 4339 4568 4613 7075 7103 7218 7356 9961 10496 12392 12433 
	}
	manpower=743000
	buildings_max_level_factor=1.000
	state_category=rural
}
