
state={
	id=812
	name="STATE_812"

	history={
		owner = RUS
		victory_points = {
			3152 20 
		}
		buildings = {
			infrastructure = 2
			industrial_complex = 2
			air_base = 1
			3152 = {
				naval_base = 2
				coastal_bunker = 1

			}

		}
		add_core_of = EST
		add_core_of = RUS
		1933.1.1 = {
			owner = EST
			remove_core_of = RUS
			buildings = {
				infrastructure = 3 #was: 5
				arms_factory = 1
				industrial_complex = 3
				3152 = {
					naval_base = 3
				}
			}
		}

	}

	provinces={
		496 567 3152 11426 
	}
	manpower=275220
	buildings_max_level_factor=1.000
	state_category=city
}
