
state={
	id=372
	name="STATE_372"
	manpower = 1854481
	
	state_category = town
	
	resources={
		aluminium=14 # was: 20
	}


	history={
		owner = USA
		add_core_of = USA
		victory_points = {
			12489 1 
		}
		buildings = {
			infrastructure = 3
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 4
				industrial_complex = 2
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 5
				industrial_complex = 3
			}
		}
	}

	provinces={
		1503 1646 4444 4567 4597 4625 7485 7543 7586 10355 10477 12458 12489 12778 
	}
}
