
state={
	id=338
	name="STATE_338"
	manpower = 1800615
	resources={
		steel=9 # was: 16
	}

	state_category = city

	history={
		owner = ENG
		add_core_of = ENG
		victory_points = {
			3369 5 
		}
		buildings = {
			infrastructure = 5
			air_base = 1
			3369 = {
				naval_base = 1
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 7
				arms_factory = 1
				industrial_complex = 1
				air_base = 2
				3369 = {
					naval_base = 2
				}
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 8
				arms_factory = 3
				industrial_complex = 2
				air_base = 3
			}
		}
	}

	provinces={
		3369 6221 6351 6378 9484 11471 
	}
}
