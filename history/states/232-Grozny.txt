
state={
	id=232
	name="STATE_232"
	resources={
		oil=19.000
	}

	history={
		owner = RUS
		victory_points = {
			3672 20 
		}
		buildings = {
			infrastructure = 2

		}
		add_core_of = CHE
		add_core_of = RUS
		1933.1.1 = {
			owner = SOV
			remove_core_of = RUS
			add_core_of = SOV
			buildings = {
				infrastructure = 4

			}
		}

	}

	provinces={
		666 670 683 3672 11632 
	}
	manpower=548271
	buildings_max_level_factor=1.000
	state_category=rural
}
