
state={
	id=294
	name="STATE_294"
	manpower = 369000
	
	state_category = rural
	
	resources={
		oil=2 # was: 2
	}

	history={
		owner = OMA
		add_core_of = OMA
		buildings = {
			infrastructure = 1
			arms_factory = 1
			industrial_complex = 1 
			10760 = {
				naval_base = 1
			}
		}
		victory_points = {
			10760 3 
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 3 
				10760 = {
					naval_base = 2
				}
			}
		}
		1944.1.1 = {
			buildings = {
				arms_factory = 3
				industrial_complex = 4
			}
		}
	}

	provinces={
		1947 2018 2059 2103 4993 5049 8002 8020 10760 
	}
}
