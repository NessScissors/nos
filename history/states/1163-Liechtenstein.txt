state={
	id=1163
	name="STATE_1163"

	history={
		owner = LIE
		add_core_of = LIE
		victory_points = {
			13313 1 
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 6
				industrial_complex = 2
				arms_factory = 1
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 7
				industrial_complex = 3
				arms_factory = 2
			}
		}
	}

	provinces={
		13313 
	}
	manpower=29812
	buildings_max_level_factor=1.000
	state_category = rural
}
