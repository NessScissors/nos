state={
	id=891
	name="STATE_891"

	history={
		owner = RUS
		buildings = {
			infrastructure = 1

		}
		add_core_of = LAT
		add_core_of = RUS
		1933.1.1 = {
			owner = LAT
			add_core_of = SOV
			remove_core_of = RUS
			buildings = {
				infrastructure = 2
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 3
			}
		}
	}

	provinces={
		13268 
	}
	manpower=30000
	buildings_max_level_factor=1.000
	state_category=enclave
}
