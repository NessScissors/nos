state={
	id=933
	name="STATE_933"

	history={
		owner = RUS
		victory_points = {
			3393 3
		}

		buildings = {
			infrastructure = 2

		}
		add_core_of = BLR
		add_core_of = RUS
		1933.1.1 = {
			owner = POL
			add_core_of = POL
			remove_core_of = RUS
			buildings = {
				infrastructure = 3
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 4
			}
		}
	}

	provinces={
		318 3393 13206 
	}
	manpower=410200
	buildings_max_level_factor=1.000
	state_category=pastoral
}
