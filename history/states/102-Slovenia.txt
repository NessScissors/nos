
state={
	id=102
	name="STATE_102"

	history={
		owner = AUS
		buildings = {
			infrastructure = 2

		}
		victory_points = {
			9627 3 
		}
		add_core_of = SLV
		add_core_of = AUS
		1933.1.1 = {
			owner = YUG
			add_core_of = YUG
			remove_core_of = AUS
			buildings = {
				infrastructure = 4
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 5
			}
		}
	}

	provinces={
		6650 9627 
	}
	manpower=631200
	buildings_max_level_factor=1.000
	state_category=large_town
}
