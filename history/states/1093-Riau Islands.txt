state={
	id=1093
	name="STATE_1093"

	history={
		owner = INS
		add_core_of = INS
		buildings = {
			infrastructure = 2
			air_base = 1
			12113 = {
				naval_base = 1
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 3
				industrial_complex = 1
				air_base = 2
				12113 = {
					naval_base = 2
				}
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 4
				industrial_complex = 2
			}
		}
	}

	provinces={
		12113 
	}
	manpower=8100
	buildings_max_level_factor=1.000
	state_category=small_island
}
