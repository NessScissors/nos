state={
	id=96
	name="STATE_96"
	manpower = 726000
	
	state_category = rural
	
	history={
		victory_points = {
			406 1
		}
		owner = RUS
		buildings = {
			infrastructure = 2
		}
		add_core_of = BLR
		add_core_of = RUS
		1933.1.1 = {
			owner = POL
			add_core_of = POL
			remove_core_of = RUS
			buildings = {
				infrastructure = 3
			}
		}
		1944.1.1 = {
			owner = SOV
			add_core_of = SOV
			buildings = {
				infrastructure = 4
			}
		}
	}

	provinces={
		233 277 358 406 6340 9341 11391 
	}
}
