state={
	id=1068
	name="STATE_1068"

	history={
		owner = RUS
		buildings = {
			infrastructure = 2
			dockyard = 1
			6157 = {
				naval_base = 1
				coastal_bunker = 1
			}
		}
		add_core_of = FIN
		add_core_of = RUS
		1933.1.1 = {
			owner = FIN
			remove_core_of = RUS
			buildings = {
				infrastructure = 4
				6157 = {
					naval_base = 2
					coastal_bunker = 2
				}
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 5
			}
		}
	}

	provinces={
		6157 
	}
	manpower=15000
	buildings_max_level_factor=1.000
	state_category = pastoral
}
