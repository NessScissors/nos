state= {
	id=593
	name="STATE_593"
	manpower = 12700000
	state_category = town
	resources={
		tungsten=15 # was: 20
		steel= 15 # was: 20
		chromium = 3 # was: 4
	}

	history= {
		owner = MAN
		add_core_of = CHI
		buildings = {
			infrastructure = 1
			9938 = {
				naval_base = 1
				coastal_bunker = 1
			}
		}
		add_core_of = MAN
		1933.1.1 = {
			owner = GXC
			add_core_of = GXC
			add_core_of = PRC
			remove_core_of = MAN
			buildings = {
				infrastructure = 3
				industrial_complex = 1
				9938 = {
					naval_base = 2
					coastal_bunker = 2
					bunker = 1
				}

			}

		}
	}
	provinces={
		1078 1120 1162 1202 4050 4165 4207 7067 7108 7141 7182 9938 9970 9978 9997 10080 12014 12095 
	}
}
