
state={
	id=291
	name="STATE_291"
	resources={
		oil=15.000
	}

	history={
		owner = TUR
		victory_points = {
			2097 20 
		}
		buildings = {
			infrastructure = 2
			air_base = 1

		}
		add_core_of = SAU
		add_core_of = TUR
		1933.1.1 = {
			owner = IRQ
			add_core_of = IRQ
			remove_core_of = TUR
			remove_core_of = SAU
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 1
				air_base = 2

			}
		}

	}

	provinces={
		78 2004 2097 7977 7994 12046 12726 12855 
	}
	manpower=1423132
	buildings_max_level_factor=1.000
	state_category=town
}
