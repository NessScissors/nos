state=
{
	id=74
	name="STATE_74"
	manpower = 810000
	
	state_category = rural
	
	
	history=
	{
		victory_points = {
			3583 5
		}
		owner = AUS
		buildings = {
			infrastructure = 3
		}
		add_core_of = CZE
		add_core_of = AUS
		1933.1.1 = {
			owner = CZE
			remove_core_of = AUS
			buildings = {
				infrastructure = 5
				arms_factory = 1
			}
		}
	}
	provinces= {
		421 3414 3583 6485
 	}
}
