
state={
	id=274
	name="STATE_274"

	history={
		owner = ENG
		add_core_of = GHA
		victory_points = {
			10862 1 
		}
		buildings = {
			infrastructure = 2
			10862 = {
				naval_base = 1
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 3
				10862 = {
					naval_base = 2
				}
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 4
				10862 = {
					naval_base = 3
				}
			}
		}
	}

	provinces={
		8039 8096 10862 12742 12787 13136 
	}
	manpower=2963400
	buildings_max_level_factor=1.000
	state_category=rural
}
