
state={
	id=129
	name="STATE_129"
	manpower = 2212021
	
	state_category = city
	
	resources={
		steel=11 # was: 20
	}

	history={
		owner = ENG
		add_core_of = ENG
		buildings = {
			infrastructure = 4
			arms_factory = 1
			air_base = 3
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 6
				arms_factory = 2
				industrial_complex = 1
				air_base = 4
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 7
				arms_factory = 4
				industrial_complex = 2
			}
		}
	}

	provinces={
		364 3353 6237 9250 9268 9322 11279 
	}
}
