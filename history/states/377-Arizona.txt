
state={
	id=377
	name="STATE_377"
	manpower = 435572
	
	state_category = rural
	
	resources={
		steel=20 # was: 40
		oil=5 # was: 10
		aluminium=8 # was: 16
	}

	history={
		owner = USA
		remove_core_of = USA
		victory_points = {
			853 3 
		}
		victory_points = {
			3834 1 
		}
		buildings = {
			infrastructure = 2
			air_base = 1
		}
		set_state_flag = wild_west_state
		add_province_modifier = {
			static_modifiers = { wild_west_province }
			province = {
				all_provinces = yes
			}
		}
		1933.1.1 = {
			add_core_of = USA
			buildings = {
				infrastructure = 4
				industrial_complex = 1
				air_base = 2
			}
			clr_state_flag = wild_west_state
			remove_province_modifier = {
				static_modifiers = { wild_west_province }
				province = {
					all_provinces = yes
				}
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 5
				industrial_complex = 3
				air_base = 3
			}
		}
	}

	provinces={
		850 853 1793 3834 4796 6897 7747 7821 7920 8115 10573 10783 11814 12765 
	}
}
