
state={
	id=104
	name="STATE_104"
	resources={
		aluminium=7.000
	}

	history={
		owner = AUS
		victory_points = {
			11899 20 
		}
		buildings = {
			infrastructure = 2
			air_base = 1

		}
		add_core_of = BOS
		1933.1.1 = {
			owner = YUG
			remove_core_of = AUS
			add_core_of = YUG
			buildings = {
				infrastructure = 3
				industrial_complex = 1
				air_base = 2

			}

		}

	}

	provinces={
		6799 6957 9922 11572 11872 11899 
	}
	manpower=1423200
	buildings_max_level_factor=1.000
	state_category=town
}
