state={
	id=925
	name="STATE_925"

	history={
		owner = RUS
		add_core_of = MOL
		victory_points = {
			3407 1 
		}
		buildings = {
			infrastructure = 2

		}
		add_core_of = ROM
		add_core_of = UKR
		add_core_of = RUS
		1933.1.1 = {
			owner = ROM
			remove_core_of = RUS
			buildings = {
				infrastructure = 3
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 4
			}
		}
	}

	provinces={
		3407 
	}
	manpower=80400
	buildings_max_level_factor=1.000
	state_category=pastoral
}
