
state={
	id=39
	name="STATE_39"
	resources={
		aluminium=14.000
	}

	history={
		owner = AUS
		add_core_of = ITA
		add_core_of = AUS
		victory_points = {
			9630 1 
		}
		buildings = {
			infrastructure = 5
		}
		1933.1.1 = {
			owner = ITA
			buildings = {
				infrastructure = 6
				industrial_complex = 1
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 7
				industrial_complex = 2
			}
		}
	}

	provinces={
		6675 9630 11615 
	}
	manpower=301330
	buildings_max_level_factor=1.000
	state_category=rural
}
