
state={
	id=43
	name="STATE_43" # Northern Hungary
	manpower = 3750752
	resources={
		steel=3 # was: 4
		aluminium=100 # was: 194
	}
	
	state_category = large_city

	history={
		owner = AUS
		buildings = {
			infrastructure = 2
			arms_factory = 1
			industrial_complex = 1
			air_base = 1
		}
		victory_points = {
			9660 30 
		}
		victory_points = {
			6751 5 
		}
		add_core_of = HUN
		add_core_of = AUS
		1933.1.1 = {
			owner = HUN
			remove_core_of = AUS
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 2
				air_base = 2
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 5
				arms_factory = 3
				industrial_complex = 3
			}
		}
	}

	provinces={
		684 716 3713 3731 6716 6751 9660 9690 11520 
	}
}
