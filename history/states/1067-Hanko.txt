state={
	id=1067
	name="STATE_1067"

	history={
		owner = RUS
		buildings = {
			infrastructure = 3
			dockyard = 1
			air_base = 2
			11031 = {
				naval_base = 1
				coastal_bunker = 1
			}
		}
		add_core_of = FIN
		add_core_of = RUS
		1933.1.1 = {
			owner = FIN
			remove_core_of = RUS
			buildings = {
				infrastructure = 4
				air_base = 3
				11031 = {
					naval_base = 2
					coastal_bunker = 2
				}
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 5
			}
		}
	}

	provinces={
		11031 
	}
	manpower=10040
	buildings_max_level_factor=1.000
	state_category = pastoral
}
