state={
	id=753
	name="STATE_753"
	provinces={
		1257 2045 4375 4403 5193 9966 10787 10903 
	}
	manpower=3000000
	buildings_max_level_factor=1.000
	state_category = rural
	history={
		owner = MAN
		add_core_of = CHI
		victory_points = {
			2045 1 
		}
		buildings = {
			infrastructure = 1
		}
		add_core_of = MAN
		1933.1.1 = {
			owner = XSM
			add_core_of = XSM
			add_core_of = PRC
			remove_core_of = MAN
			buildings = {
				infrastructure = 3

			}

		}
	}
}
