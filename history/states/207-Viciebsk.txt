
state={
	id=207
	name="STATE_207"
	manpower =  1217854
	
	state_category = town

	history={
		owner = RUS
		victory_points = {
			11241 5
		}
		buildings = {
			infrastructure = 3
			air_base = 2
		}
		add_core_of = RUS
		add_core_of = BLR
		1933.1.1 = {
			owner = SOV
			add_core_of = SOV
			remove_core_of = RUS
			buildings = {
				infrastructure = 4
				air_base = 3
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 5
			}
		}
	}

	provinces={
		323 3219 3331 6220 6249 6326 6371 9241 9323 11220 11241 
	}
}
