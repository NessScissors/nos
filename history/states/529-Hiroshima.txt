
state={
	id=529
	name="STATE_529" # Chugoku
	manpower = 5341000
	state_category = large_city

	history={
		owner = JAP
		add_core_of = JAP
		victory_points = {
			1092 30
		}
		victory_points = {
			10055 5
		}
		victory_points = {
			7145 3
		}
		buildings = {
			infrastructure = 4
			air_base = 2
			dockyard = 1
			arms_factory = 1
			industrial_complex = 1
			1092 = {
				naval_base = 7
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 7
				air_base = 3
				dockyard = 2
				arms_factory = 2
				industrial_complex = 2
				1092 = {
					naval_base = 10
				}
			}
		}
	}

	provinces={
		1092 1167 1191 4197 7050 7145 7212 9968 10052 10055 11946 11985 
	}
}
