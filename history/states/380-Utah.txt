state= {
	id=380
	name="STATE_380"
	manpower = 507846
	
	state_category = rural
	
	resources={
		tungsten=76 # was: 106
	}

	history={
		owner = USA
		add_core_of = USA
		victory_points = {
			4865 3 
		}
		victory_points = {
			12699 1 
		}
		buildings = {
			infrastructure = 2
		}
		set_state_flag = wild_west_state
		add_province_modifier = {
			static_modifiers = { wild_west_province }
			province = {
				all_provinces = yes
			}
		}
		1933.1.1 = {
			buildings = {
				infrastructure = 4
				industrial_complex = 1
			}
			clr_state_flag = wild_west_state
			remove_province_modifier = {
				static_modifiers = { wild_west_province }
				province = {
					all_provinces = yes
				}
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 5
				industrial_complex = 2
			}
		}
	}
	provinces={
		1663 1740 1756 4668 4742 4865 7764 7818 10582 10651 10662 12573 12699 13153
	}
}
