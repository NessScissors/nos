
state={
	id=541
	name="STATE_541"

	history={
		owner = GER
		buildings = {
			infrastructure = 1
		}
		victory_points = {
			4879 1 
		}
		add_core_of = NMB
		1933.1.1 = {
			owner = SAF
			add_core_of = SAF
			buildings = {
				infrastructure = 3
			}
		}
	}

	provinces={
		2207 2229 4879 5154 5189 7605 7770 8136 10957 10963 10978 12519 12921 
	}
	manpower=441000
	buildings_max_level_factor=1.000
	state_category=wasteland
}
