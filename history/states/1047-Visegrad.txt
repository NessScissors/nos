state={
	id=1047
	name="STATE_1047"

	history={
		owner = AUS
		victory_points = {
			982 1 
		}
		buildings = {
			infrastructure = 1

		}
		add_core_of = BOS
		1933.1.1 = {
			owner = YUG
			remove_core_of = AUS
			add_core_of = YUG
			buildings = {
				infrastructure = 2
			}
		}

	}

	provinces={
		606 982 11741 
	}
	manpower=22100
	buildings_max_level_factor=1.000
	state_category=pastoral
}
