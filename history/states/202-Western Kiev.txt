
state={
	id=202
	name="STATE_202"
	resources={
		oil=8.000
		aluminium=7.000
	}

	history={
		owner = RUS
		victory_points = {
			525 25 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			air_base = 3
		}
		add_core_of = RUS
		add_core_of = UKR
		1933.1.1 = {
			owner = SOV
			add_core_of = SOV
			remove_core_of = RUS
			buildings = {
				infrastructure = 5
				arms_factory = 1
				industrial_complex = 2
				fuel_silo = 1
				air_base = 6
			}
		}

	}

	provinces={
		504 525 3494 3543 6497 9465 9568 
	}
	manpower=2495352
	buildings_max_level_factor=1.000
	state_category=city
}
