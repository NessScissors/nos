state={
	id=934
	name="STATE_934"

	history={
		owner = AUS
		add_core_of = SLV
		add_core_of = AUS
		victory_points = {
			596 1 
		}
		victory_points = {
			3631 3 
		}
		buildings = {
			infrastructure = 2
		}
		1933.1.1 = {
			owner = YUG
			add_core_of = YUG
			remove_core_of = AUS
			buildings = {
				infrastructure = 3
			}
		}
		1944.1.1 = {
			buildings = {
				infrastructure = 4
			}
		}
	}

	provinces={
		596 3631 3654 9596 
	}
	manpower=421000
	buildings_max_level_factor=1.000
	state_category=rural
}
