state={
	id=1117
	name="STATE_1117"

	history={
		owner = TUR
		buildings = {
			infrastructure = 1
			5081 = {
				naval_base = 1

			}

		}
		add_core_of = SAU
		add_core_of = TUR
		1933.1.1 = {
			owner = ENG
			add_core_of = ISR
			add_core_of = PAL
			remove_core_of = TUR
			remove_core_of = SAU
			buildings = {
				infrastructure = 3
			}
		}
	}

	provinces={
		5081 
	}
	manpower=26010
	buildings_max_level_factor=1.000
	state_category=enclave
}
