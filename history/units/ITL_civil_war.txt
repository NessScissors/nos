﻿division_template = {
	name = "Unità Partigiane"

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }		
	}
	support = {
       	engineer = { x = 0 y = 0 }
       	recon = { x = 0 y = 1 }
       	artillery = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Unità Partigiane del Motore" 	

	regiments = {
		motorized = { x = 0 y = 0 }
	    motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }
		motorized = { x = 0 y = 3 }		
		motorized = { x = 1 y = 0 }
	    motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }
		motorized = { x = 1 y = 3 }		
	}
	support = {
	 	engineer = { x = 0 y = 0 }
        recon = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Carri Armati Partigiani"

	regiments = {
		medium_armor = { x = 0 y = 0 }
		medium_armor = { x = 0 y = 1 }
        medium_armor = { x = 0 y = 2 }
		medium_armor = { x = 1 y = 0 }
		medium_armor = { x = 1 y = 1}
       	mechanized = { x = 2 y = 0 }
	}
	support = {
        recon = { x = 0 y = 0 }
        engineer = { x = 0 y = 1 }
        artillery = { x = 0 y = 2 }
	}
}

units = { 
	division = {
		name = "Italian Liberation Corps 1"
		location = 11773
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 2"
		location = 6891
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 3"
		location = 4159
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 4"
		location = 11773
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Motor Unit 1"
		location = 6891
		division_template = "Unità Partigiane del Motore" 
		start_equipment_factor = 1
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
			motorized_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Tank Unit 1"
		location = 4159
		division_template = "Carri Armati Partigiani"
		force_equipment_variants = {
			lt_equipment_0 = { owner = "ITL" creator = "ITA" }
			mechanized_equipment_1 = { owner = "ITL" creator = "ITA" }
			motorized_equipment_1 = { owner = "ITL" creator = "ITA" }
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 5"
		location = 11773
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 6"
		location = 6891
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 7"
		location = 4159
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 8"
		location = 11773
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Motor Unit 2"
		location = 6891
		division_template = "Unità Partigiane del Motore" 
		start_equipment_factor = 1
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
			motorized_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Tank Unit 2"
		location = 4159
		division_template = "Carri Armati Partigiani"
		force_equipment_variants = {
			lt_equipment_0 = { owner = "ITL" creator = "ITA" }
			mechanized_equipment_1 = { owner = "ITL" creator = "ITA" }
			motorized_equipment_1 = { owner = "ITL" creator = "ITA" }
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 9"
		location = 11773
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 10"
		location = 6891
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 11"
		location = 4159
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 12"
		location = 11773
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Motor Unit 3"
		location = 6891
		division_template = "Unità Partigiane del Motore" 
		start_equipment_factor = 1
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
			motorized_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Tank Unit 3"
		location = 4159
		division_template = "Carri Armati Partigiani"
		force_equipment_variants = {
			lt_equipment_0 = { owner = "ITL" creator = "ITA" }
			mechanized_equipment_1 = { owner = "ITL" creator = "ITA" }
			motorized_equipment_1 = { owner = "ITL" creator = "ITA" }
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 13"
		location = 11773
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
	division = {
		name = "Italian Liberation Corps 14"
		location = 6891
		division_template = "Unità Partigiane"
		force_equipment_variants = {
			infantry_equipment_0 = { owner = "ITL" creator = "ITA" }
			artillery_equipment_1 = { owner = "ITL" creator = "ITA" }
		}
	}
}