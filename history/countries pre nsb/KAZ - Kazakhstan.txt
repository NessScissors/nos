﻿capital = 586
1910.1.1 = {
		
	set_research_slots = 2
	set_stability = 0.38
	set_convoys = 0
	add_namespace = { name = "unit_leader_sov" type = unit_leader }
	create_country_leader = {
		name = "Alikhan Bukeikhanov"
		picture="gfx/leaders/KAZ/KAZ_Alikhan_Bukeikhanov.dds"
		expire="1935.1.1" 
		ideology = moderatism
		traits = {} 
	}
	create_country_leader = {
		name = "Alikhan Bukeikhanov"
		picture="gfx/leaders/KAZ/KAZ_Alikhan_Bukeikhanov.dds"
		expire="1935.1.1" 
		ideology = conservatism
		traits = {} 
	}
	set_technology = {
		infantry_weapons = 1
		motorised_infantry = 1
		tech_support = 1
		tech_recon = 1
	}
	set_politics = {
		ruling_party = neutrality
		last_election = "1910.1.1" 
		election_frequency = 48 
		elections_allowed = no 
	}	
	set_popularities = {
		fascism = 2
		communism = 13
		conservative = 11
		neutrality = 74
	}
	every_unit_leader = {
		set_unit_leader_flag = ww1_general
	}
}

1933.1.1 = {
	every_unit_leader = {
		limit = { has_unit_leader_flag = ww1_general }
		retire = yes
	}

	retire_ideology_leader = monarchism
	retire_ideology_leader = communism
	retire_ideology_leader = unionism
	retire_ideology_leader = neutrality
	retire_ideology_leader = fascism
	retire_ideology_leader = social_democracy
	retire_ideology_leader = democratic
	retire_ideology_leader = conservative

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_armor_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	remove_ideas = {
	}

	oob = "KAZ_1936"

	set_technology = {
		gw_artillery = 1
		interwar_artillery = 1
		interwar_antiair = 1
		gwtank = 1
		basic_light_tank = 1
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		support_weapons = 1
		tech_support = 1
		tech_recon = 1
		tech_mountaineers = 1
		tech_engineers = 1
		early_fighter = 1
		early_bomber = 1
		CAS1 = 1
	}

	set_popularities = {
		democratic = 0
		fascism = 0
		communism = 92
		neutrality = 8
	}

	set_politics = {
		ruling_party = communism
		last_election = "1928.5.8"
		election_frequency = 120
		elections_allowed = no
	}

	create_country_leader = {
		name = "Iskhak Razzakov"
		desc = ""
		picture = "gfx/leaders/KAZ/KZK_Razzakov.dds"
		ideology = liberalism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Tolubayev Asanali"
		desc = ""
		picture = "gfx/leaders/KAZ/Portrait_Asanali.dds"
		ideology = fascism_ideology
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Aleksey Vlasovich Vagov"
		desc = ""
		picture = "gfx/leaders/KAZ/Portrait_Vagov.dds"
		ideology = despotism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Abdisamet Kazakhpayev"
		desc = ""
		picture = "gfx/leaders/KAZ/KZK_Kazakhpayev.dds"
		ideology = stalinism
		traits = {
			#
		}
	}
}