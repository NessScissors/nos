﻿capital = 46

set_research_slots = 2
set_stability = 0.75
set_war_support = 0.5
set_convoys = 13

create_country_leader={ 
	name = "King Carol I" 
	picture="gfx/leaders/ROM/ROM_carol_i.dds" 
	expire="1935.1.1" 
	ideology = absolutism 
	traits={  } 
}

create_field_marshal={  
	name = "Kliment Boyadzhiev" 
	portrait_path="gfx/hoi4tgw_portraits/ROM/army_generals/ROM_Kliment_Boyadzhiev.dds" 
	traits={ fast_planner  } 
	skill=3 
}

create_field_marshal={
	name = "Constantin Prezan" id=92101
	portrait_path="gfx/hoi4tgw_portraits/ROM/army_generals/ROM_Constantin_Prezan.dds"
	traits={ offensive_doctrine old_guard }
	skill=3
}

create_field_marshal={
	name = "Alexandru Averescu" id=92102
	portrait_path="gfx/hoi4tgw_portraits/ROM/army_generals/ROM_Alexandru_Averescu.dds"
	traits={ offensive_doctrine }
	skill=3
}

create_field_marshal={
	name = "Grigore Crăiniceanu" id=92103
	portrait_path="gfx/hoi4tgw_portraits/ROM/army_generals/ROM_Grigore_Crainiceanu.dds"
	traits={ defensive_doctrine }
	skill=2
}

create_field_marshal={
	name = "Ioan_Culcer" id=92104
	portrait_path="gfx/hoi4tgw_portraits/ROM/army_generals/ROM_Ioan_Culcer.dds"
	traits={ defensive_doctrine old_guard }
	skill=2
}


create_corps_commander={
	name = "Artur Văitoianu" id=92105
	portrait_path="gfx/hoi4tgw_portraits/ROM/army_generals/ROM_Artur_Vaitoianu.dds"
	traits={ }
	skill=3
}

create_corps_commander={
	name = "Eremia Grigorescu" id=92106
	portrait_path="gfx/hoi4tgw_portraits/ROM/army_generals/ROM_Eremia_Grigorescu.dds"
	traits={ }
	skill=2
}

create_corps_commander={
	name = "Ion Dragalina" id=92107
	portrait_path="gfx/hoi4tgw_portraits/ROM/army_generals/ROM_Ion_Dragalina.dds"
	traits={ }
	skill=2
}

create_corps_commander={
	name = "Constantin Coandă" id=92108
	portrait_path="gfx/hoi4tgw_portraits/ROM/army_generals/ROM_Constantin_Coanda.dds"
	traits={ }
	skill=2
}


create_navy_leader={  name = "Nigrescu" id=75021 portrait_path="gfx/admiral/ROM_nigrescu.tga" traits={   } skill=3 }
create_navy_leader={  name = "Georgescu I." id=75033 portrait_path="gfx/admiral/ROM_georgescu_i.tga" traits={   } skill=3 }
create_navy_leader={  name = "Sebestian" id=75034 portrait_path="gfx/admiral/ROM_sebestian.tga" traits={   } skill=3 }
#create_navy_leader={  name = "Negru" id=75037 portrait_path="gfx/admiral/ROM_negru.tga" traits={   } skill=1 }
create_navy_leader={  name = "Balescu" id=75038 portrait_path="gfx/admiral/ROM_balescu.tga" traits={   } skill=3 }
create_navy_leader={  name = "Niculescu-Rizea" id=75039 portrait_path="gfx/admiral/ROM_niculescu_rizea.tga" traits={   } skill=2 }
#create_navy_leader={  name = "Scodrea" id=75057 portrait_path="gfx/admiral/ROM_scodrea.tga" traits={   } skill=1 }

1910.1.1 = {
	oob = "ROM_1910"

	set_technology = {
		#infantry
		infantry_weapons = 1
		tech_mountaineers = 1
		
		#support
		tech_support = 1
		tech_engineers = 1

		#artillery
		gw_artillery = 1

		#engineering
		electrical_engineering = 1
		mechanical_engineering = 1
	}

	set_politics = {
		ruling_party = monarchism
		last_election = "1910.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		conservative = 21
		fascism = 0
		communism = 0
		monarchism = 79
	}

	add_ideas = {
		limited_conscription
	}
	every_unit_leader = {
		set_unit_leader_flag = ww1_general
	}
}

1933.1.1 = {
	every_unit_leader = {
		limit = { has_unit_leader_flag = ww1_general }
		retire = yes
	}

	retire_ideology_leader = monarchism
	retire_ideology_leader = communism
	retire_ideology_leader = unionism
	retire_ideology_leader = neutrality
	retire_ideology_leader = fascism
	retire_ideology_leader = social_democracy
	retire_ideology_leader = democratic
	retire_ideology_leader = conservative

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_armor_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	remove_ideas = {
	}

	load_focus_tree = ww2_romania
	set_country_flag=carol_dead
	if = {
		limit = { has_dlc = "Man the Guns" }
		set_naval_oob = "ROM_1936_naval_mtg"
		else = {
			set_naval_oob = "ROM_1936_naval_legacy"
		}
	}

	oob = "ROM_1936"

	set_research_slots = 3

	set_cosmetic_tag = ROM_monarchy

	# Starting tech
	set_technology = {
		tech_support = 1		
		tech_engineers = 1
		tech_mountaineers = 1
		motorised_infantry = 1
		gwtank = 1
		basic_light_tank = 1
		infantry_weapons = 1
		infantry_weapons1 = 1
		gw_artillery = 1
		early_fighter = 1
		fighter1 = 1
		early_submarine = 1
		early_destroyer = 1
		early_light_cruiser = 1
		basic_destroyer = 1
		basic_submarine = 1
		basic_light_cruiser = 1
		early_battleship = 1
		early_battlecruiser = 1
		transport = 1
	}

	#Ideas
	if = {
		limit = {
			has_dlc = "Death or Dishonor"
		}
		add_ideas = {
			ROM_king_carol_ii_hedonist
		}
	}


	#Kick off kings crazy life
	if = {
		limit = {
			has_dlc = "Death or Dishonor"
		}
		ROM = {
			#Pick from list of the sane events first
			random_list = {
				100 = { country_event = { id = DOD_romania.81 days = 2 random = 20 } }
			}
		}
	}

	set_convoys = 200
	set_stability = 0.5
	set_war_support = 0.2
	add_ideas = {
		neutrality_idea
		limited_conscription
	}

	set_popularities = {
		democratic = 15
		fascism = 18
		communism = 2
		monarchism = 25
		neutrality = 40
	}

	set_politics = {
		ruling_party = neutrality
		last_election = "1933.12.20"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Gheorghe Tatarescu"
		desc = "POLITICS_GHEORGHE_TATARESCU_DESC"
		picture = "Portrait_Romania_Gheorghe_Tatarescu.dds"
		expire = "1965.1.1"
		ideology = centrism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Constantin Ion Parhon"
		desc = "POLITICS_CONSTANTIN_PARHON_DESC"
		picture = "Portrait_Romania_Constantin_Parhon.dds"
		expire = "1965.1.1"
		ideology = leninism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Armand Calinescu"
		desc = "POLITICS_ARMAND_CALINESCU_DESC"
		picture = "gfx/leaders/ROM/Portrait_Romania_Armand_Calinescu.dds"
		expire = "1965.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Octavian Goga"
		desc = "POLITICS_OCTAVIAN_GOGA_DESC"
		picture = "Portrait_Romania_Octavian_Goga.dds"
		expire = "1965.1.1"
		ideology = fascism_ideology
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Ion Antonescu"
		desc = "POLITICS_ION_ANTONESCU_DESC"
		picture = "Portrait_Romania_Ion_Antonescu.dds"
		expire = "1965.1.1"
		ideology = fascism_ideology
		traits = {
			#
		}
	}

	create_field_marshal = {
		name = "Petre Dumitrescu"
		portrait_path = "gfx/leaders/ROM/Portrait_romania_petre_dumitrescu.dds"
		traits = { offensive_doctrine logistics_wizard }
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 2
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Ion Antonescu"
		picture = "Portrait_Romania_Ion_Antonescu.dds"
		traits = { offensive_doctrine }
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 2
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Ioan Mihail Racovita"
		portrait_path = "gfx/leaders/ROM/Portrait_romania_ioan_mihail_racovita.dds"
		traits = { hill_fighter }
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Gheorghe Avramescu"
		portrait_path = "gfx/leaders/ROM/Portrait_romania_gheorghe_avramescu.dds"
		traits = { }
		skill = 3
		attack_skill = 3
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Barbu Paraianu"
		portrait_path = "gfx/leaders/ROM/portrait_romania_barbu_paraianu.dds"
		traits = { hill_fighter }
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 3
	}

	create_navy_leader = {
		name = "Horia Macellariu"
		portrait_path = "gfx/leaders/ROM/Portrait_romania_horia_macellariu.dds"
		traits = { spotter }
		skill = 2
	}

}

1944.1.1 = {
	drop_cosmetic_tag = yes
}