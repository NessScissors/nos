﻿capital = 315

set_research_slots = 2
set_stability = 0.75
set_convoys = 20
set_country_flag = monroe_doctrine

1910.1.1 = {
	create_country_leader = { 
		name = "José Miguel Gómez"
		picture="gfx/leaders/CUB/CUB_jose_miguel_gomez.dds" 
		expire="1935.1.1" 
		ideology=liberalism 
		traits={  } 
	}
	create_corps_commander = {  
		name = "José Miguel Gómez"
		traits = { harsh_leader politically_connected }
		skill = 2
		attack_skill = 1
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 2
	}
	create_navy_leader = {  
		name = "Carricarte"
		traits = { gentlemanly } 
		skill = 1
		attack_skill = 1
		defense_skill = 2
		maneuvering_skill = 2
		coordination_skill = 1
	}
		

		set_technology = {
			#infantry
			infantry_weapons = 1
			infantry_weapons1 = 1
			
			#artillery
			gw_artillery = 1
			
			#engineering
		}

	set_politics = {
		ruling_party = democratic
		last_election = "1909.1.1" 
		election_frequency = 48 
		elections_allowed = yes
	}	
	set_popularities = {
		fascism = 0
		communism = 3
		democratic = 64
		neutrality = 33
	}
	every_unit_leader = {
		set_unit_leader_flag = ww1_general
	}
}

1933.1.1 = {
	every_unit_leader = {
		limit = { has_unit_leader_flag = ww1_general }
		retire = yes
	}

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_armor_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	retire_ideology_leader = monarchism
	retire_ideology_leader = communism
	retire_ideology_leader = unionism
	retire_ideology_leader = neutrality
	retire_ideology_leader = fascism
	retire_ideology_leader = social_democracy
	retire_ideology_leader = democratic
	retire_ideology_leader = conservative

	remove_ideas = {
	}

	oob = "CUB_1936"

	# Starting tech
	set_technology = {
		infantry_weapons = 1
		early_fighter = 1
	}
	set_country_flag = monroe_doctrine

	set_convoys = 20

	set_popularities = {
		democratic = 90
		fascism = 0
		communism = 10
		neutrality = 0
	}

	set_politics = {
		ruling_party = democratic
		last_election = "1936.1.10"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "José Agripino Barnet"
		desc = "POLITICS_JOSE_AGRIPINO_BARNET_DESC"
		picture = "Portrait_Cuba_Jose_Agripino_Barnet.dds"
		expire = "1965.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}
}
