﻿capital = 318

#-------------------------------------------------------
#					GENERIC VALUES
#-------------------------------------------------------
set_fuel_ratio = 0.20
set_research_slots = 1
set_stability = 0.40
set_convoys = 100
set_country_flag=monroe_doctrine
#-------------------------------------------------------
#					POLITICS & ARMY
#-------------------------------------------------------

create_country_leader = { 
	name = "Cincinnatus Leconte" 
	picture = "gfx/leaders/HAI/HAI_Leconte.dds" 
	expire = "1935.1.1" 
	ideology = despotism  
	traits = {  } 
}
create_country_leader = { 
	name = "Antoine Simon" 
	picture = "gfx/leaders/HAI/HAI_francois_c_antoine_simon.dds" 
	expire = "1935.1.1" 
	ideology = conservatism 
	traits = { } 
}
create_corps_commander={  
name = "Cincinnatus Leconte" 
traits={ } 
portrait_path="gfx/leaders/HAI/HAI_Leconte.dds" 
skill=1 
}

1910.1.1 = {
	oob = "HAI_1910"

	set_technology = {
		#infantry
		infantry_weapons = 1
		infantry_weapons1 = 1
		
		#artillery
		gw_artillery = 1
		
		#engineering
	}

	set_politics = {
	    ruling_party = conservative
	    last_election = "1908.1.1"
	    election_frequency = 36
	    elections_allowed = yes
	}
	set_popularities = {
	    conservative = 63
	    fascism = 1
	    communism = 0
	    neutrality = 36
	}

	add_ideas = {

	}	
	every_unit_leader = {
		set_unit_leader_flag = ww1_general
	}
}

1933.1.1 = {
	every_unit_leader = {
		limit = { has_unit_leader_flag = ww1_general }
		retire = yes
	}

	oob = "HAI_1936"

	retire_ideology_leader = monarchism
	retire_ideology_leader = communism
	retire_ideology_leader = unionism
	retire_ideology_leader = neutrality
	retire_ideology_leader = fascism
	retire_ideology_leader = social_democracy
	retire_ideology_leader = democratic
	retire_ideology_leader = conservative

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_armor_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	set_research_slots = 3
	# Starting tech
	set_technology = {
		infantry_weapons = 1
	}
	set_country_flag = monroe_doctrine

	set_convoys = 5

	set_popularities = {
		conservative = 90
		fascism = 10
		communism = 0
		neutrality = 0
	}

	set_politics = {
		ruling_party = conservative
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}

	create_country_leader = {
		name = "Sténio Vincent"
		desc = "POLITICS_STENIO_VINCENT_DESC"
		picture = "Portrait_Haiti_Stenio_Vincent.dds"
		expire = "1965.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Jacques Roumain"
		desc = "POLITICS_JACQUES_ROUMAIN_DESC"
		picture = "Portrait_Haiti_Jacques_Roumain.dds"
		expire = "1965.1.1"
		ideology = marxism
		traits = {
			#
		}
	}
}