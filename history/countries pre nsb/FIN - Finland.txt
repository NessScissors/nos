﻿capital = 111

1910.1.1 = {
	set_research_slots = 3
	set_stability = 0.36
	set_war_support = 0.74
	set_convoys = 30

	create_country_leader = {
		name = "Pehr Evind Svinhufvud" 
		desc = FIN_pehr_evind_svinhufvud_DESC 
		picture = "gfx/leaders/FIN/FIN_pehr_evind_svinhufvud.dds" 
		expire = "1935.1.1" 
		ideology = conservatism 
		traits = {}
	}
	create_country_leader = {
		name = "Carl Gustaf Mannerheim" 
		picture = "gfx/leaders/FIN/FIN_carl_mannerheim.dds" 
		expire = "1935.1.1" 
		ideology = oligarchism
		traits = {}
	}
	create_country_leader = {
		name = "Kullervo Manner" 
		picture = "gfx/leaders/FIN/FIN_kullervo_manner.dds" 
		expire = "1935.1.1" 
		ideology = leninism
		traits = {}
	}

	create_corps_commander = {
		name = "Carl Gustaf Mannerheim" 
		portrait_path = "gfx/leaders/FIN/FIN_carl_mannerheim.dds" 
		traits = { war_hero trickster winter_specialist } 
		id = 100
		skill = 4
		attack_skill = 2
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Ernst Linder" 
		id = 101
		portrait_path = "gfx/leaders/FIN/FIN_ernst_linder.dds" 
		traits = { } 
		skill = 2
	}

	set_technology = {
		#infantry
		infantry_weapons = 1
		support_weapons = 1
		
		#support
		tech_support = 1
		
		#artillery
		gw_artillery = 1
		
		#navy
		transport = 1

		#air
		early_fighter = 1
		airship_bomber = 1
		
		#engineering
		electrical_engineering = 1
		radio = 1
		mechanical_engineering = 1
		aircraft_engine = 1
		generator_battery = 1
	}

	set_politics = {
		ruling_party = neutrality
		last_election = "1910.1.1" 
		election_frequency = 60
		elections_allowed = no
	}
	set_popularities = {
		fascism = 1
		communism = 25
		democratic = 39
		neutrality = 35
	}
	add_ideas = {
		sisu
	}
	every_unit_leader = {
		set_unit_leader_flag = ww1_general
	}
}

1933.1.1 = {
	load_focus_tree = ww2_finland
	every_unit_leader = {
		limit = { has_unit_leader_flag = ww1_general }
		retire = yes
	}

	retire_ideology_leader = monarchism
	retire_ideology_leader = communism
	retire_ideology_leader = unionism
	retire_ideology_leader = neutrality
	retire_ideology_leader = fascism
	retire_ideology_leader = social_democracy
	retire_ideology_leader = democratic
	retire_ideology_leader = conservative

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_armor_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	remove_ideas = {
	}

	oob = "FIN_1936"

	if = {
		limit = { has_dlc = "Man the Guns" }
		set_naval_oob = "FIN_1936_naval_mtg"
		else = {
			set_naval_oob = "FIN_1936_naval_legacy"
		}
	}

	set_research_slots = 3

	add_ideas = {
		sisu
		limited_conscription
		FIN_isolation
		FIN_lotta_svard
	}

	set_technology = {
		infantry_weapons = 1
		infantry_weapons1 = 1
		gw_artillery = 1
		gwtank = 1
		tech_support = 1		
		tech_recon = 1
		tech_engineers = 1
		early_destroyer = 1
		early_submarine = 1
		early_light_cruiser = 1
		early_heavy_cruiser = 1
		early_fighter = 1
		naval_bomber1 = 1
		CAS1 = 1
		cv_naval_bomber1 = 1
		early_bomber = 1
		
		force_rotation = 1
		trench_warfare = 1
	}

	set_popularities = {
		conservative = 44
		fascism = 10
		communism = 6
		neutrality = 40
	}

	set_politics = {
		ruling_party = conservative
		last_election = "1931.1.16"
		election_frequency = 72
		elections_allowed = yes
	}

	set_convoys = 25
	set_stability = 0.75
	set_war_support = 0.7

	create_country_leader = {
		name = "Risto Ryti"
		desc = ""
		picture = "gfx/leaders/FIN/FIN_Risto_Ryti.dds"
		expire = "1965.1.1"
		ideology = centrism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Pehr Svinhufvud"
		desc = "POLITICS_PER_EVIND_SVINHUFVUD_DESC"
		picture = "FIN_Pehr_Svinhufvud.dds"
		expire = "1965.1.1"
		ideology = conservatism
		traits = {
			#
		}
		id = 15002
	}

	create_country_leader = {
		name = "Aimo Aaltonen"
		desc = "POLITICS_AIMO_AALTONEN_DESC"
		picture = "gfx/leaders/LAT/portrait_lat_karlis_ulmanis.dds"
		expire = "1965.1.1"
		ideology = marxism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Vilho Annala"
		desc = "POLITICS_VILHO_ANNALA_DESC"
		picture = "gfx/leaders/IRE/Portrait_Ireland_Eamon_de_Valera.dds"
		expire = "1965.1.1"
		ideology = fascism_ideology
		traits = {
			#
		}
	}

	create_field_marshal = {
		name = "Carl Gustaf Mannerheim"
		portrait_path = "gfx/leaders/SCA/Portrait_SCA_Mannerheim.dds"
		traits = { brilliant_strategist war_hero career_officer unyielding_defender inspirational_leader }
		skill = 5
		attack_skill = 4
	    defense_skill = 5
	    planning_skill = 5
	    logistics_skill = 5
		
	}

	create_corps_commander = { # Commander II Corps
		name = "Harald Öhquist"
		portrait_path = "gfx/leaders/SCA/Portrait_SCA_Harald_Ohquist.dds"
		traits = { trait_reckless winter_specialist hill_fighter }
		skill = 3
		attack_skill = 5
	    defense_skill = 1
	    planning_skill = 2
	    logistics_skill = 2
	}

	create_corps_commander = { # Commander III Corps
		name = "Axel Erik Heinrichs"
		portrait_path = "gfx/leaders/SCA/Portrait_SCA_Axel_Heinrichs.dds"
		traits = { infantry_officer winter_specialist ranger }
		skill = 4
	    attack_skill = 4
	    defense_skill = 4
	    planning_skill = 3
	    logistics_skill = 2
	}

	create_corps_commander = { # Commander IV Corps
		name = "Johan Woldemar Hägglund"
		portrait_path = "gfx/leaders/Europe/Portrait_europe_generic_land_6.dds"
		traits = { inflexible_strategist winter_specialist trickster }
		skill = 4
		attack_skill = 4
	    defense_skill = 4
	    planning_skill = 3
	    logistics_skill = 3
	}

	create_corps_commander = { # Commander Reserve Corps
		name = "Wiljo Einar Tuompo"
		portrait_path = "gfx/leaders/SCA/Portrait_SCA_Wiljo_Tuompo.dds"
		traits = { career_officer winter_specialist trait_engineer }
		skill = 3
		attack_skill = 2
	    defense_skill = 2
	    planning_skill = 3
	    logistics_skill = 4
	}

	create_navy_leader = {
		name = "Eero Rahola"
		portrait_path = "gfx/leaders/ITA/Portrait_Italy_Generic_navy_3.dds"
		traits = { ironside }
		skill = 3
	}
}