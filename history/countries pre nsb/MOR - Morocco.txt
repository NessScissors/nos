﻿capital = 461

add_ideas = {
	etgi_laws_leadership_structure_d
}

1910.1.1 = {
	set_stability = 0.25
	set_war_support = 0.30
	oob = "MOR_1910"

	set_technology = {
		#infantry
		infantry_weapons = 1
		
		#support
		tech_support = 1
		tech_engineers = 1

		#artillery
		gw_artillery = 1

		#navy
		transport = 1
		
		#engineering
		electrical_engineering = 1
		mechanical_engineering = 1
	}
	set_politics = {
	    ruling_party = monarchism
	    last_election = "1910.1.1"
	    election_frequency = 48
	    elections_allowed = no
	}
	set_popularities = {
	    democratic = 0
	    fascism = 0
	    communism = 0
	    monarchism = 100
	}
	FRA = {
		diplomatic_relation = {
	 	   country = MOR
	 	   relation = military_access
	  	  active = yes
		}
	}
	add_ideas = FRA_idea_moroccan_intervention
	add_opinion_modifier = {
		target = FRA
		modifier = FRA_french_intervention
	}
	create_country_leader = {
		name = "Mulay Abdelhafid"
		picture="gfx/leaders/MOR/MOR_mulay_abdelhafid.dds" 
		expire="1935.1.1" 
		ideology = absolutism  
		traits={ } 
	}

	#create_country_leader = { 
	#	name = "Hubert Lyautey"
	#	picture="gfx/leaders/FRA/FRA_hubert_lyautey.dds" 
	#	expire="1935.1.1" 
	#	ideology = despotism  
	#	traits={  } 
	#}
	every_unit_leader = {
		set_unit_leader_flag = ww1_general
	}
}

1933.1.1 = {
	every_unit_leader = {
		limit = { has_unit_leader_flag = ww1_general }
		retire = yes
	}
	load_focus_tree = ww2_morocco
	retire_ideology_leader = monarchism
	retire_ideology_leader = communism
	retire_ideology_leader = unionism
	retire_ideology_leader = neutrality
	retire_ideology_leader = fascism
	retire_ideology_leader = social_democracy
	retire_ideology_leader = democratic
	retire_ideology_leader = conservative

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_armor_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	remove_ideas = {
	}

	oob = "MOR_1936"

	if = {
		limit = { has_dlc = "Together for Victory" } 
		FRA = {
			set_autonomy = {
				target = MOR
				autonomous_state = autonomy_federal_puppet
				freedom_level = 0.3
			}
		}
		set_popularities = {
			democratic = 0
			fascism = 0
			communism = 0
			monarchism = 100
		}

		set_politics = {
			ruling_party = monarchism
			last_election = "1936.1.1"
			election_frequency = 48
			elections_allowed = no
		}
		else = {
			FRA = {
				transfer_state = 461
				transfer_state = 462
			}
		}
	}
	set_popularities = {
		democratic = 0
		fascism = 0
		communism = 0
		monarchism = 100
	}

	set_politics = {
		ruling_party = monarchism
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}

	if = {
		limit = { 
			has_dlc = "Together for Victory" 
		}
		add_to_tech_sharing_group = french_research
	}

	create_country_leader = {
		name = "Fatih taqi"
		desc = ""
		picture = "gfx/leaders/PAL/Portrait_Arabia_Generic_2.dds"
		ideology = liberalism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Hazem Nahri"
		desc = ""
		picture = "gfx/leaders/PAL/Portrait_Arabia_Generic_1.dds"
		ideology = fascism_ideology
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Osama Danid"
		desc = ""
		picture = "gfx/leaders/PAL/Portrait_Arabia_Generic_2.dds"
		ideology = leninism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "King Mohammed V"
		desc = "POLITICS_MOR_DESC"
		picture = "gfx/leaders/MOR/Portrait_Morocco_Mohammed_V.dds"
		expire = "1965.1.1"
		ideology = absolutism
	}
}