﻿capital = 314

1910.1.1 = {
	set_stability = 0.75
	set_convoys = 20
	set_country_flag = monroe_doctrine

	create_country_leader = { 
		name = "Fernando Figueroa" 
		picture="gfx/leaders/ELS/ELS_fernando_figueroa.dds"
		expire="1935.1.1" 
		ideology = oligarchism
		traits = { } 
	}

	set_research_slots = 3

		set_technology = {
			#infantry
			infantry_weapons = 1
			infantry_weapons1 = 1
			
			#artillery
			gw_artillery = 1
			
			#engineering
		}

	set_politics = {
		ruling_party = neutrality
		last_election = "1910.3.1" 
		election_frequency = 48 
		elections_allowed = yes 
	}	
	set_popularities = {
		fascism = 1
		unionism = 5
		conservative = 38
		social_democracy = 10
		neutrality = 46
	}

	oob = "ELS_1910"

}

1933.1.1 = {
	# Starting tech
	set_technology = {
		infantry_weapons = 1
		early_fighter = 1
	}
	set_country_flag = monroe_doctrine

	set_convoys = 5

	set_popularities = {
		conservative = 20
		fascism = 75
		communism = 5
		neutrality = 0
	}

	set_politics = {
		ruling_party = fascism
		last_election = "1935.1.15"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Maximiliano Hernández"
		desc = "POLITICS_MAXIMILIANO_HERNANDEZ_MARTINEZ_DESC"
		picture = "Portrait_El Salvador_Maximiliano_Hernandez_Martinez.dds"
		expire = "1965.1.1"
		ideology = fascism_ideology
		traits = {
			#
		}
	}
}