﻿capital = 289

set_cosmetic_tag = SIA_monarchy

1910.1.1 = {
	oob="SIA_1910"

	set_technology = {
		#infantry
		infantry_weapons = 1
		
		#support
		tech_support = 1
		
		#artillery
		gw_artillery = 1
		
		#navy
		transport = 1
	}

	set_politics = {
	    ruling_party = neutrality
	    last_election = "1910.1.1"
	    election_frequency = 48
	    elections_allowed = no
	}
	set_popularities = {
	    democratic = 0
	    fascism = 0
	    communism = 0
	    neutrality = 100
	}

	set_stability=0.75
	set_convoys=15

	create_country_leader = {
		name = "Chulalongkorn"
		picture = "gfx/leaders/SIA/SIA_rama_v.dds" 
		expire = "1935.1.1" 
		ideology = despotism  
		traits = {  } 
	}
	create_field_marshal = {
		name = "Prince Amoradhat"
		traits = { politically_connected }
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 2
	}
	create_field_marshal = {
		name = "Chakrabongse Bhuvanart"
		traits = { politically_connected }
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Damrong Rajanubhab"
		traits = { harsh_leader }
		skill = 2
		attack_skill = 2
		defense_skill = 1
		planning_skill = 3
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Nakhon Sawan Worapinit"
		traits = { }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Nakhonchaisri Suradej"
		traits = { }
		skill = 1
		attack_skill = 2
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Purachatra Jayakara"
		traits = { }
		skill = 1
		attack_skill = 2
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 2
	}
}

1933.1.1 = {
	oob = "SIA_1936"

	if = {
		limit = { has_dlc = "Man the Guns" }
			set_naval_oob = "SIA_1936_naval_mtg"
		else = {
			set_naval_oob = "SIA_1936_naval_legacy"
		}
	}

	add_ideas = {
		SIA_divided_people
		SIA_exiled_royals
	}

	# Starting tech
	set_technology = {
		infantry_weapons = 1
		gw_artillery = 1
		early_fighter = 1
		CAS1 = 1
		early_bomber = 1
		early_submarine = 1
		early_destroyer = 1
		early_light_cruiser = 1
		early_heavy_cruiser = 1
	}

	set_convoys = 15

	set_popularities = {
		neutrality = 58
		monarchism = 17
		communism = 11
		fascism = 12
		democratic = 2
	}

	set_politics = {
		ruling_party = neutrality
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}

	create_country_leader = {
		name = "Phraya Phahon"
		desc = "POLITICS_PHRAYA_PHAHON_DESC"
		picture = "Portrait_Siam_Phraya_Phahon.dds"
		expire = "1965.1.1"
		ideology = despotism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Phayom Chulanont"
		desc = "POLITICS_PHAYOM_CHULANONT_DESC"
		picture = "gfx/leaders/Asia/Portrait_Asia_Generic_communism.dds"
		expire = "1965.1.1"
		ideology = marxism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Khuang Aphaiwong"
		desc = "POLITICS_KHUANG_APHAIWONG_DESC"
		picture = "gfx/leaders/Asia/Portrait_Asia_Generic_democracy.dds"
		expire = "1965.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Plaek Phibunsongkhram"
		desc = "POLITICS_PLAEK_PHIBUNSONGKHRAM_DESC"
		picture = "Portrait_Siam_Plaek_Phibunsongkhram.dds"
		expire = "1965.1.1"
		ideology = fascism_ideology
		traits = {
			#
		}
	}

	create_field_marshal = {
		name = "Rattanakun Seriroengrit"
		picture = "Portrait_Siam_Rattanakun_Seriroengrit.dds"
		skill = 4
		traits = { infantry_officer }
		attack_skill = 4
		defense_skill = 2
		planning_skill = 4
		logistics_skill = 2
	}

	create_field_marshal = {
		name = "Phin Choonhavan"
		picture = "Portrait_Siam_Phin_Choonhavan.dds"
		skill = 2
		traits = { unyielding_defender career_officer }
		attack_skill = 3
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Prayoon Rattanakit"
		portrait_path = "gfx/leaders/Asia/Portrait_Asia_Generic_land_3.dds"
		skill = 1
		traits = { politically_connected }
		attack_skill = 1
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Luang Phairirayordejd"
		portrait_path = "gfx/leaders/Asia/Portrait_Asia_Generic_land_2.dds"
		skill = 2
		traits = { organizer }
		attack_skill = 3
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Luang Haansongkhram"
		picture = "Portrait_Siam_Luang_Haansongkhram.dds"
		skill = 1
		traits = {}
		attack_skill = 1
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Thwuan Wichaikhatkha"
		portrait_path = "gfx/leaders/Asia/Portrait_Asia_Generic_warlord1.dds"
		skill = 1
		traits = {}
		attack_skill = 1
		defense_skill = 3
		planning_skill = 1
		logistics_skill = 1
	}
}