﻿capital = 293
1910.1.1 = {
	oob="YEM_1910"
	set_technology={ 
		infantry_weapons=1
	}

	set_politics = {
	    ruling_party = monarchism
	    last_election = "1910.1.1"
	    election_frequency = 48
	    elections_allowed = no
	}
	set_popularities = {
	    democratic = 0
	    fascism = 0
	    communism = 0
	    monarchism = 100
	}

	set_stability=0.31

	create_country_leader={ 
		name = "Hamid ed-Din" 
		picture="gfx/leaders/YEM_hamid_ed_din.dds" 
		expire="1935.1.1" 
		ideology=theocracy  
		traits={  } 
	}
	create_country_leader={ 
		name = "Hamid ed-Din" 
		picture="gfx/leaders/YEM_hamid_ed_din.dds" 
		expire="1935.1.1" 
		ideology=fundamentalism  
		traits={  } 
	}
	every_unit_leader = {
		set_unit_leader_flag = ww1_general
	}
}

1933.1.1 = {
	every_unit_leader = {
		limit = { has_unit_leader_flag = ww1_general }
		retire = yes
	}

	retire_ideology_leader = monarchism
	retire_ideology_leader = communism
	retire_ideology_leader = unionism
	retire_ideology_leader = neutrality
	retire_ideology_leader = fascism
	retire_ideology_leader = social_democracy
	retire_ideology_leader = democratic
	retire_ideology_leader = conservative

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_armor_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	remove_ideas = {
	}

	oob = "YEM_1936"

	# Starting tech
	set_technology = {
		infantry_weapons = 1
	}

	set_popularities = {
		democratic = 0
		fascism = 0
		communism = 0
		monarchism = 100
	}

	set_politics = {
		ruling_party = monarchism
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}

	create_country_leader = {
		name = "Imam Yahya"
		desc = "POLITICS_IMAM_YAHYA_DESC"
		picture = "Portrait_Yemen_Imam_Yahya.dds"
		expire = "1965.1.1"
		ideology = absolutism
		traits = {
			#
		}
	}
}