﻿capital = 285

set_country_flag = prev_undeveloped_colony
set_cosmetic_tag = ENG_australia

set_research_slots = 2
set_stability = 0.75
set_convoys = 100

create_country_leader = { 
	name = "Lord Thomas Denman" 
	picture = "gfx/leaders/AST/AST_thomas_denman.dds" 
	expire = "1935.1.1" 
	ideology = despotism
	traits = { } 
}
create_country_leader = { 
	name = "Stanley M. Bruce" 
	picture = "gfx/leaders/AST/AST_stanley_bruce.dds" 
	expire = "1935.1.1" 
	ideology = liberalism
	traits = { } 
}
create_country_leader = { 
	name = "William M. Hughes" 
	picture = "gfx/leaders/AST/AST_william_hughes.dds" 
	expire = "1923.2.9" 
	ideology = liberalism
	traits = { } 
}
create_country_leader = { 
	name = "Andrew Fisher" 
	picture = "gfx/leaders/AST/AST_andrew_fisher.dds" 
	expire = "1915.5.1" 
	ideology = socialism
	traits = { } 
}
#ARMY
create_corps_commander = {  
	name = "Harold Elliott"
	portrait_path = "gfx/leaders/AST/AST_Harold_Elliott.dds" 
	traits = { brilliant_strategist desert_fox } 
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}
create_corps_commander = {  
	name = "James W. McCay"
	portrait_path = "gfx/leaders/AST/AST_James_McCay.dds" 
	traits = { infantry_officer } 
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}
create_corps_commander = {  
	name = "William Bridges"
	portrait_path = "gfx/leaders/AST/AST_William_Bridges.dds" 
	traits = { trait_reckless } 
	skill = 3
	attack_skill = 4
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}
create_corps_commander = {  
	name = "Charles Cox"
	traits = { infantry_officer } 
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}
create_corps_commander = {  
	name = "Charles Rosenthal"
	traits = { } 
	skill = 2
	attack_skill = 2
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}
create_corps_commander = {  
	name = "William Holmes"
	traits = { harsh_leader infantry_officer }
	skill = 2
	attack_skill = 1
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {  
	name = "Charles Brand"
	traits = { } 
	skill = 2
	attack_skill = 2
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {  
	name = "Harry Chauvel"
	traits = { cavalry_officer } 
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 1
}
#NAVY
create_navy_leader = {  
	name = "George Hyde"
	traits = { gentlemanly } 
	skill = 2
	attack_skill = 2
	defense_skill = 2
	maneuvering_skill = 2
	coordination_skill = 3
}

oob = "AST_1910"

set_politics = {
    ruling_party = social_democracy 
	last_election = "1910.4.13"
    election_frequency = 36 
	elections_allowed = yes
}

set_popularities = {
	democratic = 46
	social_democracy = 51
	neutrality = 3
}

add_ideas = {
	volunteer_only
	AST_idea_Citizen_Army
	AST_idea_Wool_Monopoly
	AST_idea_Dominion_Army
}

1910.1.1 = {


	set_technology = {
		#infantry
		infantry_weapons = 1
		tech_mountaineers = 1	
		
		#support
		tech_support = 1
		wwi_tech_engineers = 1
		wwi_tech_recon = 1
		
		#artillery
		gw_artillery = 1
		howitzer1 = 1
			
		#doctrine
		nineteenth_cent_warfare = 1

		#air
		wwi_early_fighter = 1
		airship_bomber = 1
		
		#engineering
		early_fire_control_system = 1
		electrical_engineering = 1
		radio = 1
		analytical_engine = 1
		mechanical_engineering = 1
		aircraft_engine = 1
		generator_battery = 1
	}

	if = {
		limit = {
			not = { has_dlc = "Man the Guns" }
		}
		set_technology = {
			early_submarine = 1
			early_destroyer = 1
			early_light_cruiser = 1
			early_battleship = 1
			transport = 1
		}
	}
	if = {
		limit = {
			has_dlc = "Man the Guns"
		}
		set_technology = {
			wwi_early_ship_hull_light = 1
			wwi_basic_ship_hull_light = 1
			wwi_early_ship_hull_submarine = 1
			wwi_early_ship_hull_cruiser = 1
			wwi_basic_ship_hull_cruiser = 1
			wwi_early_ship_hull_heavy = 1
			wwi_basic_ship_hull_heavy = 1
			wwi_basic_torpedo = 1
			basic_battery = 1
			mtg_transport = 1
		}
	}

	every_unit_leader = {
		set_unit_leader_flag = ww1_general
	}
}

1933.1.1 = {
	every_unit_leader = {
		limit = { has_unit_leader_flag = ww1_general }
		retire = yes
	}

	retire_ideology_leader = monarchism
	retire_ideology_leader = communism
	retire_ideology_leader = unionism
	retire_ideology_leader = neutrality
	retire_ideology_leader = fascism
	retire_ideology_leader = social_democracy
	retire_ideology_leader = democratic
	retire_ideology_leader = conservative

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_armor_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	oob = "AST_1936"
	load_focus_tree = ww2_australia
	if = {
		limit = { has_dlc = "Man the Guns" }
		set_naval_oob = "AST_1936_naval_mtg"
		else = {
			set_naval_oob = "AST_1936_naval_legacy"
		}
	}

	set_stability = 0.8
	set_war_support = 0.3
	set_research_slots = 3

	# Starting tech
	set_technology = {
		infantry_weapons = 1
		infantry_weapons1 = 1
		tech_support = 1		
		tech_engineers = 1
		tech_recon = 1
		gw_artillery = 1
		interwar_antiair = 1
		gwtank = 1
		early_fighter = 1
		cv_early_fighter = 1
		cv_naval_bomber1 = 1
		naval_bomber1 = 1
		early_bomber = 1
		CAS1 = 1
		
		trench_warfare = 1
		fuel_silos = 1

		fleet_in_being = 1
	}
	if = {
		limit = {
			not = { has_dlc = "Man the Guns" }
		}
		set_technology = {
			early_destroyer = 1
			early_light_cruiser = 1
			early_heavy_cruiser = 1
			early_submarine = 1
			transport = 1
		}
		set_naval_oob = "AST_1936_naval_legacy"
	}
	if = {
		limit = {
			 has_dlc = "Man the Guns"
		}
		set_technology = {
			basic_naval_mines = 1
			submarine_mine_laying = 1
			early_ship_hull_light = 1
			early_ship_hull_cruiser = 1
			basic_ship_hull_cruiser = 1
			early_ship_hull_submarine = 1
			basic_battery = 1
			basic_torpedo = 1
			basic_depth_charges = 1
			basic_secondary_battery = 1
			basic_cruiser_armor_scheme = 1
			mtg_transport = 1
		}
		set_naval_oob = "AST_1936_naval"
	}

	add_ideas = {
		AST_great_depression_1
		AST_idea_fed_army
		AST_idea_Military_Naval_Force
		AST_idea_Workers_Education_Association
		AST_idea_Australian_Dollar
		AST_idea_Vet_Army
		AST_idea_overseas_vol
	}

	if = {
		limit = {
			has_dlc = "Together for Victory"
		}
		add_to_tech_sharing_group = commonwealth_research
	}

	remove_ideas = {
		volunteer_only
		AST_idea_Citizen_Army
		AST_idea_Wool_Monopoly
		AST_idea_Dominion_Army
	}

	set_popularities = {
		conservative = 49
		social_democracy = 43
		neutrality = 8
	}

	set_politics = {
		ruling_party = social_democracy
		last_election = "1934.9.15"
		election_frequency = 36
		elections_allowed = yes
	}

	#create_country_leader = {
	#	name = "John Curtin"
	#	desc = "POLITICS_JOHN_CURTIN_DESC"
	#	picture = "Portrait_Australia_John_Curtin.dds"
	#	expire = "1965.1.1"
	#	ideology = socialism
	#	traits = {
	#		#
	#	}
	#}

	create_country_leader = {
		name = "Eric Campbell"
		desc = "POLITICS_ERIC_CAMPBELL_DESC"
		picture = "GFX_AST_eric_campbell"
		expire = "1965.1.1"
		ideology = fascism_ideology
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Richard Dixon"
		desc = "POLITICS_RICHARD_DIXON_DESC"
		picture = "GFX_AST_richard_dixon"
		expire = "1965.1.1"
		ideology = stalinism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Earle Page"
		desc = "POLITICS_EARLE_PAGE_DESC"
		picture = "GFX_AST_earle_page"
		expire = "1965.1.1"
		ideology = socialism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Rod Hull"
		desc = ""
		picture = "Portrait_Australia_Rod_Hull.dds"
		expire = "1965.1.1"
		ideology = despotism
		traits = {
			#
		}
	}

	set_convoys = 100

	create_field_marshal = {
		name = "Thomas Blamey"
		gfx = "GFX_AST_thomas_blamey"
		traits = { }
		skill = 4
	    attack_skill = 4
	    defense_skill = 3
	    planning_skill = 3
	    logistics_skill = 3
	}
	create_corps_commander = {
		name = "Leslie Morshead"
		gfx = "GFX_AST_leslie_morshead"
		traits = { }
		skill = 4
	    attack_skill = 4
	    defense_skill = 2
	    planning_skill = 5
	    logistics_skill = 2
	}
	create_corps_commander = {
		name = "Henry Wynter"
		gfx = "GFX_AST_henry_wynter"
		traits = { }
		skill = 4
	    attack_skill = 3
	    defense_skill = 4
	    planning_skill = 3
	    logistics_skill = 3
	}
	create_corps_commander = {
		name = "Alan Vasey"
		gfx = "GFX_AST_alan_vasey"
		traits = { hill_fighter }
		skill = 3
	    attack_skill = 2
	    defense_skill = 4
	    planning_skill = 3
	    logistics_skill = 1
	}
	create_corps_commander = {
		name = "Horace Robertson"
		gfx = "GFX_AST_horace_robertson"
		traits = { desert_fox }
		skill = 3
	    attack_skill = 3
	    defense_skill = 1
	    planning_skill = 3
	    logistics_skill = 3
	}
	create_corps_commander = {
		name = "John Northcott"
		gfx = "GFX_AST_john_northcott"
		traits = { armor_officer}
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_navy_leader = {
		name = "Harold Farncomb"
		gfx = "GFX_AST_harold_farncomb"
		traits = { aviation_enthusiast naval_lineage }
		skill = 4
		attack_skill = 3
		defense_skill = 3
		maneuvering_skill = 3
		coordination_skill = 4
	}

	create_navy_leader = {
		name = "Victor Crutchley"
		gfx = "GFX_AST_victor_crutchley"
		traits = { bold }
		skill = 3
		attack_skill = 3
		defense_skill = 2
		maneuvering_skill = 3
		coordination_skill = 2
	}
}
