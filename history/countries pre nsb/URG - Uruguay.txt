﻿capital = 300

#-------------------------------------------------------
#					GENERIC VALUES
#-------------------------------------------------------
set_fuel_ratio = 0.20
set_research_slots = 2
set_stability = 0.75
set_convoys = 20


create_country_leader={ 
	name = "Claudio Williman" 
	picture="gfx/leaders/URG/URG_claudio_williman.dds" 
	expire="1935.1.1" 
	ideology=conservatism 
	traits={  } 
}
create_corps_commander={  
	name = "Viera" 
	 traits={   } 
	 skill=2
 }
create_corps_commander={ 
	 name = "Buquet" 
	 traits={   } 
	 skill=2 
 }
create_corps_commander={  
	name = "Maeso"
	 traits={   } 
	 skill=2 
 }
create_corps_commander={  
	name = "Pereira"
	 traits={   } 
	 skill=2
 }
create_corps_commander={  
	name = "Fabregat"
	 traits={   } 
	skill=2 
}
create_corps_commander={  
	name = "Cantón"
	traits={   } 
	skill=2 
}

1910.1.1 = {
	oob = "URG_1910"

	set_technology = {
		#infantry
		infantry_weapons = 1
		infantry_weapons1 = 1
		
		#artillery
		gw_artillery = 1
		
		#engineering
	}

	set_politics = {
	    ruling_party = conservative
	    last_election = "1910.1.1"
	    election_frequency = 60
	    elections_allowed = yes
	}
	set_popularities = {
	    conservative = 95
	    fascism = 1
	    unionism = 3
	    neutrality = 1
	}

	add_ideas = {

	}
	every_unit_leader = {
		set_unit_leader_flag = ww1_general
	}
}

1933.1.1 = {
	every_unit_leader = {
		limit = { has_unit_leader_flag = ww1_general }
		retire = yes
	}

	retire_ideology_leader = monarchism
	retire_ideology_leader = communism
	retire_ideology_leader = unionism
	retire_ideology_leader = neutrality
	retire_ideology_leader = fascism
	retire_ideology_leader = social_democracy
	retire_ideology_leader = democratic
	retire_ideology_leader = conservative

	reset_doctrines = yes
	set_country_flag = has_auto_1936_tech
	update_infantry_tech_1936 = yes
	update_armor_tech_1936 = yes
	update_support_tech_1936 = yes
	update_artillery_tech_1936 = yes
	update_electric_tech_1936 = yes
	update_industry_tech_1936 = yes
	update_air_tech_1936 = yes
	update_mtgnaval_tech_1936 = yes

	remove_ideas = {
	}

	oob = "URG_1936"

	# Starting tech
	set_technology = {
		infantry_weapons = 1
		gw_artillery = 1
		early_fighter = 1
		early_destroyer = 1
		early_light_cruiser = 1
	}
	set_country_flag = monroe_doctrine

	set_convoys = 5

	set_popularities = {
		democratic = 97
		fascism = 0
		communism = 3
		neutrality = 0
	}

	set_politics = {
		ruling_party = democratic
		last_election = "1934.4.19"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Gabriel Terra"
		desc = "POLITICS_GABRIEL_TERRA_DESC"
		picture = "Portrait_Uruguay_Gabriel_Terra.dds"
		expire = "1965.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}
}
