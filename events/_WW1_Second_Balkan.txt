﻿add_namespace = ww1_secondbalkan

# SECOND BALKAN WAR
####################
country_event = {
	id = ww1_secondbalkan.1
	title = ww1_secondbalkan.1.t
	desc = ww1_secondbalkan.1.d
	picture = GFX_report_event_military_planning

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = ww1_secondbalkan.1.a
		ai_chance = { factor = 100 }
		
		BUL = {
			declare_war_on = {
				target = YUG
				type = take_claimed_state
				generator = {
					106
				}
			}
			if = {
				limit = { GRE = { owns_state = 731 } }
				declare_war_on = {
					target = GRE
					type = take_claimed_state
					generator = {
						731
					}
				}
			}
			if = {
				limit = { is_in_faction = no }
				create_faction = "greater_builgaria"
			}
			set_country_flag = kis_secondbalkanwar_start
		}
		
		news_event = { days = 1 id = ww1_secondbalkan.100 }
		
		hidden_effect = {
			set_country_flag = kis_secondbalkan_war
			set_global_flag = kis_secondbalkan_war
			
			YUG = {
				create_faction = anti_bulg_league
				set_country_flag = join_antibulgarian_league
				if = {
					limit = {
						GRE = { 
							owns_state = 731
						}
					}
					add_to_faction = GRE
				}
				if = {
					limit = { 
						MNT = { 
							has_country_flag = join_balkan_league 
							NOT = { has_country_flag = leave_balkan_league }
						}
					}
					add_to_faction = MNT
				}
			}

			if = {
				limit = { 
					GRE = { 
						owns_state = 731
					}
				}
				GRE = { set_country_flag = join_antibulgarian_league }
			}
			if = {
				limit = { 
					MNT = {
						is_in_faction_with = YUG
					}
				}
				MNT = { set_country_flag = join_antibulgarian_league }
				add_to_war = {
					targeted_alliance = YUG enemy = BUL
				}
			}
		}
	}
}

# ROMANIAN INTERVENTION
########################
country_event = {
	id = ww1_secondbalkan.2
	title = ww1_secondbalkan.2.t
	desc = ww1_secondbalkan.2.d
	picture = GFX_report_event_military_planning

	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = { has_global_flag = kis_secondbalkanwar_victory }
	}

	option = {
		name = ww1_secondbalkan.2.a
		ai_chance = { factor = 100 }
		ROM = {
			set_country_flag = kis_secondbalkanwar_romanian_intervention
			set_country_flag = join_antibulgarian_league
			declare_war_on = {
				target = BUL
				type = take_claimed_state
				generator = {
					77
				}
			}
	        add_to_war = {
	            targeted_alliance = YUG enemy = BUL
	        }
		}
		YUG = {
			add_to_faction = ROM
		}
		news_event = { days = 1 id = ww1_secondbalkan.102 }
	}
}

# BULGARIA OFFERS SURRENDER
############################
country_event = {
	id = ww1_secondbalkan.3
	title = ww1_secondbalkan.3.t
	desc = ww1_secondbalkan.3.d
	picture = GFX_report_event_military_planning

	fire_only_once = yes

	is_triggered_only = yes

	option = { 
		name = ww1_secondbalkan.3.a
		ai_chance = { factor = 100 }

			set_global_flag = kis_secondbalkanwar_victory		
			
			#Восточная Македония
			if = {
				limit = {
					YUG = {
						has_war_with = BUL
					}
				}
				BUL = { remove_state_core = 106 add_state_claim = 106 }
			}
			if = {
				limit = {
					BUL = { owns_state = 106 }
					YUG = {
						has_war_with = BUL
					}
				}
				YUG = { 
					transfer_state = 106
					add_state_core = 106
				}
				BUL = { remove_state_core = 106 add_state_claim = 106 }				
			}
			if = {
				limit = {
					BUL = { owns_state = 803 }
					YUG = {
						has_war_with = BUL
					}
				}
				YUG = { 
					transfer_state = 803
					add_state_core = 803
				}
				BUL = { remove_state_core = 803 add_state_claim = 803 }				
			}
			# Греческая Македония
			if = {
				limit = {
					BUL = { owns_state = 731 }
					GRE = { has_war_with = BUL }
				}
				GRE = { 
					transfer_state = 731
					add_state_core = 731
				}
				BUL = { remove_state_core = 731 add_state_claim = 731 }	
			}
			if = {
				limit = {
					BUL = { owns_state = 731 }
					GRE = {
						NOT = { has_war_with = BUL }
					}
				}
				YUG = { 
					transfer_state = 731 
					add_state_core = 731
				}
				BUL = { remove_state_core = 731 add_state_claim = 731 }	
			}
			# Греческая Фракия
			if = {
				limit = {
					BUL = { owns_state = 1063 }
					GRE = {
						has_war_with = BUL
					}
				}
				GRE = { 
					transfer_state = 1063
					add_state_core = 1063
				}
				BUL = { remove_state_core = 1063 add_state_claim = 1063 }				
			}	
			# Добруджа
			if = {
				limit = {
					BUL = { owns_state = 77 }
					ROM = {
						has_war_with = BUL
					}
				}
				ROM = {
					transfer_state = 77
					add_state_core = 77
				}
				BUL = { remove_state_core = 77 add_state_claim = 77 }
			}
			# Мир
			every_country = {
				limit = {
					has_war_with = BUL
					has_country_flag = join_antibulgarian_league
				}
				white_peace = BUL
				remove_opinion_modifier = { 
					target = TUR 
					modifier = HOI4TGW_Opinion_hostile_relations_post_balkan_war 
				}
				set_truce = {
					target = BUL
					days = 365
				}
			}
			if = {
				limit = { YUG = {is_faction_leader = yes}}
		    	YUG = {
			        dismantle_faction = yes
				}
			}
			if = {
				limit = { BUL = {is_faction_leader = yes}}
			    BUL = {
				    dismantle_faction = yes
				}
			}
			if = {
				limit = { YUG = {is_faction_leader = yes}}
		    	YUG = {
			        dismantle_faction = yes
				}
			}
			if = {
				limit = { BUL = {is_faction_leader = yes}}
			    BUL = {
				    dismantle_faction = yes
				}
			}
			BUL = {
				add_ai_strategy = {
					type = friend
					id = GER
					value = 100
				}
			}
			news_event = { days = 1 id = ww1_secondbalkan.101 }
	}
	
	option = { 
		name = ww1_secondbalkan.3.b
		    if = {
				limit = { YUG = {is_faction_leader = yes}}
		    	YUG = {
			        dismantle_faction = yes
				}
			}
			if = {
				limit = { BUL = {is_faction_leader = yes}}
			    BUL = {
				    dismantle_faction = yes
				}
			}
		BUL = {
			add_ai_strategy = {
				type = alliance
				id = GER
				value = 100
			}
		}
		hidden_effect = {
			GER = {
				add_ai_strategy = {
					type = alliance
					id = BUL
					value = 100
				}
			}
		}
		ai_chance = { factor = 0 }
	}
}

# SERBIA OFFERS SURRENDER
############################
country_event = {
	id = ww1_secondbalkan.4
	title = ww1_secondbalkan.4.t
	desc = ww1_secondbalkan.4.d
	picture = GFX_report_event_generic_read_write

	fire_only_once = yes

	is_triggered_only = yes

	option = { 
		name = ww1_secondbalkan.3.a
		ai_chance = { factor = 100 }

			set_global_flag = kis_secondbalkanwar_lose	
			
			if = {
				limit = {
					BUL = {
						has_war_with = YUG
					}
					YUG = { owns_state = 106 }
				}
				BUL = {
					transfer_state = 106
					add_state_core = 106
				}
				YUG = { remove_state_core = 106 add_state_claim = 106 }					
			}
			if = {
				limit = {
					BUL = {
						has_war_with = GRE
					}
					GRE = { owns_state = 731 }
				}
				BUL = {
					transfer_state = 731 
					add_state_core = 731
				}				
			}
			if = {
				limit = {
					BUL = {
						has_war_with = YUG
					}
					YUG = { owns_state = 731 }
				}
				BUL = {
					transfer_state = 731 
					add_state_core = 731
				}				
			}
			if = {
				limit = {
					BUL = {
						has_war_with = GRE
					}
					GRE = { owns_state = 184 }
				}
				BUL = {
					transfer_state = 184 
					add_state_core = 184
				}			
			}
			
			if = {
				limit = {
					BUL = { owns_state = 803 }
					YUG = {
						has_war_with = BUL
					}
				}
				YUG = { 
					transfer_state = 803
					add_state_core = 803
				}
				BUL = { remove_state_core = 803 add_state_claim = 803 }				
			}
	
			# Греческая Фракия
			if = {
				limit = {
					BUL = { owns_state = 1063 }
					GRE = {
						has_war_with = BUL
					}
				}
				GRE = { 
					transfer_state = 1063
					add_state_core = 1063
				}
				BUL = { remove_state_core = 1063 add_state_claim = 1063 }				
			}	
			# Мир
			every_country = {
				limit = {
					has_war_with = BUL
					has_country_flag = join_antibulgarian_league
				}
				white_peace = BUL
				remove_opinion_modifier = { 
					target = TUR 
					modifier = HOI4TGW_Opinion_hostile_relations_post_balkan_war 
				}
				set_truce = {
					target = BUL
					days = 365
				}
			}
			if = {
				limit = { YUG = {is_faction_leader = yes}}
		    	YUG = {
			        dismantle_faction = yes
				}
			}
			if = {
				limit = { BUL = {is_faction_leader = yes}}
			    BUL = {
				    dismantle_faction = yes
				}
			}
			YUG = {
				add_ai_strategy = {
					type = friend
					id = RUS
					value = 100
				}
			}
			news_event = { days = 1 id = ww1_secondbalkan.104 }
	}
	option = { 
		name = ww1_secondbalkan.3.b
		news_event = { days = 1 id = ww1_secondbalkan.105 }
		ai_chance = { factor = 0 }
		    if = {
				limit = { YUG = {is_faction_leader = yes}}
		    	YUG = {
			        dismantle_faction = yes
				}
			}
			if = {
				limit = { BUL = {is_faction_leader = yes}}
			    BUL = {
				    dismantle_faction = yes
				}
			}
	}
}
# SECOND BALKAN WAR
####################
country_event = {
	id = ww1_secondbalkan.5
	title = ww1_secondbalkan.5.t
	desc = ww1_secondbalkan.5.d
	picture = GFX_report_event_military_planning

	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.5.a
		set_global_flag = kis_secondbalkan_war
		set_global_flag = kis_secondbalkanwar_victory
		ai_chance = { factor = 10 }
	}
	option = {
		name = ww1_secondbalkan.5.b
		ai_chance = { factor = 1 }
		
		YUG = {
			create_faction = anti_bulg_league
			declare_war_on = {
				target = BUL
				type = take_claimed_state
				generator = {
					
				}
			}
		}
		BUL = {
			if = {
				limit = { is_in_faction = no }
				create_faction = "greater_builgaria"
			}
			set_country_flag = kis_secondbalkanwar_start
		}
		news_event = { days = 1 id = ww1_secondbalkan.100 }
		
		hidden_effect = {
			set_country_flag = kis_secondbalkan_war
			set_global_flag = kis_secondbalkan_war			
			YUG = {	
				set_country_flag = join_antibulgarian_league
				if = {
					limit = {
						GRE = { 
							owns_state = 731
						}
					}
					add_to_faction = GRE
					GRE = { set_country_flag = join_antibulgarian_league }
				}
				if = {
					limit = { 
						MNT = { 
							has_country_flag = join_balkan_league 
							NOT = { has_country_flag = leave_balkan_league }
						}
					}
					add_to_faction = MNT
					MNT = { set_country_flag = join_antibulgarian_league }
					add_to_war = {
						targeted_alliance = YUG 
						enemy = BUL
					}
				}
			}
		}
	}
}
# SECOND BALKAN WAR (GREAT SERBIA)
####################
country_event = {
	id = ww1_secondbalkan.6
	title = ww1_secondbalkan.6.t
	desc = ww1_secondbalkan.6.d
	picture = GFX_report_event_military_planning

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = ww1_secondbalkan.6.a
		ai_chance = { factor = 100 }
	
		BUL = {
			declare_war_on = {
				target = YUG
				type = take_claimed_state
				generator = {
					106
				}
			}
			set_country_flag = kis_secondbalkanwar_start
		}
		
		news_event = { days = 1 id = ww1_secondbalkan.106 }
		
		hidden_effect = {
			set_country_flag = kis_secondbalkan_war
			set_global_flag = kis_secondbalkan_war

			if = {
			    limit = { 
					GRE = { has_country_flag = join_bulgarian_balkan_league is_in_faction_with = BUL}
				}
				GRE = {
				    add_to_war = {
					    targeted_alliance = BUL enemy = YUG
				    }
				}	
			}
			if = {
			    limit = { 
					MNT = { has_country_flag = join_bulgarian_balkan_league is_in_faction_with = BUL}
				}
				MNT = {
				    add_to_war = {
					    targeted_alliance = BUL enemy = YUG
				    }
				}
			}
		}
	}
}
# GREAT SERBIA OFFERS SURRENDER 
############################
country_event = {
	id = ww1_secondbalkan.7
	title = ww1_secondbalkan.4.t
	desc = ww1_secondbalkan.7.d
	picture = GFX_report_event_generic_read_write

	fire_only_once = yes

	is_triggered_only = yes

	option = { 
		name = ww1_secondbalkan.3.a
		ai_chance = { factor = 100 }

			set_global_flag = kis_secondbalkanwar_lose	
			
			if = {
				limit = {
					BUL = {
						has_war_with = YUG
					}
					YUG = { owns_state = 106 }
				}
				BUL = {
					transfer_state = 106
					add_state_core = 106
				}
				YUG = { remove_state_core = 106 add_state_claim = 106 }					
			}
			if = {
				limit = {
					BUL = {
						has_war_with = YUG
					}
					YUG = { owns_state = 1139 }
				}
				BUL = {
					transfer_state = 1139
					add_state_core = 1139
				}
				YUG = { remove_state_core = 1139 add_state_claim = 1139 }					
			}
			if = {
				limit = {
					GRE = {
						has_war_with = YUG
					}
					YUG = { owns_state = 731 }
				}
				GRE = {
					transfer_state = 731 
					add_state_core = 731
				}				
			}
			if = {
				limit = {
					GRE = {
						has_war_with = YUG
					}
					YUG = { owns_state = 184 }
				}
				GRE = {
					transfer_state = 184 
					add_state_core = 184
				}				
			}
			
			if = {
				limit = {
					GRE = {
						has_war_with = YUG
					}
					YUG = { owns_state = 1063 }
				}
				GRE = {
					transfer_state = 1063 
					add_state_core = 1063
				}				
			}
			
			# Мир
			every_country = {
				limit = {
					has_war_with = BUL
					has_country_flag = join_antibulgarian_league
				}
				white_peace = BUL
				set_truce = {
					target = BUL
					days = 365
				}
			}
			every_country = {
				limit = {
					has_war_with = YUG
					OR= {
					    has_country_flag = join_bulgarian_balkan_league
					    has_country_flag =  BUL_creating_balkan_league
					}	
				}
				white_peace = YUG
				set_truce = {
					target = YUG
					days = 365
				}
			}
			if = {
				limit = { YUG = {is_faction_leader = yes}}
		    	YUG = {
			        dismantle_faction = yes
				}
			}
			if = {
				limit = { BUL = {is_faction_leader = yes}}
			    BUL = {
				    dismantle_faction = yes
				}
			}
			if = {
			    limit = {
					YUG = {has_government = fascism}
					BUL = {has_government = monarchism}
				}	
			    YUG = {
					set_politics = {
				        ruling_party = monarchism
				    }
				}	
			}
			news_event = { days = 1 id = ww1_secondbalkan.107 }
	}		
	option = { 
		name = ww1_secondbalkan.3.b
		news_event = { days = 1 id = ww1_secondbalkan.110 }
		ai_chance = { factor = 0 }
		if = {
				limit = { YUG = {is_faction_leader = yes}}
		    	YUG = {
			        dismantle_faction = yes
				}
			}
			if = {
				limit = { BUL = {is_faction_leader = yes}}
			    BUL = {
				    dismantle_faction = yes
				}
			}
	}
}
# BULGARIAN BALKAN LEAGE OFFERS SURRENDER
############################
country_event = {
	id = ww1_secondbalkan.8
	title = ww1_secondbalkan.3.t
	desc = ww1_secondbalkan.8.d
	picture = GFX_report_event_generic_read_write

	fire_only_once = yes

	is_triggered_only = yes

	option = { 
		name = ww1_secondbalkan.3.a
		ai_chance = { factor = 20 }

			set_global_flag = kis_secondbalkanwar_victory		
			
			if = {
				limit = {
					BUL = { owns_state = 48 }
					YUG = {
						has_war_with = BUL
					}
				}
				YUG = { 
					transfer_state = 48 
				}					
			}
			if = {
				limit = {
					BUL = { owns_state = 1103 }
					YUG = {
						has_war_with = BUL
					}
				}
				YUG = { 
					transfer_state = 1103 
				}					
			}
			if = {
				limit = {
					GRE = { owns_state = 731 }
					GRE = {
						has_war_with = YUG
					}
				}
				YUG = { 
					transfer_state = 731 
				}					
			}
			if = {
				limit = {
					GRE = { owns_state = 184 }
					GRE = {
						has_war_with = YUG
					}
				}
				YUG = { 
					transfer_state = 184 
				}					
			}
			if = {
				limit = {
					GRE = { owns_state = 1063 }
					GRE = {
						has_war_with = YUG
					}
				}
				YUG = { 
					transfer_state = 1063 
				}					
			}
			if = {
				limit = {
					GRE = { owns_state = 185 }
					GRE = {
						has_war_with = YUG
					}
				}
				YUG = { 
					transfer_state = 185 
				}					
			}
			if = {
				limit = {
					GRE = { owns_state = 976 }
					GRE = {
						has_war_with = YUG
					}
				}
				YUG = { 
					transfer_state = 976 
				}					
			}
			if = {
				limit = {
					GRE = { owns_state = 977 }
					GRE = {
						has_war_with = YUG
					}
				}
				YUG = { 
					transfer_state = 977 
				}					
			}
			if = {
				limit = {
					MNT = { owns_state = 105 }
					YUG = {
						has_war_with = MNT
					}
				}
				YUG = { 
					transfer_state = 105
					add_state_core = 105
				}			
			}
			if = {
				limit = {
					MNT = { owns_state = 940 }
					YUG = {
						has_war_with = MNT
					}
				}
				YUG = { 
					transfer_state = 940
					add_state_core = 940
				}			
			}
			if = {
				limit = {
					BUL = { owns_state = 77 }
					ROM = {
						has_war_with = BUL
					}
				}
				ROM = {
					transfer_state = 77
					add_state_core = 77
				}
				BUL = { remove_state_core = 77 add_state_claim = 77 }
			}
			every_country = {
				limit = {
					has_war_with = BUL
					has_country_flag = join_antibulgarian_league
				}
				white_peace = BUL
				set_truce = {
					target = BUL
					days = 365
				}
			}
			every_country = {
				limit = {
					has_war_with = YUG
					OR= {
					    has_country_flag =  join_bulgarian_balkan_league
					    has_country_flag =  BUL_creating_balkan_league
					}	
				}
				white_peace = YUG
				set_truce = {
					target = YUG
					days = 365
				}
			}
			if = {
				limit = { YUG = {is_faction_leader = yes}}
		    	YUG = {
			        dismantle_faction = yes
				}
			}
			if = {
				limit = { BUL = {is_faction_leader = yes}}
			    BUL = {
				    dismantle_faction = yes
				}
			}
			news_event = { days = 1 id = ww1_secondbalkan.108 }
	}
	option = { 
		name = ww1_secondbalkan.8.b
		news_event = { days = 1 id = ww1_secondbalkan.109 }
		ai_chance = { factor = 0 }
		    if = {
				limit = { YUG = {is_faction_leader = yes}}
		    	YUG = {
			        dismantle_faction = yes
				}
			}
			if = {
				limit = { BUL = {is_faction_leader = yes}}
			    BUL = {
				    dismantle_faction = yes
				}
			}
	}
}
# TURKISH INTERVENTION
########################
country_event = {
	id = ww1_secondbalkan.9
	title = ww1_secondbalkan.9.t
	desc = ww1_secondbalkan.9.d
	picture = GFX_report_event_military_planning

	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = { has_global_flag = kis_secondbalkanwar_victory }
	}

	option = {
		name = ww1_secondbalkan.9.a
		ai_chance = { factor = 100 }
		TUR = {
			set_country_flag = join_antibulgarian_league
			declare_war_on = {
				target = BUL
				type = take_claimed_state
				generator = {
					77
				}
			}
	        add_to_war = {
	            targeted_alliance = YUG enemy = BUL
	        }
		}
		YUG = {
			add_to_faction = TUR
		}
		news_event = { days = 1 id = ww1_secondbalkan.103 }
	}
}
# SECOND BALKAN WAR STARTS - NEWS
############################
news_event = {
	id = ww1_secondbalkan.100
	title = ww1_secondbalkan.100.t
	desc = ww1_secondbalkan.100.d
	picture = EVENT_hoi4tgw_second_balkan_war

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.100.a
	}
}

# SECOND BALKAN WAR ENDS - NEWS
############################
news_event = {
	id = ww1_secondbalkan.101
	title = ww1_secondbalkan.101.t
	desc = ww1_secondbalkan.101.d
	picture = EVENT_hoi4tgw_second_balkan_war_end

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.101.a
	}
}

# ROMANIAN INTERVENTION - NEWS
############################
news_event = {
	id = ww1_secondbalkan.102
	title = ww1_secondbalkan.102.t
	desc = ww1_secondbalkan.102.d
	picture = EVENT_hoi4tgw_second_balkan_war

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.102.a
	}
}

# TURKISH INTERVENTION - NEWS
############################
news_event = {
	id = ww1_secondbalkan.103
	title = ww1_secondbalkan.103.t
	desc = ww1_secondbalkan.103.d
	picture = EVENT_hoi4tgw_second_balkan_war

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.103.a
	}
}

# BULGARIA WINS - NEWS
############################
news_event = {
	id = ww1_secondbalkan.104
	title = ww1_secondbalkan.104.t
	desc = ww1_secondbalkan.104.d
	picture = EVENT_hoi4tgw_second_balkan_war

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.104.a
	}
}

# COLLAPSE OF LEAGUE - NEWS
############################
news_event = {
	id = ww1_secondbalkan.105
	title = ww1_secondbalkan.105.t
	desc = ww1_secondbalkan.105.d
	picture = EVENT_hoi4tgw_second_balkan_war

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.105.a
	}
}
# SECOND BALKAN WAR STARTS - NEWS (BULGARIAN BALKAN LEAGUE DECLARES WAR ON GREAT SERRBIA)
############################
news_event = {
	id = ww1_secondbalkan.106
	title = ww1_secondbalkan.100.t
	desc = ww1_secondbalkan.106.d
	picture = EVENT_hoi4tgw_second_balkan_war

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.100.a
	}
}
# SECOND BALKAN WAR ENDS - NEWS (BULGARIAN BALKAN LEAGUE WON AGAINST GREAT SERRBIA)
############################
news_event = {
	id = ww1_secondbalkan.107
	title = ww1_secondbalkan.104.t
	desc = ww1_secondbalkan.107.d
	picture = EVENT_hoi4tgw_second_balkan_war_end

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.101.a
	}
}
# SECOND BALKAN WAR ENDS - NEWS (GREAT SERBIA WON)
############################
news_event = {
	id = ww1_secondbalkan.108
	title = ww1_secondbalkan.101.t
	desc = ww1_secondbalkan.108.d
	picture = EVENT_hoi4tgw_second_balkan_war_end

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.101.a
	}
}
# COLLAPSE OF BULGARIAN LEAGUE - NEWS
############################
news_event = {
	id = ww1_secondbalkan.109
	title = ww1_secondbalkan.105.t
	desc = ww1_secondbalkan.109.d
	picture = EVENT_hoi4tgw_second_balkan_war

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.105.a
	}
}
# COLLAPSE OF GREAT SERBIA
############################
news_event = {
	id = ww1_secondbalkan.110
	title = ww1_secondbalkan.110.t
	desc = ww1_secondbalkan.110.d
	picture = EVENT_hoi4tgw_second_balkan_war

	major = yes
	is_triggered_only = yes
	
	option = {
		name = ww1_secondbalkan.105.a
	}
}